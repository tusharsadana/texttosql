ALTER TABLE `account_delete_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `additional_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `additional_infos_internship_id_idx` (`job_id`),
  ADD KEY `type` (`type`),
  ADD KEY `question_type` (`question_type`);




ALTER TABLE `administrative_area_levels_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_indian_state_ut` (`is_indian_state_ut`),
  ADD KEY `name` (`name`(191));




ALTER TABLE `administrative_area_levels_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));




ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `admins_admin_privileges`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_id_2` (`admin_id`,`admin_privilege_id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `privilege_id` (`admin_privilege_id`);




ALTER TABLE `admins_daily_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);




ALTER TABLE `admins_internships_tracker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);




ALTER TABLE `admin_em_lists_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);




ALTER TABLE `admin_privileges`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);




ALTER TABLE `admin_privileges_admin_urls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_privilege_id` (`admin_privilege_id`),
  ADD KEY `admin_url_id` (`admin_url_id`);




ALTER TABLE `admin_uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);




ALTER TABLE `admin_urls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`);




ALTER TABLE `advanced_search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advanced_search_internship_idx` (`job_id`),
  ADD KEY `operator` (`operator`);




ALTER TABLE `aicte_signups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `is_processed` (`is_processed`);




ALTER TABLE `analysis_student_complaints_data`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `analysis_student_complaints_output`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complaint_id` (`complaint_id`),
  ADD KEY `internship_id` (`internship_id`);




ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `applications_combo_uq` (`student_id`,`job_id`),
  ADD KEY `applications_internship_id_idx` (`job_id`),
  ADD KEY `applications_student_id_idx` (`student_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `sort_order` (`sort_order`),
  ADD KEY `status` (`status`),
  ADD KEY `is_filtered_out` (`is_filtered_out`),
  ADD KEY `applications_invitation_status` (`invitation_status`),
  ADD KEY `is_internship_story_mail_sent` (`is_internship_story_mail_sent`);




ALTER TABLE `applications_additional_infos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `applications_additionalinfos_combo_uq` (`application_id`,`additional_info_id`),
  ADD KEY `applications_additionalinfos_application_id_idx` (`application_id`),
  ADD KEY `additional_info_id` (`additional_info_id`);




ALTER TABLE `applications_evaluation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `applications_new`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `applications_combo_uq` (`student_id`,`internship_id`),
  ADD KEY `applications_internship_id_idx` (`internship_id`),
  ADD KEY `applications_student_id_idx` (`student_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `sort_order` (`sort_order`),
  ADD KEY `status` (`status`),
  ADD KEY `is_filtered_out` (`is_filtered_out`),
  ADD KEY `applications_invitation_status` (`invitation_status`),
  ADD KEY `is_internship_story_mail_sent` (`is_internship_story_mail_sent`);




ALTER TABLE `applications_similar_jobs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `similar_internship_id` (`similar_job_id`);




ALTER TABLE `applications_soft_seen_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`);




ALTER TABLE `applications_status_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `old_status` (`old_status`),
  ADD KEY `new_status` (`new_status`),
  ADD KEY `processing_status` (`processing_status`);




ALTER TABLE `application_form_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `application_reporting_reasons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `professionalism_report_reason_id` (`professionalism_report_reason_id`),
  ADD KEY `status` (`status`),
  ADD KEY `source` (`source`),
  ADD KEY `options` (`options`);




ALTER TABLE `approved_colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `used_count` (`used_count`);




ALTER TABLE `app_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `send_on` (`send_on`);




ALTER TABLE `app_notification_gcm_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `app_notification_id_2` (`app_notification_id`,`gcm_key_id`),
  ADD KEY `app_notification_id` (`app_notification_id`,`gcm_key_id`),
  ADD KEY `is_sent` (`is_sent`),
  ADD KEY `gcm_key_id` (`gcm_key_id`);




ALTER TABLE `app_rating_dialogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `modal_name` (`modal_name`),
  ADD KEY `response` (`response`);




ALTER TABLE `banned_employer_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_2` (`type`,`value`),
  ADD KEY `type` (`type`,`value`);




ALTER TABLE `banner_key_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slider_banner_id_2` (`slider_banner_id`,`banner_template_key_id`),
  ADD KEY `slider_banner_id` (`slider_banner_id`),
  ADD KEY `banner_template_key_id` (`banner_template_key_id`);




ALTER TABLE `banner_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);




ALTER TABLE `banner_template_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`banner_template_id`),
  ADD KEY `banner_template_id` (`banner_template_id`);




ALTER TABLE `blocked_domains`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain_name` (`domain_name`);




ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(255)),
  ADD KEY `status` (`status`);




ALTER TABLE `business_case_contests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `preference` (`preference`),
  ADD KEY `employer_id` (`employer_id`);




ALTER TABLE `business_case_contest_certificates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_plan_preference_id` (`business_case_contest_preference_id`),
  ADD KEY `student_id` (`student_id`);




ALTER TABLE `business_case_contest_mail_templates`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `business_case_contest_preferences`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `b_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain_name` (`domain_name`);




ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `acknowledgement_mail_template_id` (`acknowledgement_mail_template_id`),
  ADD KEY `url` (`url`) USING BTREE;




ALTER TABLE `campaigns_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `referral_campaign_user_id` (`referral_campaign_user_id`);




ALTER TABLE `campaign_experiment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_user_id` (`campaign_user_id`);




ALTER TABLE `campaign_fair_application_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `application_id` (`application_id`);




ALTER TABLE `campaign_fair_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`);




ALTER TABLE `campaign_fair_interviewees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_code` (`student_code`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `campaign_fair_detail_id` (`campaign_fair_detail_id`);




ALTER TABLE `campaign_fair_interviewees_slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `score` (`score`),
  ADD KEY `campaign_fair_interviewee_id` (`campaign_fair_interviewee_id`);




ALTER TABLE `campaign_faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`);




ALTER TABLE `campaign_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `mail_template_id` (`mail_template_id`);




ALTER TABLE `campaign_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaigns_sections_ibfk_1` (`campaign_id`);




ALTER TABLE `campaign_tnps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `campaign_tnps_college_location_id` (`college_location_id`),
  ADD KEY `campaign_tnps_college_id` (`college_id`);




ALTER TABLE `campaign_users_additional_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_entries_additional_info_campaign_entry_id` (`campaign_user_id`),
  ADD KEY `campaign_key` (`campaign_key`(191));




ALTER TABLE `campaign_user_fair_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `campaign_entry_id` (`campaign_user_id`);




ALTER TABLE `campaign_user_referral_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_user` (`campaign_user_id`);




ALTER TABLE `career_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);




ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_category_id` (`parent_category_id`),
  ADD KEY `linkedin_industry_code_id` (`linkedin_industry_code_id`),
  ADD KEY `linkedin_job_function_id` (`linkedin_job_function_id`),
  ADD KEY `name` (`name`),
  ADD KEY `label` (`label`),
  ADD KEY `status` (`status`);




ALTER TABLE `categories_streams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `stream_id` (`stream_id`);




ALTER TABLE `category_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `precise` (`precise`),
  ADD KEY `recall` (`recall`),
  ADD KEY `count` (`count`),
  ADD KEY `f1_score` (`f1_score`);




ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `certificate_campaign_id` (`certificate_campaign_id`);




ALTER TABLE `certificate_campaigns`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `certificate_templates`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `chatbot_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`);




ALTER TABLE `chat_conversations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `application_id` (`application_id`),
  ADD KEY `is_seen_by_student` (`is_seen_by_student`),
  ADD KEY `is_seen_by_employer` (`is_seen_by_employer`),
  ADD KEY `blocked_by` (`blocked_by`),
  ADD KEY `chat_initation_referral` (`chat_initation_referral`);




ALTER TABLE `chat_conversation_extended`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_conversation_extended_ibfk_1` (`chat_conversation_id`);




ALTER TABLE `chat_emails_sent`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `chat_message_id_2` (`chat_message_id`),
  ADD KEY `status` (`status`),
  ADD KEY `chat_conversation_id` (`chat_conversation_id`),
  ADD KEY `chat_message_id` (`chat_message_id`);




ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `sender` (`sender`),
  ADD KEY `is_seen` (`is_seen`),
  ADD KEY `chat_conversation_id` (`chat_conversation_id`),
  ADD KEY `chat_messages_is_hidden` (`is_hidden`),
  ADD KEY `filename` (`filename`(191)),
  ADD KEY `old_attachment` (`old_attachment`(191)),
  ADD KEY `attachment` (`attachment`(191));




ALTER TABLE `chat_message_thumbnails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_message_id` (`chat_message_id`);




ALTER TABLE `chat_users_presence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `fk_user_id` (`user_id`);




ALTER TABLE `city_clusters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);




ALTER TABLE `city_clusters_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_cluster_id` (`city_cluster_id`),
  ADD KEY `locality_id` (`locality_id`),
  ADD KEY `name` (`name`);




ALTER TABLE `city_specific_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discount_id` (`discount_id`),
  ADD KEY `city_name` (`city_name`);




ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`rid`) USING BTREE,
  ADD UNIQUE KEY `id` (`id`,`ip_address`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `classified_emails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`message_id`),
  ADD KEY `predicted_tag` (`predicted_tag`),
  ADD KEY `received_on` (`received_on`),
  ADD KEY `email_from` (`email_from`),
  ADD KEY `type` (`type`),
  ADD KEY `employer_email` (`employer_email`);




ALTER TABLE `classified_emails_help_center`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`message_id`),
  ADD KEY `predicted_tag` (`w2v_predicted_tag`),
  ADD KEY `received_on` (`received_on`),
  ADD KEY `email_from` (`email_from`),
  ADD KEY `type` (`type`),
  ADD KEY `employer_email` (`employer_email`);




ALTER TABLE `classified_emails_trainings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`message_id`),
  ADD KEY `predicted_tag` (`predicted_tag`),
  ADD KEY `received_on` (`received_on`),
  ADD KEY `email_from` (`email_from`),
  ADD KEY `type` (`type`),
  ADD KEY `employer_email` (`employer_email`);




ALTER TABLE `classified_emails_trainings_bk_new`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`message_id`),
  ADD KEY `predicted_tag` (`predicted_tag`),
  ADD KEY `received_on` (`received_on`),
  ADD KEY `email_from` (`email_from`),
  ADD KEY `type` (`type`),
  ADD KEY `employer_email` (`employer_email`);




ALTER TABLE `cms_app_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_content_id` (`cms_content_id`);




ALTER TABLE `cms_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);




ALTER TABLE `cms_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slot_id` (`slot_id`),
  ADD KEY `cms_template_id` (`cms_template_id`),
  ADD KEY `training_campaign_id` (`training_campaign_id`);




ALTER TABLE `cms_contents_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_content_id` (`cms_content_id`),
  ADD KEY `category_id` (`category_id`);




ALTER TABLE `cms_contents_internshala_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_content_id` (`cms_content_id`),
  ADD KEY `cms_app_content_id` (`cms_app_content_id`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `display_counter` (`display_counter`);




ALTER TABLE `cms_contents_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_content_id` (`cms_content_id`),
  ADD KEY `cms_city_id` (`cms_city_id`),
  ADD KEY `cms_state_id` (`cms_state_id`);




ALTER TABLE `cms_content_key_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_content_id` (`cms_content_id`,`cms_template_key_id`),
  ADD KEY `cms_template_key_id` (`cms_template_key_id`);




ALTER TABLE `cms_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);




ALTER TABLE `cms_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);




ALTER TABLE `cms_template_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`name`,`cms_template_id`),
  ADD KEY `cms_template_id` (`cms_template_id`);




ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`),
  ADD KEY `is_processed` (`is_processed`),
  ADD KEY `new_google_college_id` (`google_college_id`);




ALTER TABLE `colleges_aicte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `is_processed` (`is_processed`);




ALTER TABLE `colleges_extended`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Institutes_location_id_idx` (`location_id`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `google_college_first_id` (`google_college_first_id`),
  ADD KEY `college_id` (`college_id`);




ALTER TABLE `college_discount_coupon_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `college_id` (`college_id`),
  ADD KEY `discount_id` (`discount_id`);




ALTER TABLE `college_registrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_key` (`unique_key`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `fk_location_id` (`location_id`),
  ADD KEY `email` (`email`),
  ADD KEY `registered_through` (`registered_through`),
  ADD KEY `designation` (`designation`);




ALTER TABLE `college_registration_bulk_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `college_registration_students_csv_id` (`college_registration_students_csv_id`);




ALTER TABLE `college_registration_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `college_registration_files_college_registration_id` (`college_registration_id`);




ALTER TABLE `college_registration_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tnp_id` (`tnp_id`),
  ADD KEY `email` (`email`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `college_registration_bulk_student_id` (`college_registration_bulk_student_id`);




ALTER TABLE `college_registration_students_csv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tnp_id` (`tnp_id`);




ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_user` (`created_user_id`),
  ADD KEY `name` (`name`),
  ADD KEY `type` (`type`),
  ADD KEY `is_big_brand` (`is_big_brand`);




ALTER TABLE `contest_faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_case_contest_preference_id` (`business_case_contest_preference_id`);




ALTER TABLE `contest_icons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_case_contest_preference_id` (`business_case_contest_preference_id`);




ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));




ALTER TABLE `crons`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `crons_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cron_id` (`cron_id`);




ALTER TABLE `curl_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`(191));




ALTER TABLE `c_emails`
  ADD KEY `domain_name` (`domain_name`);




ALTER TABLE `decline_reasons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `employment_type` (`employment_type`);




ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `courses_name_uq` (`name`),
  ADD KEY `status` (`status`),
  ADD KEY `used_count` (`used_count`);




ALTER TABLE `degree_groups`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `degree_groups_degrees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `degree_group_id` (`degree_group_id`),
  ADD KEY `degree_id` (`degree_id`);




ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupon_code` (`coupon_code`);




ALTER TABLE `emails_to_block`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `email_change_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `old_email` (`old_email`),
  ADD KEY `new_email` (`new_email`),
  ADD KEY `status` (`status`);




ALTER TABLE `email_daily_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `source` (`source`,`type`,`date`);




ALTER TABLE `email_daily_stats_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`,`source`,`type`),
  ADD KEY `email` (`email`);




ALTER TABLE `email_list`
  ADD KEY `email` (`email`);




ALTER TABLE `email_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mail_to` (`mail_to`(767)),
  ADD KEY `type` (`type`),
  ADD KEY `source` (`source`),
  ADD KEY `reply_to` (`reply_to`(767)),
  ADD KEY `subject` (`subject`(191)),
  ADD KEY `attachment_path` (`attachment_path`(767)),
  ADD KEY `is_email_sent` (`is_email_sent`),
  ADD KEY `email_message_id` (`email_message_id`),
  ADD KEY `email_response` (`email_response`(191));




ALTER TABLE `email_tracking_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_client` (`email_client`(767)),
  ADD KEY `opened_at` (`opened_at`),
  ADD KEY `clicked_at` (`clicked_at`),
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `mail_tracker_type` (`mail_tracker_type`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `sent_on` (`sent_on`),
  ADD KEY `sent_on_2` (`sent_on`,`opened_at`),
  ADD KEY `sent_on_3` (`sent_on`,`clicked_at`);




ALTER TABLE `employers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employers_user_id_uq` (`user_id`),
  ADD KEY `authenticated_by_admin_id` (`authenticated_by_admin_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `digest_sent_at` (`digest_sent_at`),
  ADD KEY `registration_source` (`registration_source`);




ALTER TABLE `employers_highlights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internships_id` (`employer_company_id`),
  ADD KEY `profile_id` (`highlight_id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`);




ALTER TABLE `employers_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `notification_id` (`notification_id`);




ALTER TABLE `employer_authentication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `employer_authentication_method_id` (`employer_authentication_method_id`);




ALTER TABLE `employer_authentication_methods`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `employer_bulk_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `processing_status` (`processing_status`);




ALTER TABLE `employer_bulk_request_applications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `employer_bulk_request_id` (`employer_bulk_request_id`);




ALTER TABLE `employer_bulk_request_excel_applications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_excel_request_id` (`employer_bulk_request_id`),
  ADD KEY `application_id` (`application_id`);




ALTER TABLE `employer_cin_verification_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `cin` (`cin`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `token` (`token`);




ALTER TABLE `employer_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_user` (`employer_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `org_details_approved_by_admin_id` (`org_details_approved_by_admin_id`),
  ADD KEY `authentication_type` (`authentication_type`),
  ADD KEY `authentication_status` (`authentication_status`),
  ADD KEY `is_self` (`is_self`),
  ADD KEY `cron_auth_status` (`cron_auth_status`);




ALTER TABLE `employer_companies_auto_saved`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_user` (`employer_id`);




ALTER TABLE `employer_feedbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`);




ALTER TABLE `employer_mail_processing_tracking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `action` (`action`);




ALTER TABLE `employer_social_connects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_user` (`employer_id`),
  ADD KEY `profile_type` (`profile_type`);




ALTER TABLE `error_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `request_url` (`request_url`(767));




ALTER TABLE `error_logs_email`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `request_url` (`request_url`(767));




ALTER TABLE `error_logs_general`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `request_url` (`request_url`(767));




ALTER TABLE `error_logs_sql`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `exam_specific_mailer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `email_2` (`email`);




ALTER TABLE `experience_organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`);




ALTER TABLE `external_internship_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);




ALTER TABLE `flask_access_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`internship_id`);




ALTER TABLE `follow_up_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `follow_up_type` (`name`(1));




ALTER TABLE `gcm_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`gcm_key`),
  ADD KEY `installed_version` (`installed_version`);




ALTER TABLE `gcm_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `gcm_id` (`gcm_key_id`);




ALTER TABLE `gmail_referral_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referral_user_id` (`referral_user_id`),
  ADD KEY `referred_to_email` (`referred_to_email`);




ALTER TABLE `google_applications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_key` (`unique_key`),
  ADD KEY `google_internship_id` (`google_internship_id`),
  ADD KEY `google_student_detail_id` (`google_student_detail_id`);




ALTER TABLE `google_clean_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_processed` (`is_processed`),
  ADD KEY `g_name` (`g_name`(767)),
  ADD KEY `name` (`name`);




ALTER TABLE `google_colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `is_premium_college` (`is_premium_college`);




ALTER TABLE `google_colleges_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `place_id` (`place_id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `google_college_id` (`google_college_id`);




ALTER TABLE `google_college_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `new_google_college_id` (`google_college_id`);




ALTER TABLE `google_companies`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `google_form_res`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `c1` (`c1`);




ALTER TABLE `google_internships`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `google_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `google_company_id` (`google_company_id`),
  ADD KEY `google_company` (`google_company`(767)),
  ADD KEY `internshala_company` (`internshala_company`(767)),
  ADD KEY `company` (`company`),
  ADD KEY `salary_clean` (`salary_clean`);




ALTER TABLE `google_jobs_apply_on`
  ADD PRIMARY KEY (`id`),
  ADD KEY `google_job_id` (`google_job_id`);




ALTER TABLE `google_student_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `home_town` (`home_town`),
  ADD KEY `college_location` (`college_location`);




ALTER TABLE `hall_of_fame_details`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `help_center_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_name` (`category_name`);




ALTER TABLE `help_center_faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_help_center_category_id` (`help_center_category_id`),
  ADD KEY `sort_order` (`sort_order`);




ALTER TABLE `help_center_query_box`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `help_center_query_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_help_center_query_box_id` (`help_center_query_box_id`),
  ADD KEY `email` (`email`);




ALTER TABLE `highlights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);




ALTER TABLE `internshala_devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `device_key` (`device_key`),
  ADD KEY `created_on` (`created_on`);




ALTER TABLE `internshala_device_behaviour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internshala_device_id` (`internshala_device_id`);




ALTER TABLE `internshala_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistent_session` (`persistent_session`,`token`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `token` (`token`),
  ADD KEY `created_on` (`created_on`),
  ADD KEY `session_started_on` (`session_started_on`);




ALTER TABLE `internshala_trainings`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `internshala_view_caches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`,`cache_id`,`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `cache_id_string` (`cache_id_string`),
  ADD KEY `expires_on` (`expires_on`);




ALTER TABLE `internship_day_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_day_signup_id` (`internship_day_signup_id`);




ALTER TABLE `internship_day_participants`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `internship_day_signups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `state` (`state_id`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `location_id` (`location_id`);




ALTER TABLE `internship_day_signups_2018`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `state` (`state_id`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `location_id` (`location_id`);




ALTER TABLE `internship_day_tnps`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_key` (`unique_key`),
  ADD KEY `email` (`email`,`is_reminder_mail_sent`,`utm_source`,`utm_medium`,`utm_campaign`);




ALTER TABLE `internship_day_tnp_guests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_day_signup_id` (`internship_day_tnp_id`),
  ADD KEY `email` (`email`);




ALTER TABLE `internship_stipend`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `salary_performance_scale_id` (`salary_performance_scale_id`);




ALTER TABLE `ip_blocked_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`,`type`,`status`);




ALTER TABLE `ip_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_from` (`ip_from`,`ip_to`);




ALTER TABLE `isps_to_exclude`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `email_2` (`email`);




ALTER TABLE `isp_applications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `isp_version_id_2` (`isp_version_id`,`email`),
  ADD KEY `isp_version_id` (`isp_version_id`),
  ADD KEY `college_id` (`college_id`),
  ADD KEY `college_location_id` (`college_location_id`),
  ADD KEY `degree_id` (`degree_id`),
  ADD KEY `stream_id` (`stream_id`),
  ADD KEY `email` (`email`),
  ADD KEY `email_2` (`email`),
  ADD KEY `referral_isp_application_id` (`referral_isp_application_id`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `isp_group` (`isp_group`),
  ADD KEY `mobile` (`mobile`);




ALTER TABLE `isp_applications1`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `isp_version_id_2` (`isp_version_id`,`email`),
  ADD KEY `isp_version_id` (`isp_version_id`),
  ADD KEY `college_id` (`college_id`),
  ADD KEY `college_location_id` (`college_location_id`),
  ADD KEY `degree_id` (`degree_id`),
  ADD KEY `stream_id` (`stream_id`),
  ADD KEY `email` (`email`),
  ADD KEY `email_2` (`email`),
  ADD KEY `referral_isp_application_id` (`referral_isp_application_id`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `isp_group` (`isp_group`),
  ADD KEY `mobile` (`mobile`);




ALTER TABLE `isp_application_question_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_application_id` (`isp_application_id`),
  ADD KEY `isp_application_question_id` (`isp_question_id`);




ALTER TABLE `isp_completion_certificates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_version_id` (`isp_version_id`);




ALTER TABLE `isp_emails`
  ADD KEY `email` (`email`),
  ADD KEY `new_email` (`new_email`);




ALTER TABLE `isp_faqs`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `isp_groups`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `isp_hiring_general`
  ADD KEY `email` (`email`);




ALTER TABLE `isp_hiring_tier2`
  ADD KEY `email` (`email`);




ALTER TABLE `isp_joining_letters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_version_id` (`isp_version_id`),
  ADD KEY `isp_application_id` (`isp_application_id`),
  ADD KEY `email` (`email`(255));




ALTER TABLE `isp_lists`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `isp_lists_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_list_id` (`isp_list_id`);




ALTER TABLE `isp_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_version_id` (`isp_version_id`);




ALTER TABLE `isp_registration_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_id` (`isp_id`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `isp_result_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_result_page_id` (`isp_result_page_id`);




ALTER TABLE `isp_result_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_version_id` (`isp_version_id`),
  ADD KEY `ack_mail_template_id` (`offer_letter_mail_template_id`),
  ADD KEY `reminder_mail_template_id` (`reminder_mail_template_id`),
  ADD KEY `offer_acceptance_mail_template_id` (`offer_acceptance_mail_template_id`);




ALTER TABLE `isp_share_page_template`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `isp_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isp_version` (`isp_version`);




ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internships_url_uq` (`url`),
  ADD KEY `internships_company_id_idx` (`company_id`),
  ADD KEY `internships_posted_by_idx` (`posted_by`),
  ADD KEY `internships_status_idx` (`status`),
  ADD KEY `internships_work_from_home_idx` (`is_work_from_home`),
  ADD KEY `internships_approved_at_idx` (`approved_at`),
  ADD KEY `internships_expires_at_idx` (`expires_at`),
  ADD KEY `processed_by_admin_id` (`processed_by_admin_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `expires_at` (`expires_at`),
  ADD KEY `clone_internship_id` (`clone_job_id`),
  ADD KEY `assigned_admin` (`assigned_admin_id`),
  ADD KEY `internships_campaign_id` (`campaign_id`),
  ADD KEY `is_applications_from_other_cities_restricted` (`is_applications_from_other_cities_restricted`),
  ADD KEY `internship_closure_status` (`closure_status`),
  ADD KEY `duration_type` (`duration_type`),
  ADD KEY `duration1` (`duration1`),
  ADD KEY `duration2` (`duration2`),
  ADD KEY `duration_scale` (`duration_scale`),
  ADD KEY `part_time_sort_order` (`part_time_sort_order`),
  ADD KEY `type` (`type`),
  ADD KEY `start_date1` (`start_date1`),
  ADD KEY `start_date2` (`start_date2`),
  ADD KEY `is_three_day_zero_application_mail_sent` (`is_three_day_zero_application_mail_sent`),
  ADD KEY `application_count` (`application_count`),
  ADD KEY `employment_type` (`employment_type`),
  ADD KEY `to_show_campaign_jobs_in_search` (`to_show_campaign_jobs_in_search`);




ALTER TABLE `jobs_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `category_id` (`category_id`);




ALTER TABLE `jobs_degree_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `degree_group_id` (`degree_group_id`);




ALTER TABLE `jobs_highlights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internships_id` (`job_id`),
  ADD KEY `profile_id` (`highlight_id`),
  ADD KEY `status` (`status`),
  ADD KEY `description` (`description`(767));




ALTER TABLE `jobs_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internships_locations_internship_fk` (`job_id`),
  ADD KEY `location_id` (`location_id`);




ALTER TABLE `jobs_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `value` (`value`(767)),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `job_metas_type_value_index` (`type`,`value`(767));




ALTER TABLE `jobs_not_hired_reasons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `reason_id` (`reason_id`);




ALTER TABLE `jobs_perks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`perk`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `jobs_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `is_primary_profile` (`is_primary_profile`);




ALTER TABLE `jobs_segments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `segment` (`segment`);




ALTER TABLE `jobs_share_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `jobs_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internships_skills_internship_id_idx` (`job_id`),
  ADD KEY `internships_skills_skill_id_idx` (`skill_id`);




ALTER TABLE `jobs_status_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `processed_by_admin_id` (`processed_by_admin_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`);




ALTER TABLE `jobs_streams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `stream_id` (`stream_id`);




ALTER TABLE `jobs_titles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `title_id` (`title_id`);




ALTER TABLE `jobs_to_evaluate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `jobs_transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaction_id_2` (`transaction_id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `payment_gateway_id` (`payment_gateway_id`),
  ADD KEY `status` (`status`),
  ADD KEY `payment_error_code_id` (`payment_error_code_id`);




ALTER TABLE `jobs_view_application_click_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `job_access_application_till_referers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `job_auto_save`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_key` (`unique_key`),
  ADD KEY `employer_id` (`employer_id`);




ALTER TABLE `job_campus_ambassador_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `job_detail_additional_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `job_extended`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `recommendation_computed_on` (`recommendation_computed_on`),
  ADD KEY `draft_reminder` (`draft_reminder`),
  ADD KEY `last_job_transaction_id` (`last_job_transaction_id`);




ALTER TABLE `job_follow_up_mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `job_follow_up_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `follow_up_type_id` (`follow_up_type_id`);




ALTER TABLE `job_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `clone_internship_id` (`clone_job_id`),
  ADD KEY `salary_performance_scale_id` (`salary_performance_scale_id`),
  ADD KEY `profile_id` (`profile_id`);




ALTER TABLE `job_original_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `admin_id` (`admin_id`);




ALTER TABLE `job_rejection_mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `job_rejection_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `rejection_type_id` (`rejection_type_id`);




ALTER TABLE `job_salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`);




ALTER TABLE `job_year_of_completions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_year_of_completions_internship` (`job_id`);




ALTER TABLE `junk_boards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_board_id` (`approved_board_id`);




ALTER TABLE `junk_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_category_id` (`approved_category_id`);




ALTER TABLE `junk_colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_institute_id` (`approved_college_id`),
  ADD KEY `junk_name` (`junk_name`);




ALTER TABLE `junk_degrees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_degree_id` (`approved_degree_id`);




ALTER TABLE `junk_email_domains`
  ADD PRIMARY KEY (`id`),
  ADD KEY `junk_domain_name` (`junk_domain_name`),
  ADD KEY `approved_domain_name` (`approved_domain_name`);




ALTER TABLE `junk_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_profile_id` (`approved_profile_id`);




ALTER TABLE `junk_schools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_school_id` (`approved_school_id`);




ALTER TABLE `junk_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_skill_id` (`approved_skill_id`),
  ADD KEY `junk_name` (`junk_name`);




ALTER TABLE `junk_streams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_stream_id` (`approved_stream_id`),
  ADD KEY `junk_name` (`junk_name`);




ALTER TABLE `junk_trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_training_id` (`approved_training_id`);




ALTER TABLE `karix_sms_responses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karix_request_id` (`karix_request_id`),
  ADD KEY `dest` (`dest`);




ALTER TABLE `karix_sms_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile_number` (`mobile_number`),
  ADD KEY `response_code` (`karix_request_id`),
  ADD KEY `type` (`type`),
  ADD KEY `sms_response_code_id` (`sms_response_code_id`);




ALTER TABLE `linkedin_industry_codes`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `linkedin_job_functions`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `localities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UniqueField` (`name`);




ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_locality_id` (`sublocality_level_1_id`),
  ADD KEY `locality_id` (`locality_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `sublocality_level_2_id` (`sublocality_level_2_id`),
  ADD KEY `administrative_area_level_2_id` (`administrative_area_level_2_id`),
  ADD KEY `administrative_area_level_1_id` (`administrative_area_level_1_id`),
  ADD KEY `name` (`name`),
  ADD KEY `use_count` (`use_count`),
  ADD KEY `place_id` (`place_id`);




ALTER TABLE `location_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `google_locality_name` (`google_name`),
  ADD KEY `approved_locality_name` (`approved_name`);




ALTER TABLE `mail_emails`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `mail_history`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `mail_queue`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `mail_queue_general`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `mail_senders_receivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mail_email_id` (`mail_email_id`),
  ADD KEY `mail_type_id` (`mail_type_id`);




ALTER TABLE `mail_signatures`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);




ALTER TABLE `mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `mail_type_id` (`mail_type_id`),
  ADD KEY `mail_signature_id` (`mail_signature_id`);




ALTER TABLE `mail_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);




ALTER TABLE `mca_crawl_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cin` (`cin`);




ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `noc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `tnp_id` (`tnp_id`);




ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `type` (`type`),
  ADD KEY `display_location` (`display_location`);




ALTER TABLE `not_hired_reasons`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `out_of_office_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`out_of_office_date`);




ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pagename` (`pagename`);




ALTER TABLE `payment_error_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_2` (`code`),
  ADD KEY `code` (`code`);




ALTER TABLE `personal_email_domains`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain_name` (`domain_name`);




ALTER TABLE `professionalism_contests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_reporting_reason_id` (`application_reporting_reason_id`);




ALTER TABLE `professionalism_report_reasons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reason` (`reason`),
  ADD KEY `sort_preference` (`sort_preference`);




ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `name_2` (`name`),
  ADD KEY `status` (`status`);




ALTER TABLE `profiles_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_group_id` (`category_id`),
  ADD KEY `profile_id` (`profile_id`);




ALTER TABLE `profile_groups`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `profile_groups_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_group_id` (`profile_group_id`),
  ADD KEY `profile_id` (`profile_id`);




ALTER TABLE `rating_expected_behaviour_faqs`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `rating_levels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sort_order` (`sort_order`);




ALTER TABLE `rating_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rating_level_id` (`rating_level_id`),
  ADD KEY `rating_question_set_id` (`rating_question_set_id`);




ALTER TABLE `rating_questions_helpers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rating_question_id` (`rating_question_id`),
  ADD KEY `type` (`type`);




ALTER TABLE `rating_question_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rating_question_id` (`rating_question_id`);




ALTER TABLE `rating_question_sets`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `rating_quiz_student_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rating_level_id` (`rating_level_id`),
  ADD KEY `rating_question_set_id` (`rating_question_set_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`);




ALTER TABLE `rating_quiz_student_sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `rating_level_id` (`rating_level_id`);




ALTER TABLE `rating_student_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `rating_quiz_student_score_id` (`rating_quiz_student_score_id`),
  ADD KEY `rating_question_option_id` (`rating_question_option_id`);




ALTER TABLE `recommendations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internship_id_2` (`job_id`,`student_id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `score` (`score`),
  ADD KEY `status` (`status`),
  ADD KEY `computed_on` (`computed_on`);




ALTER TABLE `recommendations_advanced_search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`);




ALTER TABLE `recommendations_decline_reasons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recommendation_id` (`recommendation_id`),
  ADD KEY `decline_reason_id` (`decline_reason_id`);




ALTER TABLE `recommendations_intermediate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internship_students_recommendations_intermediate_combo` (`job_id`,`student_id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `score` (`score`),
  ADD KEY `status` (`is_processed`);




ALTER TABLE `recommendations_intermediate_job`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internship_students_recommendations_intermediate_combo` (`job_id`,`student_id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `score` (`score`),
  ADD KEY `status` (`is_processed`);




ALTER TABLE `recommendations_job`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internship_id_2` (`job_id`,`student_id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `score` (`score`),
  ADD KEY `status` (`status`);




ALTER TABLE `recommendations_view_source`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`);




ALTER TABLE `recommendation_dumps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`job_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `score` (`score`),
  ADD KEY `status` (`is_processed`);




ALTER TABLE `rejection_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rejection_type` (`name`(1));




ALTER TABLE `remember_me_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `remember_me_tokens_combined_uq` (`persistant_key`,`token`),
  ADD KEY `remember_me_tokens_users_fk` (`user_id`);




ALTER TABLE `resume_detail_sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail` (`detail`),
  ADD KEY `detail_id` (`detail_id`),
  ADD KEY `source` (`source`);




ALTER TABLE `resume_quiz_questions_answers`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `resume_quiz_user_details`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `ribbon_messages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `url` (`url`),
  ADD KEY `type` (`type`),
  ADD KEY `priority` (`priority`),
  ADD KEY `location` (`location`),
  ADD KEY `status` (`status`);




ALTER TABLE `rsa_keys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_id` (`device_id`);




ALTER TABLE `salary_performance_scales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `scale` (`scale`);




ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(255)),
  ADD KEY `status` (`status`);




ALTER TABLE `server_time`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `shortened_urls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shortened_url` (`shortened_url`),
  ADD UNIQUE KEY `shortened_url_2` (`shortened_url`,`url`);




ALTER TABLE `shortened_url_csv`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `shortened_url_csv_shortened_urls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shortened_url_id` (`shortened_url_id`),
  ADD KEY `shortened_url_csv_shortened_urls_ibfk_1` (`shortened_url_csv_id`);




ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`);




ALTER TABLE `skills_trainings_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skill_id` (`skill_id`);




ALTER TABLE `slider_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banner_template_id` (`banner_template_id`);




ALTER TABLE `slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `slot_num` (`slot_num`);




ALTER TABLE `sms_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sms_list_id` (`sms_list_id`);




ALTER TABLE `sms_events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sms_lists_contact_id` (`sms_lists_contact_id`,`sms_campaign_id`),
  ADD KEY `sms_campaign_id` (`sms_campaign_id`);




ALTER TABLE `sms_lists`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `sms_lists_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sms_list_id` (`sms_list_id`),
  ADD KEY `number` (`number`);




ALTER TABLE `sms_lists_contacts_csv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sms_list_id` (`sms_list_id`);




ALTER TABLE `sms_queue_general`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `sms_referral_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referral_user_id` (`referral_user_id`);




ALTER TABLE `sms_response_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_2` (`code`),
  ADD KEY `code` (`code`);




ALTER TABLE `sms_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile_number` (`mobile_number`),
  ADD KEY `response_code` (`response_code`),
  ADD KEY `type` (`type`);




ALTER TABLE `stats_count`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entity` (`entity`);




ALTER TABLE `stipend_prediction_internships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`internship_id`),
  ADD KEY `profile_name` (`profile_name`),
  ADD KEY `location` (`location`),
  ADD KEY `stipend` (`stipend`);




ALTER TABLE `stipend_prediction_recommendations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internship_id` (`job_id`);




ALTER TABLE `streams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`);




ALTER TABLE `stream_degree_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discount_id` (`discount_id`),
  ADD KEY `degree_id` (`degree_id`),
  ADD KEY `stream_id` (`stream_id`);




ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_user_id_uq` (`user_id`),
  ADD UNIQUE KEY `students_unique_key_uq` (`unique_key`),
  ADD KEY `current_city` (`current_city`),
  ADD KEY `second_city_of_residence` (`second_city_of_residence`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `student_digest_sent_on` (`student_digest_sent_on`);




ALTER TABLE `students_activation_tracker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);




ALTER TABLE `students_certificates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`);




ALTER TABLE `students_colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `college_id` (`college_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `degree_id` (`degree_id`),
  ADD KEY `stream_id` (`stream_id`),
  ADD KEY `degree_type` (`degree_type`),
  ADD KEY `end_year` (`end_year`);




ALTER TABLE `students_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `notification_id` (`notification_id`);




ALTER TABLE `students_schools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `standard_type` (`standard_type`),
  ADD KEY `board_id` (`board_id`);




ALTER TABLE `students_skills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_skills_combo_uq` (`student_id`,`skill_id`),
  ADD KEY `students_skills_skill_id_idx` (`skill_id`),
  ADD KEY `students_skills_student_id_idx` (`student_id`);




ALTER TABLE `student_additional_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);




ALTER TABLE `student_badges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `internshala_training_id` (`internshala_training_id`);




ALTER TABLE `student_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_campaigns_campaign_id` (`campaign_id`);




ALTER TABLE `student_digest_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gcm_user_id` (`gcm_user_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `created_on` (`created_on`),
  ADD KEY `preference_type` (`preference_type`);




ALTER TABLE `student_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_fk` (`student_id`),
  ADD KEY `experience_organization_id_fk` (`experience_organization_id`),
  ADD KEY `profile_id_fk` (`profile_id`),
  ADD KEY `location_id_fk` (`location_id`),
  ADD KEY `experience_type` (`experience_type`);




ALTER TABLE `student_extended`
  ADD PRIMARY KEY (`id`),
  ADD KEY `internship_id` (`student_id`);




ALTER TABLE `student_invitation_counts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `open_invites_job` (`open_invites_job`);




ALTER TABLE `student_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`(767)),
  ADD KEY `student_id` (`student_id`);




ALTER TABLE `student_position_of_responsibilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);




ALTER TABLE `student_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `end_month` (`end_month`);




ALTER TABLE `student_registration_custom_messages`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `student_trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_fk1` (`student_id`),
  ADD KEY `experience_organization_id_fk1` (`experience_organization_id`),
  ADD KEY `training_id_fk1` (`training_id`),
  ADD KEY `location_id_fk1` (`location_id`),
  ADD KEY `end_date` (`end_date`);




ALTER TABLE `student_work_samples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `type` (`type`);




ALTER TABLE `sublocality_levels_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));




ALTER TABLE `sublocality_levels_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));




ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `subscribers_status_idx` (`status`),
  ADD KEY `subscribers_digest_sent_on_idx` (`digest_sent_on`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `digest_sent_on` (`digest_sent_on`);




ALTER TABLE `subscribers_digest_count`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscriber_id` (`subscriber_id`),
  ADD KEY `preferences_count` (`preferences_count`),
  ADD KEY `application_history` (`application_history_count`),
  ADD KEY `profile` (`profile_count`);




ALTER TABLE `subscribers_status_trigger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `is_processed` (`is_processed`),
  ADD KEY `subscriber_id` (`subscriber_id`),
  ADD KEY `email` (`email`),
  ADD KEY `new_status` (`new_status`);




ALTER TABLE `subscribers_status_trigger_dump`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `suppressed_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_key` (`unique_key`) USING BTREE,
  ADD KEY `email` (`email`),
  ADD KEY `status` (`status`);




ALTER TABLE `survey_experiences`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `team_members_page`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `temp_autocomplete_count`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `temp_birthday_registrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);




ALTER TABLE `temp_birthday_values`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `temp_blocked_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `temp_blocked_employer_detail_id` (`blocked_employer_detail_id_old`),
  ADD KEY `message_id` (`chat_message_id`),
  ADD KEY `temp_blocked_employer_details_new_id` (`blocked_employer_detail_id`);




ALTER TABLE `temp_blocked_employer_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`blocked_employer_email_id`),
  ADD KEY `type` (`type`),
  ADD KEY `value` (`value`(767)),
  ADD KEY `start_at` (`start_at`),
  ADD KEY `end_at` (`end_at`);




ALTER TABLE `temp_blocked_employer_details_new`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`blocked_employer_email_id`),
  ADD KEY `type` (`type`),
  ADD KEY `value` (`value`(767)),
  ADD KEY `start_at` (`start_at`),
  ADD KEY `end_at` (`end_at`);




ALTER TABLE `temp_blocked_employer_email`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_id` (`employer_id`),
  ADD KEY `employer_id_2` (`employer_id`);




ALTER TABLE `temp_chat_message_response_analysis`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `temp_chat_message_response_analysis_2`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `temp_college_discount_coupon_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `college_id` (`college_id`),
  ADD KEY `discount_id` (`discount_id`);




ALTER TABLE `temp_crs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tnp_id` (`tnp_id`),
  ADD KEY `student_email` (`student_email`);




ALTER TABLE `temp_crs_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_email` (`student_email`),
  ADD KEY `tnp_email` (`tnp_email`);




ALTER TABLE `temp_domain`
  ADD KEY `email` (`email`(767));




ALTER TABLE `temp_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);




ALTER TABLE `temp_emails_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);




ALTER TABLE `temp_isp_coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupon` (`coupon`),
  ADD KEY `coupon_2` (`coupon`);




ALTER TABLE `temp_spam_test`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`,`status`);




ALTER TABLE `titles_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `title_id` (`title_id`);




ALTER TABLE `tnp`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `institute_id` (`college_id`);




ALTER TABLE `tnps_hired_students_applications_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tnp_id` (`tnp_id`),
  ADD KEY `application_id` (`application_id`);




ALTER TABLE `tnp_college_rank_csvs`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`);




ALTER TABLE `training_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);




ALTER TABLE `training_contests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `training_contest_detail_id` (`training_contest_detail_id`);




ALTER TABLE `training_contest_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `training_url` (`training_url`),
  ADD UNIQUE KEY `contest_url` (`contest_url`);




ALTER TABLE `urls_to_index`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uq` (`email`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`),
  ADD KEY `unique_key` (`unique_key`),
  ADD KEY `password_hash_algo` (`password_hash_algo`);




ALTER TABLE `users_app_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gcm_user_id` (`user_id`),
  ADD KEY `notification_type` (`notification_type`);




ALTER TABLE `user_autocomplete_request_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_3` (`user_id`,`type`,`ip2long`),
  ADD KEY `type` (`type`,`ip2long`),
  ADD KEY `user_id_2` (`user_id`,`type`,`ip`);




ALTER TABLE `user_blocked_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `user_block_reasons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `block_type` (`block_type`);




ALTER TABLE `user_otps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`,`otp`,`country_code`,`phone`);




ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `user_preferences_availabilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_preference_id` (`user_preference_id`),
  ADD KEY `type` (`type`);




ALTER TABLE `user_preferences_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_preference_id` (`user_preference_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `preference_number` (`preference_number`);




ALTER TABLE `user_preferences_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_preference_id` (`user_preference_id`),
  ADD KEY `location_id_2` (`location_id`),
  ADD KEY `preference_number` (`preference_number`);




ALTER TABLE `user_preference_seen_stats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session_id` (`session_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `session_id_2` (`session_id`);




ALTER TABLE `user_registration_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);




ALTER TABLE `user_request_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `type` (`type`,`status`),
  ADD KEY `type_2` (`type`,`status`,`ip`);




ALTER TABLE `user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `internshala_session_id` (`internshala_session_id`),
  ADD KEY `source` (`source`);




ALTER TABLE `vtc_notifications`
  ADD PRIMARY KEY (`id`);