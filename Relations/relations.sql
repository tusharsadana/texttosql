ALTER TABLE `account_delete_history`
  ADD CONSTRAINT `account_delete_history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `additional_infos`
  ADD CONSTRAINT `additional_infos_internships_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `admins_admin_privileges`
  ADD CONSTRAINT `admins_admin_privileges_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `admins_daily_plan`
  ADD CONSTRAINT `admins_daily_plan_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `admins_internships_tracker`
  ADD CONSTRAINT `admins_internships_tracker_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `admin_em_lists_queue`
  ADD CONSTRAINT `admin_em_lists_queue_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `admin_privileges_admin_urls`
  ADD CONSTRAINT `admin_privileges_admin_urls_ibfk_1` FOREIGN KEY (`admin_privilege_id`) REFERENCES `admin_privileges` (`id`),
  ADD CONSTRAINT `admin_privileges_admin_urls_ibfk_2` FOREIGN KEY (`admin_url_id`) REFERENCES `admin_urls` (`id`);




ALTER TABLE `admin_uploads`
  ADD CONSTRAINT `admin_uploads_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `advanced_search`
  ADD CONSTRAINT `advanced_search_internship_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `applications`
  ADD CONSTRAINT `applications_internship_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `applications_students_fk` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `applications_additional_infos`
  ADD CONSTRAINT `applications_additional_infos_applications_fk` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `applications_additional_infos_ibfk_1` FOREIGN KEY (`additional_info_id`) REFERENCES `additional_infos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `applications_evaluation`
  ADD CONSTRAINT `applications_evalution_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `applications_evalution_ibfk_2` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `applications_new`
  ADD CONSTRAINT `applications_new_ibfk_1` FOREIGN KEY (`internship_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `applications_new_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `applications_similar_jobs_data`
  ADD CONSTRAINT `applications_similar_jobs_data_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `applications_similar_jobs_data_ibfk_2` FOREIGN KEY (`similar_job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `applications_soft_seen_data`
  ADD CONSTRAINT `applications_soft_seen_data_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`);




ALTER TABLE `applications_status_queue`
  ADD CONSTRAINT `applications_status_queue_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`);




ALTER TABLE `application_form_views`
  ADD CONSTRAINT `application_form_views_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `application_form_views_ibfk_2` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `application_reporting_reasons`
  ADD CONSTRAINT `application_reporting_reasons_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `application_reporting_reasons_ibfk_2` FOREIGN KEY (`professionalism_report_reason_id`) REFERENCES `professionalism_report_reasons` (`id`);




ALTER TABLE `app_notifications`
  ADD CONSTRAINT `app_notifications_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `app_notification_gcm_keys`
  ADD CONSTRAINT `app_notification_gcm_keys_ibfk_1` FOREIGN KEY (`app_notification_id`) REFERENCES `app_notifications` (`id`),
  ADD CONSTRAINT `app_notification_gcm_keys_ibfk_2` FOREIGN KEY (`gcm_key_id`) REFERENCES `gcm_keys` (`id`);




ALTER TABLE `app_rating_dialogs`
  ADD CONSTRAINT `app_rating_dialogs_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `banner_key_values`
  ADD CONSTRAINT `banner_key_values_ibfk_1` FOREIGN KEY (`slider_banner_id`) REFERENCES `slider_banners` (`id`),
  ADD CONSTRAINT `banner_key_values_ibfk_2` FOREIGN KEY (`banner_template_key_id`) REFERENCES `banner_template_keys` (`id`);




ALTER TABLE `banner_template_keys`
  ADD CONSTRAINT `banner_template_keys_ibfk_1` FOREIGN KEY (`banner_template_id`) REFERENCES `banner_templates` (`id`);




ALTER TABLE `business_case_contests`
  ADD CONSTRAINT `business_case_contests_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `business_case_contests_ibfk_2` FOREIGN KEY (`preference`) REFERENCES `business_case_contest_preferences` (`id`),
  ADD CONSTRAINT `business_case_contests_ibfk_3` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `business_case_contest_certificates`
  ADD CONSTRAINT `business_case_contest_certificates_ibfk_1` FOREIGN KEY (`business_case_contest_preference_id`) REFERENCES `business_case_contest_preferences` (`id`),
  ADD CONSTRAINT `business_case_contest_certificates_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `campaigns`
  ADD CONSTRAINT `campaigns_acknowledge_mail_template_id` FOREIGN KEY (`acknowledgement_mail_template_id`) REFERENCES `mail_templates` (`id`);




ALTER TABLE `campaigns_users`
  ADD CONSTRAINT `campaigns_users_ibfk_1` FOREIGN KEY (`referral_campaign_user_id`) REFERENCES `campaigns_users` (`id`),
  ADD CONSTRAINT `campaign_entries_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`),
  ADD CONSTRAINT `campaign_entries_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `campaign_experiment`
  ADD CONSTRAINT `campaign_experiment_ibfk_1` FOREIGN KEY (`campaign_user_id`) REFERENCES `campaigns_users` (`id`);




ALTER TABLE `campaign_fair_application_details`
  ADD CONSTRAINT `campaign_fair_application_details_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`),
  ADD CONSTRAINT `campaign_fair_application_details_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`);




ALTER TABLE `campaign_fair_details`
  ADD CONSTRAINT `campaign_fair_details_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);




ALTER TABLE `campaign_fair_interviewees`
  ADD CONSTRAINT `campaign_fair_interviewees_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `campaign_fair_interviewees_ibfk_2` FOREIGN KEY (`campaign_fair_detail_id`) REFERENCES `campaign_user_fair_details` (`id`);




ALTER TABLE `campaign_fair_interviewees_slots`
  ADD CONSTRAINT `campaign_fair_interviewees_slots_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `campaign_fair_interviewees_slots_ibfk_2` FOREIGN KEY (`campaign_fair_interviewee_id`) REFERENCES `campaign_fair_interviewees` (`id`);




ALTER TABLE `campaign_faqs`
  ADD CONSTRAINT `campaign_faqs_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);




ALTER TABLE `campaign_mail_templates`
  ADD CONSTRAINT `campaign_mail_templates_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`),
  ADD CONSTRAINT `campaign_mail_templates_ibfk_2` FOREIGN KEY (`mail_template_id`) REFERENCES `mail_templates` (`id`);




ALTER TABLE `campaign_sections`
  ADD CONSTRAINT `campaign_sections_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);




ALTER TABLE `campaign_tnps`
  ADD CONSTRAINT `campaign_tnps_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`),
  ADD CONSTRAINT `campaign_tnps_ibfk_2` FOREIGN KEY (`college_location_id`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `campaign_tnps_ibfk_3` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`);




ALTER TABLE `campaign_users_additional_info`
  ADD CONSTRAINT `campaign_entries_additional_info_campaign_entry_id` FOREIGN KEY (`campaign_user_id`) REFERENCES `campaigns_users` (`id`);




ALTER TABLE `campaign_user_fair_details`
  ADD CONSTRAINT `campaign_user_fair_details_ibfk_1` FOREIGN KEY (`campaign_user_id`) REFERENCES `campaigns_users` (`id`);




ALTER TABLE `campaign_user_referral_details`
  ADD CONSTRAINT `campaign_user_referral_details_ibfk_1` FOREIGN KEY (`campaign_user_id`) REFERENCES `campaigns_users` (`id`);




ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`linkedin_job_function_id`) REFERENCES `linkedin_job_functions` (`id`),
  ADD CONSTRAINT `categories_ibfk_3` FOREIGN KEY (`linkedin_industry_code_id`) REFERENCES `linkedin_industry_codes` (`id`);




ALTER TABLE `categories_streams`
  ADD CONSTRAINT `streams_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `streams_categories_ibfk_2` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`id`);




ALTER TABLE `category_scores`
  ADD CONSTRAINT `category_scores_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);




ALTER TABLE `certificates`
  ADD CONSTRAINT `certificates_ibfk_1` FOREIGN KEY (`certificate_campaign_id`) REFERENCES `certificate_campaigns` (`id`);




ALTER TABLE `chatbot_logs`
  ADD CONSTRAINT `chatbot_logs_ibfk_1` FOREIGN KEY (`internshala_session_id`) REFERENCES `internshala_sessions` (`id`),
  ADD CONSTRAINT `chatbot_requests_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `chat_conversations`
  ADD CONSTRAINT `chat_conversations_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`);




ALTER TABLE `chat_conversation_extended`
  ADD CONSTRAINT `chat_conversation_extended_ibfk_1` FOREIGN KEY (`chat_conversation_id`) REFERENCES `chat_conversations` (`id`);




ALTER TABLE `chat_emails_sent`
  ADD CONSTRAINT `chat_emails_sent_ibfk_1` FOREIGN KEY (`chat_conversation_id`) REFERENCES `chat_conversations` (`id`),
  ADD CONSTRAINT `chat_emails_sent_ibfk_2` FOREIGN KEY (`chat_message_id`) REFERENCES `chat_messages` (`id`);




ALTER TABLE `chat_messages`
  ADD CONSTRAINT `chat_messages_ibfk_1` FOREIGN KEY (`chat_conversation_id`) REFERENCES `chat_conversations` (`id`);




ALTER TABLE `chat_message_thumbnails`
  ADD CONSTRAINT `chat_message_thumbnails_ibfk_1` FOREIGN KEY (`chat_message_id`) REFERENCES `chat_messages` (`id`);




ALTER TABLE `chat_users_presence`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `city_clusters_cities`
  ADD CONSTRAINT `city_clusters_cities_ibfk_1` FOREIGN KEY (`city_cluster_id`) REFERENCES `city_clusters` (`id`),
  ADD CONSTRAINT `city_clusters_cities_ibfk_2` FOREIGN KEY (`locality_id`) REFERENCES `localities` (`id`);




ALTER TABLE `city_specific_discounts`
  ADD CONSTRAINT `city_specific_discounts_ibfk_1` FOREIGN KEY (`discount_id`) REFERENCES `trainings`.`discounts` (`id`);




ALTER TABLE `ci_sessions`
  ADD CONSTRAINT `ci_sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `cms_app_contents`
  ADD CONSTRAINT `cms_app_contents_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`);




ALTER TABLE `cms_contents`
  ADD CONSTRAINT `cms_contents_ibfk_1` FOREIGN KEY (`slot_id`) REFERENCES `slots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cms_contents_ibfk_2` FOREIGN KEY (`cms_template_id`) REFERENCES `cms_templates` (`id`),
  ADD CONSTRAINT `cms_contents_ibfk_3` FOREIGN KEY (`training_campaign_id`) REFERENCES `training_campaigns` (`id`);




ALTER TABLE `cms_contents_internshala_devices`
  ADD CONSTRAINT `cms_contents_internshala_devices_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`),
  ADD CONSTRAINT `cms_contents_internshala_devices_ibfk_2` FOREIGN KEY (`cms_app_content_id`) REFERENCES `cms_app_contents` (`id`),
  ADD CONSTRAINT `cms_contents_internshala_devices_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `internshala_devices` (`id`);




ALTER TABLE `cms_contents_locations`
  ADD CONSTRAINT `cms_contents_locations_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`),
  ADD CONSTRAINT `cms_contents_locations_ibfk_2` FOREIGN KEY (`cms_city_id`) REFERENCES `cms_cities` (`id`),
  ADD CONSTRAINT `cms_contents_locations_ibfk_3` FOREIGN KEY (`cms_state_id`) REFERENCES `cms_states` (`id`);




ALTER TABLE `cms_content_key_values`
  ADD CONSTRAINT `cms_content_key_values_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`),
  ADD CONSTRAINT `cms_content_key_values_ibfk_2` FOREIGN KEY (`cms_template_key_id`) REFERENCES `cms_template_keys` (`id`);




ALTER TABLE `cms_template_keys`
  ADD CONSTRAINT `cms_template_keys_ibfk_1` FOREIGN KEY (`cms_template_id`) REFERENCES `cms_templates` (`id`);




ALTER TABLE `colleges`
  ADD CONSTRAINT `colleges_ibfk_3` FOREIGN KEY (`google_college_id`) REFERENCES `google_colleges` (`id`);




ALTER TABLE `colleges_extended`
  ADD CONSTRAINT `colleges_extended_ibfk_1` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`);




ALTER TABLE `college_discount_coupon_codes`
  ADD CONSTRAINT `college_discount_coupon_codes_ibfk_1` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`),
  ADD CONSTRAINT `college_discount_coupon_codes_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `trainings`.`discounts` (`id`);




ALTER TABLE `college_registrations`
  ADD CONSTRAINT `college_registrations_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `administrative_area_levels_1` (`id`),
  ADD CONSTRAINT `college_registrations_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`);




ALTER TABLE `college_registration_bulk_students`
  ADD CONSTRAINT `college_registration_bulk_students_ibfk_1` FOREIGN KEY (`college_registration_students_csv_id`) REFERENCES `college_registration_students_csv` (`id`);




ALTER TABLE `college_registration_files`
  ADD CONSTRAINT `college_registration_files_college_registration_id` FOREIGN KEY (`college_registration_id`) REFERENCES `college_registrations` (`id`);




ALTER TABLE `college_registration_students`
  ADD CONSTRAINT `college_registration_students_ibfk_1` FOREIGN KEY (`tnp_id`) REFERENCES `tnp` (`id`),
  ADD CONSTRAINT `college_registration_students_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `college_registration_students_ibfk_3` FOREIGN KEY (`college_registration_bulk_student_id`) REFERENCES `college_registration_bulk_students` (`id`);




ALTER TABLE `college_registration_students_csv`
  ADD CONSTRAINT `college_registration_students_csv_ibfk_1` FOREIGN KEY (`tnp_id`) REFERENCES `tnp` (`id`);




ALTER TABLE `companies`
  ADD CONSTRAINT `companies_ibfk_1` FOREIGN KEY (`created_user_id`) REFERENCES `users` (`id`);




ALTER TABLE `contest_faqs`
  ADD CONSTRAINT `contest_faqs_ibfk_1` FOREIGN KEY (`business_case_contest_preference_id`) REFERENCES `business_case_contest_preferences` (`id`);




ALTER TABLE `contest_icons`
  ADD CONSTRAINT `contest_icons_ibfk_1` FOREIGN KEY (`business_case_contest_preference_id`) REFERENCES `business_case_contest_preferences` (`id`);




ALTER TABLE `crons_stats`
  ADD CONSTRAINT `crons_stats_ibfk_1` FOREIGN KEY (`cron_id`) REFERENCES `crons` (`id`);




ALTER TABLE `decline_reasons`
  ADD CONSTRAINT `decline_reasons_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `decline_reasons` (`id`);




ALTER TABLE `degree_groups_degrees`
  ADD CONSTRAINT `degree_groups_degrees_ibfk_1` FOREIGN KEY (`degree_group_id`) REFERENCES `degree_groups` (`id`),
  ADD CONSTRAINT `degree_groups_degrees_ibfk_2` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`);




ALTER TABLE `email_change_requests`
  ADD CONSTRAINT `email_change_requests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `email_tracking_stats`
  ADD CONSTRAINT `email_tracking_stats_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `employers`
  ADD CONSTRAINT `employers_ibfk_1` FOREIGN KEY (`authenticated_by_admin_id`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `employers_ibfk_2` FOREIGN KEY (`internshala_session_id`) REFERENCES `internshala_sessions` (`id`),
  ADD CONSTRAINT `employers_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `employers_highlights`
  ADD CONSTRAINT `employers_highlights_ibfk_1` FOREIGN KEY (`employer_company_id`) REFERENCES `employer_companies` (`id`),
  ADD CONSTRAINT `employers_highlights_ibfk_2` FOREIGN KEY (`highlight_id`) REFERENCES `highlights` (`id`),
  ADD CONSTRAINT `employers_highlights_ibfk_3` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `employers_notifications`
  ADD CONSTRAINT `employers_notifications_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`),
  ADD CONSTRAINT `employers_notifications_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`);




ALTER TABLE `employer_authentication`
  ADD CONSTRAINT `employer_authentication_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `employer_bulk_requests`
  ADD CONSTRAINT `employer_id_fk` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`),
  ADD CONSTRAINT `internship_id_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `employer_bulk_request_applications`
  ADD CONSTRAINT `employer_bulk_request_applications_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `employer_bulk_request_applications_ibfk_1` FOREIGN KEY (`employer_bulk_request_id`) REFERENCES `employer_bulk_requests` (`id`);




ALTER TABLE `employer_bulk_request_excel_applications`
  ADD CONSTRAINT `application_id_fk` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `employer_bulk_request_excel_applications_ibfk_1` FOREIGN KEY (`employer_bulk_request_id`) REFERENCES `employer_bulk_requests` (`id`),
  ADD CONSTRAINT `employer_excel_request_id_fk` FOREIGN KEY (`employer_bulk_request_id`) REFERENCES `employer_bulk_requests` (`id`);




ALTER TABLE `employer_cin_verification_requests`
  ADD CONSTRAINT `employer_cin_verification_requests_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `employer_companies`
  ADD CONSTRAINT `employer_companies_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`),
  ADD CONSTRAINT `employer_companies_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `employer_companies_ibfk_3` FOREIGN KEY (`org_details_approved_by_admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `employer_companies_auto_saved`
  ADD CONSTRAINT `employer_companies_auto_saved_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `employer_feedbacks`
  ADD CONSTRAINT `employer_feedbacks_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `employer_mail_processing_tracking`
  ADD CONSTRAINT `employer_mail_processing_tracking_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`);




ALTER TABLE `employer_social_connects`
  ADD CONSTRAINT `employer_social_connects_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `error_logs`
  ADD CONSTRAINT `error_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `error_logs_general`
  ADD CONSTRAINT `error_logs_general_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `external_internship_companies`
  ADD CONSTRAINT `external_internship_companies_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);




ALTER TABLE `flask_access_logs`
  ADD CONSTRAINT `flask_access_logs_ibfk_1` FOREIGN KEY (`internship_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `gcm_users`
  ADD CONSTRAINT `gcm_users_ibfk_1` FOREIGN KEY (`gcm_key_id`) REFERENCES `gcm_keys` (`id`),
  ADD CONSTRAINT `gcm_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `gmail_referral_logs`
  ADD CONSTRAINT `gmail_referral_logs_ibfk_1` FOREIGN KEY (`referral_user_id`) REFERENCES `users` (`id`);




ALTER TABLE `google_applications`
  ADD CONSTRAINT `google_applications_ibfk_1` FOREIGN KEY (`google_internship_id`) REFERENCES `google_internships` (`id`),
  ADD CONSTRAINT `google_applications_ibfk_2` FOREIGN KEY (`google_student_detail_id`) REFERENCES `google_student_details` (`id`);




ALTER TABLE `google_colleges_locations`
  ADD CONSTRAINT `google_colleges_locations_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `google_colleges_locations_ibfk_2` FOREIGN KEY (`google_college_id`) REFERENCES `google_colleges` (`id`);




ALTER TABLE `google_college_types`
  ADD CONSTRAINT `google_college_types_ibfk_1` FOREIGN KEY (`google_college_id`) REFERENCES `google_colleges` (`id`);




ALTER TABLE `google_jobs`
  ADD CONSTRAINT `google_jobs_ibfk_1` FOREIGN KEY (`google_company_id`) REFERENCES `google_companies` (`id`);




ALTER TABLE `google_jobs_apply_on`
  ADD CONSTRAINT `google_jobs_apply_on_ibfk_1` FOREIGN KEY (`google_job_id`) REFERENCES `google_jobs` (`id`);




ALTER TABLE `google_student_details`
  ADD CONSTRAINT `google_student_details_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `google_student_details_ibfk_2` FOREIGN KEY (`home_town`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `google_student_details_ibfk_3` FOREIGN KEY (`college_location`) REFERENCES `locations` (`id`);




ALTER TABLE `help_center_faqs`
  ADD CONSTRAINT `help_center_faqs_ibfk_1` FOREIGN KEY (`help_center_category_id`) REFERENCES `help_center_categories` (`id`);




ALTER TABLE `help_center_query_history`
  ADD CONSTRAINT `help_center_query_history_ibfk_1` FOREIGN KEY (`help_center_query_box_id`) REFERENCES `help_center_query_box` (`id`);




ALTER TABLE `internshala_device_behaviour`
  ADD CONSTRAINT `internshala_device_behaviour_ibfk_1` FOREIGN KEY (`internshala_device_id`) REFERENCES `internshala_devices` (`id`);




ALTER TABLE `internshala_sessions`
  ADD CONSTRAINT `internshala_sessions_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `internshala_devices` (`id`),
  ADD CONSTRAINT `internshala_sessions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `internship_day_files`
  ADD CONSTRAINT `internship_day_files_ibfk_1` FOREIGN KEY (`internship_day_signup_id`) REFERENCES `internship_day_signups` (`id`);




ALTER TABLE `internship_day_signups`
  ADD CONSTRAINT `internship_day_signups_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `administrative_area_levels_1` (`id`),
  ADD CONSTRAINT `internship_day_signups_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`);




ALTER TABLE `internship_day_tnp_guests`
  ADD CONSTRAINT `internship_day_tnp_guests_ibfk_1` FOREIGN KEY (`internship_day_tnp_id`) REFERENCES `internship_day_tnps` (`id`);




ALTER TABLE `internship_stipend`
  ADD CONSTRAINT `internship_stipend_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `internship_stipend_ibfk_2` FOREIGN KEY (`salary_performance_scale_id`) REFERENCES `salary_performance_scales` (`id`);




ALTER TABLE `isp_applications`
  ADD CONSTRAINT `isp_applications_ibfk_2` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`),
  ADD CONSTRAINT `isp_applications_ibfk_3` FOREIGN KEY (`college_location_id`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `isp_applications_ibfk_4` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`),
  ADD CONSTRAINT `isp_applications_ibfk_5` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`id`),
  ADD CONSTRAINT `isp_applications_ibfk_6` FOREIGN KEY (`isp_version_id`) REFERENCES `isp_versions` (`id`),
  ADD CONSTRAINT `isp_applications_ibfk_7` FOREIGN KEY (`referral_isp_application_id`) REFERENCES `isp_applications` (`id`);




ALTER TABLE `isp_application_question_answers`
  ADD CONSTRAINT `isp_application_question_answers_ibfk_1` FOREIGN KEY (`isp_application_id`) REFERENCES `isp_applications` (`id`),
  ADD CONSTRAINT `isp_application_question_answers_ibfk_2` FOREIGN KEY (`isp_question_id`) REFERENCES `isp_questions` (`id`);




ALTER TABLE `isp_completion_certificates`
  ADD CONSTRAINT `isp_completion_certificates_ibfk_1` FOREIGN KEY (`isp_version_id`) REFERENCES `isp_versions` (`id`);




ALTER TABLE `isp_joining_letters`
  ADD CONSTRAINT `fk` FOREIGN KEY (`isp_application_id`) REFERENCES `isp_applications` (`id`),
  ADD CONSTRAINT `isp_joining_letters_ibfk_1` FOREIGN KEY (`isp_version_id`) REFERENCES `isp_versions` (`id`);




ALTER TABLE `isp_lists_members`
  ADD CONSTRAINT `isp_lists_members_ibfk_1` FOREIGN KEY (`isp_list_id`) REFERENCES `isp_lists` (`id`);




ALTER TABLE `isp_questions`
  ADD CONSTRAINT `isp_questions_ibfk_1` FOREIGN KEY (`isp_version_id`) REFERENCES `isp_versions` (`id`);




ALTER TABLE `isp_registration_stats`
  ADD CONSTRAINT `isp_registration_stats_ibfk_1` FOREIGN KEY (`isp_id`) REFERENCES `trainings`.`isps` (`id`),
  ADD CONSTRAINT `isp_registration_stats_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `isp_result_files`
  ADD CONSTRAINT `isp_result_files_ibfk_1` FOREIGN KEY (`isp_result_page_id`) REFERENCES `isp_result_pages` (`id`);




ALTER TABLE `isp_result_pages`
  ADD CONSTRAINT `isp_result_pages_ibfk_1` FOREIGN KEY (`isp_version_id`) REFERENCES `isp_versions` (`id`),
  ADD CONSTRAINT `isp_result_pages_ibfk_2` FOREIGN KEY (`offer_letter_mail_template_id`) REFERENCES `mail_templates` (`id`),
  ADD CONSTRAINT `isp_result_pages_ibfk_3` FOREIGN KEY (`reminder_mail_template_id`) REFERENCES `mail_templates` (`id`),
  ADD CONSTRAINT `isp_result_pages_ibfk_4` FOREIGN KEY (`offer_acceptance_mail_template_id`) REFERENCES `mail_templates` (`id`);




ALTER TABLE `jobs`
  ADD CONSTRAINT `internships_companies_fk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `internships_users_fk` FOREIGN KEY (`posted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `jobs_ibfk_3` FOREIGN KEY (`processed_by_admin_id`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `jobs_ibfk_5` FOREIGN KEY (`clone_job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_ibfk_6` FOREIGN KEY (`assigned_admin_id`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `jobs_ibfk_7` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);




ALTER TABLE `jobs_categories`
  ADD CONSTRAINT `jobs_categories_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);




ALTER TABLE `jobs_degree_groups`
  ADD CONSTRAINT `jobs_degree_groups_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_degree_groups_ibfk_2` FOREIGN KEY (`degree_group_id`) REFERENCES `degree_groups` (`id`);




ALTER TABLE `jobs_highlights`
  ADD CONSTRAINT `jobs_highlights_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_highlights_ibfk_2` FOREIGN KEY (`highlight_id`) REFERENCES `highlights` (`id`);




ALTER TABLE `jobs_locations`
  ADD CONSTRAINT `jobs_locations_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `jobs_locations_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `jobs_metas`
  ADD CONSTRAINT `jobs_metas_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `jobs_not_hired_reasons`
  ADD CONSTRAINT `jobs_not_hired_reasons_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_not_hired_reasons_ibfk_2` FOREIGN KEY (`reason_id`) REFERENCES `not_hired_reasons` (`id`);




ALTER TABLE `jobs_perks`
  ADD CONSTRAINT `jobs_perks_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `jobs_profiles`
  ADD CONSTRAINT `jobs_profiles_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`),
  ADD CONSTRAINT `jobs_profiles_ibfk_3` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `jobs_segments`
  ADD CONSTRAINT `jobs_segments_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `jobs_share_stats`
  ADD CONSTRAINT `jobs_share_stats_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `jobs_skills`
  ADD CONSTRAINT `internships_skills_internships_fk` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `internships_skills_skills_fk` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `jobs_status_histories`
  ADD CONSTRAINT `jobs_status_histories_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_status_histories_ibfk_2` FOREIGN KEY (`processed_by_admin_id`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `jobs_status_histories_ibfk_3` FOREIGN KEY (`internshala_session_id`) REFERENCES `internshala_sessions` (`id`);




ALTER TABLE `jobs_streams`
  ADD CONSTRAINT `jobs_streams_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_streams_ibfk_2` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`id`);




ALTER TABLE `jobs_titles`
  ADD CONSTRAINT `jobs_titles_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_titles_ibfk_2` FOREIGN KEY (`title_id`) REFERENCES `titles` (`id`);




ALTER TABLE `jobs_to_evaluate`
  ADD CONSTRAINT `jobs_to_evaluate_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `jobs_transactions`
  ADD CONSTRAINT `jobs_transactions_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_transactions_ibfk_2` FOREIGN KEY (`payment_error_code_id`) REFERENCES `payment_error_codes` (`id`);




ALTER TABLE `jobs_view_application_click_stats`
  ADD CONSTRAINT `jobs_view_application_click_stats_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_access_application_till_referers`
  ADD CONSTRAINT `job_access_application_till_referers_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_auto_save`
  ADD CONSTRAINT `job_auto_save_ibfk_1` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `job_campus_ambassador_info`
  ADD CONSTRAINT `job_campus_ambassador_info_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_detail_additional_infos`
  ADD CONSTRAINT `job_detail_additional_infos_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_extended`
  ADD CONSTRAINT `intenship_booleans_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_follow_up_mails`
  ADD CONSTRAINT `job_follow_up_mails_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_follow_up_types`
  ADD CONSTRAINT `job_follow_up_types_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `job_follow_up_types_ibfk_2` FOREIGN KEY (`follow_up_type_id`) REFERENCES `follow_up_types` (`id`);




ALTER TABLE `job_histories`
  ADD CONSTRAINT `job_histories_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `job_histories_ibfk_2` FOREIGN KEY (`clone_job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `job_histories_ibfk_3` FOREIGN KEY (`salary_performance_scale_id`) REFERENCES `salary_performance_scales` (`id`),
  ADD CONSTRAINT `job_histories_ibfk_4` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`);




ALTER TABLE `job_original_admin`
  ADD CONSTRAINT `job_original_admin_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `job_original_admin_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);




ALTER TABLE `job_rejection_mails`
  ADD CONSTRAINT `job_rejection_mails_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_rejection_types`
  ADD CONSTRAINT `job_rejection_types_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `job_rejection_types_ibfk_2` FOREIGN KEY (`rejection_type_id`) REFERENCES `rejection_types` (`id`);




ALTER TABLE `job_salary`
  ADD CONSTRAINT `job_salary_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `job_year_of_completions`
  ADD CONSTRAINT `internship_year_of_completions_internship` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `junk_boards`
  ADD CONSTRAINT `junk_boards_ibfk_1` FOREIGN KEY (`approved_board_id`) REFERENCES `boards` (`id`);




ALTER TABLE `junk_categories`
  ADD CONSTRAINT `junk_categories_ibfk_1` FOREIGN KEY (`approved_category_id`) REFERENCES `categories` (`id`);




ALTER TABLE `junk_colleges`
  ADD CONSTRAINT `junk_colleges_ibfk_1` FOREIGN KEY (`approved_college_id`) REFERENCES `approved_colleges` (`id`);




ALTER TABLE `junk_degrees`
  ADD CONSTRAINT `junk_degrees_ibfk_1` FOREIGN KEY (`approved_degree_id`) REFERENCES `degrees` (`id`);




ALTER TABLE `junk_profiles`
  ADD CONSTRAINT `junk_profiles_ibfk_1` FOREIGN KEY (`approved_profile_id`) REFERENCES `profiles` (`id`);




ALTER TABLE `junk_schools`
  ADD CONSTRAINT `junk_schools_ibfk_1` FOREIGN KEY (`approved_school_id`) REFERENCES `schools` (`id`);




ALTER TABLE `junk_skills`
  ADD CONSTRAINT `junk_skills_ibfk_1` FOREIGN KEY (`approved_skill_id`) REFERENCES `skills` (`id`);




ALTER TABLE `junk_streams`
  ADD CONSTRAINT `junk_streams_ibfk_1` FOREIGN KEY (`approved_stream_id`) REFERENCES `streams` (`id`);




ALTER TABLE `junk_trainings`
  ADD CONSTRAINT `junk_trainings_ibfk_1` FOREIGN KEY (`approved_training_id`) REFERENCES `trainings` (`id`);




ALTER TABLE `karix_sms_stats`
  ADD CONSTRAINT `karix_sms_stats_ibfk_1` FOREIGN KEY (`sms_response_code_id`) REFERENCES `sms_response_codes` (`id`);




ALTER TABLE `locations`
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`sublocality_level_2_id`) REFERENCES `sublocality_levels_2` (`id`),
  ADD CONSTRAINT `locations_ibfk_2` FOREIGN KEY (`sublocality_level_1_id`) REFERENCES `sublocality_levels_1` (`id`),
  ADD CONSTRAINT `locations_ibfk_3` FOREIGN KEY (`locality_id`) REFERENCES `localities` (`id`),
  ADD CONSTRAINT `locations_ibfk_4` FOREIGN KEY (`administrative_area_level_2_id`) REFERENCES `administrative_area_levels_2` (`id`),
  ADD CONSTRAINT `locations_ibfk_5` FOREIGN KEY (`administrative_area_level_1_id`) REFERENCES `administrative_area_levels_1` (`id`),
  ADD CONSTRAINT `locations_ibfk_6` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);




ALTER TABLE `mail_senders_receivers`
  ADD CONSTRAINT `mail_senders_receivers_ibfk_1` FOREIGN KEY (`mail_email_id`) REFERENCES `mail_emails` (`id`),
  ADD CONSTRAINT `mail_senders_receivers_ibfk_2` FOREIGN KEY (`mail_type_id`) REFERENCES `mail_types` (`id`);




ALTER TABLE `mail_templates`
  ADD CONSTRAINT `mail_templates_ibfk_1` FOREIGN KEY (`mail_type_id`) REFERENCES `mail_types` (`id`),
  ADD CONSTRAINT `mail_templates_ibfk_2` FOREIGN KEY (`mail_signature_id`) REFERENCES `mail_signatures` (`id`);




ALTER TABLE `noc`
  ADD CONSTRAINT `noc_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `noc_ibfk_2` FOREIGN KEY (`tnp_id`) REFERENCES `tnp` (`id`);




ALTER TABLE `professionalism_contests`
  ADD CONSTRAINT `professionalism_contests_ibfk_1` FOREIGN KEY (`application_reporting_reason_id`) REFERENCES `application_reporting_reasons` (`id`);




ALTER TABLE `profiles_categories`
  ADD CONSTRAINT `profiles_categories_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`),
  ADD CONSTRAINT `profiles_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);




ALTER TABLE `profile_groups_profiles`
  ADD CONSTRAINT `profile_groups_profiles_ibfk_1` FOREIGN KEY (`profile_group_id`) REFERENCES `profile_groups` (`id`),
  ADD CONSTRAINT `profile_groups_profiles_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`);




ALTER TABLE `rating_questions`
  ADD CONSTRAINT `rating_questions_ibfk_1` FOREIGN KEY (`rating_level_id`) REFERENCES `rating_levels` (`id`),
  ADD CONSTRAINT `rating_questions_ibfk_2` FOREIGN KEY (`rating_question_set_id`) REFERENCES `rating_question_sets` (`id`);




ALTER TABLE `rating_questions_helpers`
  ADD CONSTRAINT `rating_questions_helpers_ibfk_1` FOREIGN KEY (`rating_question_id`) REFERENCES `rating_questions` (`id`);




ALTER TABLE `rating_question_options`
  ADD CONSTRAINT `rating_question_options_ibfk_1` FOREIGN KEY (`rating_question_id`) REFERENCES `rating_questions` (`id`);




ALTER TABLE `rating_quiz_student_scores`
  ADD CONSTRAINT `rating_quiz_student_scores_ibfk_1` FOREIGN KEY (`rating_level_id`) REFERENCES `rating_levels` (`id`),
  ADD CONSTRAINT `rating_quiz_student_scores_ibfk_2` FOREIGN KEY (`rating_level_id`) REFERENCES `rating_levels` (`id`),
  ADD CONSTRAINT `rating_quiz_student_scores_ibfk_3` FOREIGN KEY (`rating_question_set_id`) REFERENCES `rating_question_sets` (`id`),
  ADD CONSTRAINT `rating_quiz_student_scores_ibfk_4` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `rating_quiz_student_scores_ibfk_5` FOREIGN KEY (`internshala_session_id`) REFERENCES `internshala_sessions` (`id`);




ALTER TABLE `rating_quiz_student_sources`
  ADD CONSTRAINT `rating_quiz_student_sources_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `rating_quiz_student_sources_ibfk_2` FOREIGN KEY (`rating_level_id`) REFERENCES `rating_levels` (`id`);




ALTER TABLE `rating_student_answers`
  ADD CONSTRAINT `rating_student_answers_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `rating_student_answers_ibfk_2` FOREIGN KEY (`rating_quiz_student_score_id`) REFERENCES `rating_quiz_student_scores` (`id`),
  ADD CONSTRAINT `rating_student_answers_ibfk_3` FOREIGN KEY (`rating_question_option_id`) REFERENCES `rating_question_options` (`id`);




ALTER TABLE `recommendations`
  ADD CONSTRAINT `recommendations_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `recommendations_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `recommendations_advanced_search`
  ADD CONSTRAINT `recommendations_advanced_search_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `recommendations_decline_reasons`
  ADD CONSTRAINT `recommendations_decline_reasons_ibfk_1` FOREIGN KEY (`recommendation_id`) REFERENCES `recommendations` (`id`),
  ADD CONSTRAINT `recommendations_decline_reasons_ibfk_2` FOREIGN KEY (`decline_reason_id`) REFERENCES `decline_reasons` (`id`);




ALTER TABLE `recommendations_intermediate`
  ADD CONSTRAINT `recommendations_intermediate_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `recommendations_intermediate_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `recommendations_job`
  ADD CONSTRAINT `recommendations_job_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `recommendations_view_source`
  ADD CONSTRAINT `recommendations_view_source_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `recommendation_dumps`
  ADD CONSTRAINT `recommendation_dumps_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `recommendation_dumps_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `remember_me_tokens`
  ADD CONSTRAINT `remember_me_tokens_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `rsa_keys`
  ADD CONSTRAINT `rsa_keys_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `internshala_devices` (`id`);




ALTER TABLE `shortened_url_csv_shortened_urls`
  ADD CONSTRAINT `shortened_url_csv_shortened_urls_ibfk_1` FOREIGN KEY (`shortened_url_csv_id`) REFERENCES `shortened_url_csv` (`id`),
  ADD CONSTRAINT `shortened_url_csv_shortened_urls_ibfk_2` FOREIGN KEY (`shortened_url_id`) REFERENCES `shortened_urls` (`id`);




ALTER TABLE `skills_trainings_mapping`
  ADD CONSTRAINT `skills_trainings_mapping_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`);




ALTER TABLE `slider_banners`
  ADD CONSTRAINT `slider_banners_ibfk_1` FOREIGN KEY (`banner_template_id`) REFERENCES `banner_templates` (`id`);




ALTER TABLE `slots`
  ADD CONSTRAINT `slots_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;




ALTER TABLE `sms_campaigns`
  ADD CONSTRAINT `sms_campaigns_ibfk_1` FOREIGN KEY (`sms_list_id`) REFERENCES `sms_lists` (`id`);




ALTER TABLE `sms_events`
  ADD CONSTRAINT `sms_events_ibfk_1` FOREIGN KEY (`sms_lists_contact_id`) REFERENCES `sms_lists_contacts` (`id`),
  ADD CONSTRAINT `sms_events_ibfk_2` FOREIGN KEY (`sms_campaign_id`) REFERENCES `sms_campaigns` (`id`);




ALTER TABLE `sms_lists_contacts`
  ADD CONSTRAINT `sms_lists_contacts_ibfk_1` FOREIGN KEY (`sms_list_id`) REFERENCES `sms_lists` (`id`);




ALTER TABLE `sms_lists_contacts_csv`
  ADD CONSTRAINT `sms_lists_contacts_csv_ibfk_1` FOREIGN KEY (`sms_list_id`) REFERENCES `sms_lists` (`id`);




ALTER TABLE `sms_referral_logs`
  ADD CONSTRAINT `sms_referral_logs_ibfk_1` FOREIGN KEY (`referral_user_id`) REFERENCES `users` (`id`);




ALTER TABLE `stipend_prediction_internships`
  ADD CONSTRAINT `stipend_prediction_internships_ibfk_1` FOREIGN KEY (`internship_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `stipend_prediction_recommendations`
  ADD CONSTRAINT `stipend_prediction_recommendations_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);




ALTER TABLE `stream_degree_discounts`
  ADD CONSTRAINT `stream_degree_discounts_ibfk_1` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`),
  ADD CONSTRAINT `stream_degree_discounts_ibfk_2` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`id`);




ALTER TABLE `students`
  ADD CONSTRAINT `second_city_of_residence_fk` FOREIGN KEY (`second_city_of_residence`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `students_current_city_fk` FOREIGN KEY (`current_city`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `students_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `students_activation_tracker`
  ADD CONSTRAINT `students_activation_tracker_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `students_colleges`
  ADD CONSTRAINT `students_colleges_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `students_colleges_ibfk_2` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`),
  ADD CONSTRAINT `students_colleges_ibfk_3` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`),
  ADD CONSTRAINT `students_colleges_ibfk_4` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`id`);




ALTER TABLE `students_notifications`
  ADD CONSTRAINT `students_notifications_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `students_notifications_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`);




ALTER TABLE `students_schools`
  ADD CONSTRAINT `students_schools_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `students_schools_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `schools` (`id`);




ALTER TABLE `students_skills`
  ADD CONSTRAINT `students_skills_skills_fk` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `students_skills_students_fk` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `student_additional_details`
  ADD CONSTRAINT `student_additional_details_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_badges`
  ADD CONSTRAINT `student_badges_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `student_badges_ibfk_2` FOREIGN KEY (`internshala_training_id`) REFERENCES `internshala_trainings` (`id`);




ALTER TABLE `student_campaigns`
  ADD CONSTRAINT `student_campaigns_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);




ALTER TABLE `student_digest_notifications`
  ADD CONSTRAINT `student_digest_notifications_ibfk_1` FOREIGN KEY (`gcm_user_id`) REFERENCES `gcm_users` (`id`),
  ADD CONSTRAINT `student_digest_notifications_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_experiences`
  ADD CONSTRAINT `experience_organization_id_fk` FOREIGN KEY (`experience_organization_id`) REFERENCES `experience_organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `location_id_fk` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `profile_id_fk` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_fk` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `student_extended`
  ADD CONSTRAINT `student_extended_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_invitation_counts`
  ADD CONSTRAINT `student_invitation_counts_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_metas`
  ADD CONSTRAINT `student_metas_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_position_of_responsibilities`
  ADD CONSTRAINT `student_position_of_responsibilities_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_projects`
  ADD CONSTRAINT `student_projects_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `student_trainings`
  ADD CONSTRAINT `experience_organization_id_fk1` FOREIGN KEY (`experience_organization_id`) REFERENCES `experience_organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `location_id_fk1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_fk1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `training_id_fk1` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `student_work_samples`
  ADD CONSTRAINT `student_work_samples_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);




ALTER TABLE `subscribers`
  ADD CONSTRAINT `subscribers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `subscribers_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `subscribers_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `subscribers_digest_count`
  ADD CONSTRAINT `subscribers_digest_count_ibfk_1` FOREIGN KEY (`subscriber_id`) REFERENCES `subscribers` (`id`);




ALTER TABLE `subscribers_status_trigger`
  ADD CONSTRAINT `subscribers_status_trigger_ibfk_1` FOREIGN KEY (`subscriber_id`) REFERENCES `subscribers` (`id`);




ALTER TABLE `temp_blocked_chat_messages`
  ADD CONSTRAINT `temp_blocked_chat_messages_ibfk_2` FOREIGN KEY (`chat_message_id`) REFERENCES `chat_messages` (`id`),
  ADD CONSTRAINT `temp_blocked_chat_messages_ibfk_3` FOREIGN KEY (`blocked_employer_detail_id`) REFERENCES `temp_blocked_employer_details_new` (`id`);




ALTER TABLE `temp_blocked_employer_details`
  ADD CONSTRAINT `temp_blocked_employer_details_ibfk_1` FOREIGN KEY (`blocked_employer_email_id`) REFERENCES `temp_blocked_employer_email` (`id`);




ALTER TABLE `temp_blocked_employer_details_new`
  ADD CONSTRAINT `temp_blocked_employer_details_new_ibfk_1` FOREIGN KEY (`blocked_employer_email_id`) REFERENCES `temp_blocked_employer_email` (`id`);




ALTER TABLE `temp_blocked_employer_email`
  ADD CONSTRAINT `employer_users_fk` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`);




ALTER TABLE `temp_college_discount_coupon_codes`
  ADD CONSTRAINT `temp_college_discount_coupon_codes_ibfk_1` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`),
  ADD CONSTRAINT `temp_college_discount_coupon_codes_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `trainings`.`discounts` (`id`);




ALTER TABLE `titles_profiles`
  ADD CONSTRAINT `titles_profiles_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`),
  ADD CONSTRAINT `titles_profiles_ibfk_2` FOREIGN KEY (`title_id`) REFERENCES `titles` (`id`);




ALTER TABLE `tnp`
  ADD CONSTRAINT `tnp_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `tnp_ibfk_2` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`);




ALTER TABLE `tnps_hired_students_applications_history`
  ADD CONSTRAINT `tnps_hired_students_applications_history_ibfk_1` FOREIGN KEY (`tnp_id`) REFERENCES `tnp` (`id`),
  ADD CONSTRAINT `tnps_hired_students_applications_history_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`);




ALTER TABLE `training_contests`
  ADD CONSTRAINT `training_contest_ibfk_1` FOREIGN KEY (`training_contest_detail_id`) REFERENCES `training_contest_details` (`id`);




ALTER TABLE `users_app_notifications`
  ADD CONSTRAINT `users_app_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_autocomplete_request_stats`
  ADD CONSTRAINT `user_autocomplete_request_stats_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_blocked_stats`
  ADD CONSTRAINT `user_blocked_stats_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_block_reasons`
  ADD CONSTRAINT `user_block_reasons_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_otps`
  ADD CONSTRAINT `user_otps_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_preferences`
  ADD CONSTRAINT `user_preferences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_preferences_availabilities`
  ADD CONSTRAINT `user_preferences_availabilities_ibfk_1` FOREIGN KEY (`user_preference_id`) REFERENCES `user_preferences` (`id`);




ALTER TABLE `user_preferences_categories`
  ADD CONSTRAINT `user_preferences_categories_ibfk_1` FOREIGN KEY (`user_preference_id`) REFERENCES `user_preferences` (`id`),
  ADD CONSTRAINT `user_preferences_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);




ALTER TABLE `user_preferences_locations`
  ADD CONSTRAINT `user_preferences_locations_ibfk_1` FOREIGN KEY (`user_preference_id`) REFERENCES `user_preferences` (`id`),
  ADD CONSTRAINT `user_preferences_locations_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;




ALTER TABLE `user_preference_seen_stats`
  ADD CONSTRAINT `user_preference_seen_stats_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_registration_devices`
  ADD CONSTRAINT `user_registration_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_request_stats`
  ADD CONSTRAINT `user_request_stats_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);




ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);