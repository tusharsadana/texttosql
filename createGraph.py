import schema
import helper

def createNode(sqlTable):
    
    
    #find table name
    try:
        firstTilda = sqlTable.find('`')
        counter = firstTilda + 1
        while(sqlTable[counter] != '`'):
            counter+=1
        
        tableName = sqlTable[firstTilda+1:counter]
        #find all the column names
        
        sqlTableList = sqlTable.split('\n')
        columnList = {'all' : 'all' }
        for i in range(1, len(sqlTableList)):
            sqlTableList[i] = sqlTableList[i].strip()

            columnCounter = 1
            try:
                while(sqlTableList[i][columnCounter] != '`'):
                    columnCounter += 1
                columnName = sqlTableList[i][1:columnCounter]
                columnType = sqlTableList[i].split(' ')[1]
                columnList[columnName] = columnType
            except:
                pass

        
        table = schema.Table(tableName)
        table.setColumns(columnList)

        return table
    except:
        return None
    
def addKeys(keyString = """ALTER TABLE `user_preference_seen_stats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session_id` (`session_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `session_id_2` (`session_id`);"""):
    try:
        firstTilda = keyString.find('`')
        counter = firstTilda + 1
        while(keyString[counter] != '`'):
            counter+=1
        
        primaryKeys = []
        uniqueKeys = []
        tableName = keyString[firstTilda+1:counter]
        keyString = keyString.split('\n')
        for i in range(len(keyString)):
            if(keyString[i].find("PRIMARY KEY") != -1):
                temp = keyString[i].find('(')
                temp2 = keyString[i].find(')')
                tempStore = keyString[i][temp+1:temp2].replace('`','').split(',')
                for j in tempStore:
                    primaryKeys.append(j)
                
            elif(keyString[i].find("UNIQUE KEY") != -1):
                temp = keyString[i].find('(')
                temp2 = keyString[i].find(')')
                tempStore = keyString[i][temp+1:temp2].replace('`','').split(',')
                for j in tempStore:
                    uniqueKeys.append(j)
        
        return tableName, primaryKeys, uniqueKeys
    except:
        return None, None, None
        pass
    pass

def createRelationship(relationString = """ALTER TABLE `students_colleges`
  ADD CONSTRAINT `students_colleges_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `students_colleges_ibfk_2` FOREIGN KEY (`college_id`) REFERENCES `colleges` (`id`),
  ADD CONSTRAINT `students_colleges_ibfk_3` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`),
  ADD CONSTRAINT `students_colleges_ibfk_4` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`id`);"""):
        
    try:
        firstTilda = relationString.find('`')
        counter = firstTilda + 1
        while(relationString[counter] != '`'):
            counter+=1
        foreignKeys = {}
        tableName = relationString[firstTilda+1:counter]
        relationString = relationString.split('\n')
        for i in range(1, len(relationString)):
            relationString[i] = relationString[i].split('FOREIGN KEY')[1].split('REFERENCES')
            temp = relationString[i][0].find('(')
            temp2 = relationString[i][0].find(')')
            tempStore = relationString[i][0][temp+1:temp2].replace('`','')
            
            foreignTable = relationString[i][1].strip()
            
            firstTilda = foreignTable.find('`')
            counter = firstTilda + 1
            while(foreignTable[counter] != '`'):
                counter+=1
            foreignTableName = foreignTable[firstTilda+1:counter]
            temp = relationString[i][1].find('(')
            temp2 = relationString[i][1].find(')')
            foreignColumn = relationString[i][1][temp+1:temp2].replace('`','')
            
            foreignKeys[tempStore] = (foreignTableName, foreignColumn)
        
        return tableName, foreignKeys
        
    except:
        return None, None
        pass
    
    pass


def createAllNodes(tableFilename='./Tables/tables.sql', keyFilename = './Keys/keys.sql', relationFilename= './Relations/relations.sql'):
    
    tableFilename='./Tables/tables.sql'
    with open(tableFilename, 'r') as f:
        sqlTable = (f.read())
        
    sqlTable = sqlTable.split(';')
    allNodes = {}

    for i in range(len(sqlTable)):
        sqlTable[i] = sqlTable[i].strip()
        objectNode = createNode(str(sqlTable[i]))
        if(objectNode != None):
            allNodes[objectNode.tableName] = objectNode
    
    with open(keyFilename, 'r') as f:
        keyString = (f.read())
    
    keyString = keyString.split(';')
    
    for i in range(len(keyString)):
        keyString[i] = keyString[i].strip()
        tempTable, primaryKeys, uniqueKeys = addKeys(keyString[i])
        
        if(tempTable in allNodes):
            instance = allNodes[tempTable]
            instance.setPrimaryKeys(primaryKeys)
            instance.setUniqueKeys(uniqueKeys)
    
    with open(relationFilename, 'r') as f:
        relationString = (f.read())
    
    relationString = relationString.split(';')
    
    for i in range(len(relationString)):
        relationString[i] = relationString[i].strip()
        tempTable, foreignKeys = createRelationship(relationString[i])
        
        if(tempTable in allNodes):
            instance = allNodes[tempTable]
            instance.setForeignKeys(foreignKeys)
    
    return allNodes


def createJoinHash(allNodes):
    
    joinHash = {}
    
    for i in allNodes.keys():
        queue = schema.Queue()
        queue.addToQueue([i, None, None])
        stringHash = ''
        hashValue = []
        while(not queue.isEmpty()):
            stringHash = ''
            hashValue = []
            node = queue.popFromQueue()
            tableName = node[0]
            neighbours = allNodes[tableName].getForeignKeys()
            for neighbour in neighbours.keys():
                if(node[1] == None):    
                        stringHash = i + ':' + neighbours[neighbour][0]
                        hashValue = [(neighbour, neighbours[neighbour][1])]
                        
                        if(stringHash not in joinHash.keys()):
                            joinHash[stringHash] = hashValue
                            queue.addToQueue([neighbours[neighbour][0], stringHash, hashValue])
                            
                            
                    
                elif(node[1] is not None):
                    if(neighbours[neighbour][0] not in node[1].split(':')):
                        
                        stringHash = node[1] + ':' + neighbours[neighbour][0]
                        
                        
                        if(stringHash not in joinHash.keys()):
                            hashValue = node[2]
                            temp = [k for k in node[2]]
                            temp.append((neighbour, neighbours[neighbour][1]))
                            joinHash[stringHash] = temp
                            queue.addToQueue([neighbours[neighbour][0], stringHash, temp])
                                    
        
            
    return joinHash
        
        

























