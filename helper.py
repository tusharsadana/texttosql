# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 13:21:26 2020

@author: tusha
"""
def conditionsResolver(condition):
    typeOfColumn = condition[1][1]
    columnName = condition[1][0]
    tableName = condition[0]
    
    if(typeOfColumn.find('varchar') != -1):
        conditionToSolve = condition[2]
        operation = conditionToSolve[0]
        string = conditionToSolve[1]
        if(operation == 'equalTo'):
            operation = '='
        elif(operation == "notEqualTo"):
            operation = "!="
        elif(operation == "like"):
            operation = "LIKE"
        return tableName + '.' + columnName + " " + operation + " \""+ string +"\""
    
    elif(typeOfColumn.find('timestamp') != -1 or
         typeOfColumn.find('date') != -1):
        
        conditionToSolve = condition[2]
        operation = conditionToSolve[0]
        value = conditionToSolve[1]
        
        if(operation == "lessThan"):
            operation = "<"
        elif(operation == "greaterThan"):
            operation = ">"
        elif(operation == "lessThanEqualTo"):
            operation = "<="
        elif(operation == "greaterThanEqualTo"):
            operation = ">="
        elif(operation == "notEqualTo"):
            operation = "!="
        elif(operation == "equalTo"):
            operation = "="
        elif(operation == "like"):
            operation = "LIKE"
            return tableName + "." + columnName + " " + operation + " \"" + value +"\"" 
        elif(operation == "range"):
            if(len(conditionToSolve) == 3):
                value2 = conditionToSolve[2]
                query = tableName + "." + columnName + " " + ">=" + " \"" + value + "\" AND " + tableName + "." + columnName + " " + "<=" + " \"" + value2 + "\""
                return query
            else:
                return "error in range"
        
        return tableName + "." + columnName + " " + operation + " \"" + value +"\""
    
    elif(typeOfColumn.find('decimal') != -1 or
         typeOfColumn.find('int') != -1  or
         typeOfColumn.find('mediumint') != -1 or
         typeOfColumn.find('smallint') != -1 or
         typeOfColumn.find('tinyint') != -1):
        
        conditionToSolve = condition[2]
        operation = conditionToSolve[0]
        value = conditionToSolve[1]
        
        if(operation == "lessThan"):
            operation = "<"
        elif(operation == "greaterThan"):
            operation = ">"
        elif(operation == "lessThanEqualTo"):
            operation = "<="
        elif(operation == "greaterThanEqualTo"):
            operation = ">="
        elif(operation == "notEqualTo"):
            operation = "!="
        elif(operation == "equalTo"):
            operation = "=" 
        elif(operation == "range"):
            if(len(conditionToSolve) == 3):
                value2 = conditionToSolve[2]
                query = tableName + "." + columnName + " " + ">=" + " " + value + " AND " + tableName + "." + columnName + " " + "<=" + " " + value2
                return query
            else:
                return "error in range"
        
        return tableName + "." + columnName + " " + operation + " " + value
        
        
    
    elif(typeOfColumn.find('enum') != -1):
        
        conditionToSolve = condition[2]
        operation = conditionToSolve[0]
        value = conditionToSolve[1]
        
        if(operation == "equalTo"):
            operation = "="
        elif(operation == "notEqualTo"):
            operation = "!="
        
        return tableName + "." + columnName + " " + operation + " \"" + value + "\""
        
def resolvePossibleJoins(joinHash):
    length = len(joinHash.keys())
    keys = list(joinHash.keys())
    additionalHash = {}
    for i in range(length):
        keyToCheck = keys[i]
        keyToCheck = keyToCheck.split(':')
        for j in range(i+1, length):
            if(keys[j].split(':')[0] == keyToCheck[0]):
                flag = 0
                word = keys[j].split(':')
                for k in range(1, len(keyToCheck)):
                    if(keyToCheck[k] in word):
                        flag = 1
                        break
                if(flag == 0):
                    #write code
                    temp = [keyToCheck[r] for r in range(len(keyToCheck)-1, -1, -1)]
                    tempValue = joinHash[':'.join(keyToCheck)]
                    correctValue = [(tempValue[w][1], tempValue[w][0]) for w in range(len(tempValue)-1,-1,-1)]
                    keysJValue = joinHash[keys[j]]
                    for q in keysJValue:
                        correctValue.append(q)
                    temp.pop()
                    stringTemp = ':'.join(temp)
                    finalString = stringTemp + ':' + keys[j]
                    if(finalString not in additionalHash.keys()):
                        additionalHash[finalString] = correctValue
                        
                    else:
                        print('repeat')

    return additionalHash

def mergeTwoDictionaries(dict1, dict2):
    return(dict2.update(dict1))

def startToEndDefiner(joinHash):
    startToEndDictionary = {}
    keys = list(joinHash.keys())
    for i in keys:
        temp = i.split(':')
        newString = temp[0] + ':' + temp[-1]
        if(newString not in startToEndDictionary.keys()):
            startToEndDictionary[newString] = [i]
        else:
            startToEndDictionary[newString].append(i)
        
    return startToEndDictionary
    

                      

def joinConditionResolver(accessString, joinHash, joins = None):
    
    columns = joinHash[accessString]
    accessString = accessString.split(':')
    if(joins == None):
        joins = 'JOIN'
    if(accessString[-1] != 'jobs'):
        pass
    else:
        accessString = [accessString[r] for r in range(len(accessString)-1, -1, -1)]
        columns = [(columns[w][1], columns[w][0]) for w in range(len(columns)-1,-1,-1)]
    main = ''
    for i in range(1, len(accessString)):
        temp = ''
        temp = ' ' + joins + ' ' + accessString[i] 
        temp += ' ON ' + accessString[i-1] + '.' + columns[i-1][0] + ' = ' +accessString[i] + '.' + columns[i-1][1]
        main += temp
    
    return accessString[0], main        

        
                       
                    
            
            
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
