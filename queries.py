# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 13:10:50 2020

@author: tusha
"""

import createGraph
import helper

def generateSingleQuery(selection, conditions = None):
    query = "SELECT "
    columnNames = []
    for i in selection:
        columnNames.append(i[0]+"."+i[1])
    
    names = ', '.join(columnNames)
    query += names
    
    query+= " FROM "
    query+= selection[0][0]
    
    if(conditions != None):
        if(len(conditions) == 3):
            where = helper.conditionsResolver(conditions[0])
            where += " " + conditions[1] + " "
            where += helper.conditionsResolver(conditions[2])
            
        elif(len(conditions) == 1):
            where = helper.conditionsResolver(conditions[0])
        query += " " + "WHERE " + where
    
    return query
        
#details of students who applied in job profile = data science
    #skills of all the jobs of profile data science
#ANSWER - SELECT * FROM students 
def generateMultipleQuery(selection, joinHash, startToEnd, joins = None, conditions = None):
    findTables = []
    for i in selection:
        if i[0] not in findTables:
            findTables.append(i[0])
        if i[3] != None and i[3] not in findTables:
            findTables.append(i[3])
    conditionTables = []
    
    if(conditions != None):
        for i in conditions:
            temp = []
            if(type(i) != str):
                if i[0] not in findTables:
                    temp = [i[0]]
                if i[3] != None and i[3] not in findTables:
                    temp.append(i[3])
                if(len(temp) > 0):
                    conditionTables.append(temp)
    connectingConditions = []
    for i in conditionTables:
        accessString = ':'.join(i)
        accessString = startToEnd[accessString]
        connectingTable, conditionJoin = helper.joinConditionResolver(accessString[0], joinHash)
        connectingConditions.append((connectingTable, conditionJoin))
    print(connectingConditions)
    
            
    
    pass


    



#Sample Driver Code
    
#studentTable = createGraph.createNode()    
#sampleSelection = [('students', 'id', 'int(11)'), ('students', 'user_id', 'int(11)'), ('students', 'first_name', 'varchar(256)')]
#sampleCondition = [["students", ("created_at", "timestamp"), ("like", "abc")], "OR", ["students", ("last_updated", "timestamp"), ("greaterThan", "date")]]
#generatedQuery = generateSingleQuery(sampleSelection, sampleCondition)
#print(generatedQuery)

allNodes = createGraph.createAllNodes()
joinHash = createGraph.createJoinHash(allNodes)
additionHash = helper.resolvePossibleJoins(joinHash)  
helper.mergeTwoDictionaries(additionHash, joinHash)
startToEndDict = helper.startToEndDefiner(joinHash)

sampleSelectionMulti = [('students', 'id', 'int(11)', None), ('students', 'user_id', 'int(11)', None), ('students', 'first_name', 'varchar(256)', None), ('users', 'email', 'varchar(1000)', 'students')]
sampleConditionMulti = [["profiles", ("name", "varchar"), ("like", "data"),  'jobs'], "AND", ["students", ("last_updated", "timestamp"), ("greaterThan", "date"), None]]

generateMultipleQuery(sampleSelectionMulti, joinHash, startToEndDict, conditions = sampleConditionMulti)
