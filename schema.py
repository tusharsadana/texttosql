
class Table:
    def __init__(self, tableName):
        self.tableName = tableName
        self.foreignKeys = {}
        self.uniqueKeys = []
        self.primaryKeys= []
        self.columns = {} #name to show to user mapped to column name (attribute)
        self.root = '' #object
        self.children = [] #objects (for future if needed)
        self.joinDict = {}
    
    def setForeignKeys(self, arrayOfFKeys):
        self.foreignKeys = arrayOfFKeys
    
    def setColumns(self, listOfColumns):
        self.columns = listOfColumns
    
    def setRoot(self, node):
        self.root = node
    
    def setPrimaryKeys(self, primaryKeysList):
        self.primaryKeys = primaryKeysList
    
    def setUniqueKeys(self, uniqueKeysList):
        self.uniqueKeys = uniqueKeysList
    
    def getForeignKeys(self):
        return self.foreignKeys
    
    def getColumns(self):
        return self.columns
    
    def getRoot(self):
        return self.root
    
    def getPrimaryKeys(self):
        return self.primaryKeys
    


        
class Queue:
    queue = []
    def __init__(self):
        pass
    
    def addToQueue(self, item):
        Queue.queue.append(item)
    
    def popFromQueue(self):
        return Queue.queue.pop(0)
    
    def isEmpty(self):
        return len(Queue.queue) == 0
    
    def size(self):
        return len(Queue.queue)

    
    