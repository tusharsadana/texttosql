CREATE TABLE `account_delete_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_key` varchar(256) NOT NULL,
  `user_type` enum('student','employer','admin','user','tnp','training_partner') NOT NULL,
  `old_email` varchar(256) NOT NULL,
  `feedback` varchar(1024) DEFAULT NULL,
  `status` enum('unconfirmed','confirmed','expired') NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



 CREATE TABLE `additional_infos` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `type` enum('assessment','availability','job_availability') DEFAULT NULL,
  `question_type` enum('text','file') DEFAULT NULL,
  `question` varchar(5000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;  

CREATE TABLE `administrative_area_levels_1` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_indian_state_ut` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `administrative_area_levels_2` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `phone_primary` varchar(12) DEFAULT NULL,
  `operation_team_member_status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `team` enum('internship') DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admins_admin_privileges` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `admin_privilege_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admins_daily_plan` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `green_internships` int(11) NOT NULL DEFAULT '0',
  `blue_internships` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admins_internships_tracker` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `stray_internships` int(11) NOT NULL DEFAULT '0',
  `tracked_on` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admin_em_lists_queue` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `em_username` enum('admin','training','employer','student','common','newsletter') NOT NULL,
  `listname` varchar(256) DEFAULT NULL,
  `ownername` varchar(256) DEFAULT NULL,
  `owner_email` varchar(256) DEFAULT NULL,
  `reply_to_email` varchar(256) DEFAULT NULL,
  `file` varchar(512) DEFAULT NULL,
  `total_rows` int(11) DEFAULT NULL,
  `rows_per_file` int(11) DEFAULT NULL,
  `total_lists` int(11) DEFAULT NULL,
  `list_ids` varchar(256) DEFAULT NULL,
  `lists_processed` int(11) NOT NULL DEFAULT '0',
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `status` enum('to_be_processed','processing','processed','error') NOT NULL,
  `info` varchar(4096) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admin_privileges` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `name_to_display` varchar(256) DEFAULT NULL,
  `details` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admin_privileges_admin_urls` (
  `id` int(11) NOT NULL,
  `admin_privilege_id` int(11) DEFAULT NULL,
  `admin_url_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admin_uploads` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `type` enum('image','video','file') NOT NULL,
  `name` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `admin_urls` (
  `id` int(11) NOT NULL,
  `url` varchar(256) DEFAULT NULL,
  `is_logging_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `advanced_search` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `search_by` varchar(20000) NOT NULL,
  `operator` enum('>=','<=','>','<','=','<>','in','like') NOT NULL,
  `value` varchar(5000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `aicte_signups` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `phone` varchar(12) NOT NULL,
  `college_name` varchar(512) NOT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `location` varchar(512) DEFAULT NULL,
  `state` varchar(512) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT '2019-01-01 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `analysis_student_complaints_data` (
  `id` int(11) NOT NULL,
  `email` varchar(256) DEFAULT NULL,
  `company` varchar(1024) DEFAULT NULL,
  `issue` varchar(256) DEFAULT NULL,
  `outcome` varchar(256) DEFAULT NULL,
  `is_outcome_positive` tinyint(4) DEFAULT NULL,
  `is_processed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `analysis_student_complaints_output` (
  `id` int(11) NOT NULL,
  `complaint_id` int(11) DEFAULT NULL,
  `registered_company_name` varchar(1024) DEFAULT NULL,
  `internship_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `status` enum('open','rejected','shortlisted','hired','inactive','invited') NOT NULL,
  `invitation_status` enum('open','accepted','rejected','expired') DEFAULT NULL,
  `is_invitation_seen` tinyint(1) NOT NULL DEFAULT '0',
  `cover_letter` varchar(5000) DEFAULT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `is_hiring_exception` tinyint(4) DEFAULT '0',
  `is_message_sent` int(10) NOT NULL DEFAULT '0',
  `last_employer_message_sent_at` timestamp NULL DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `hired_at` timestamp NULL DEFAULT NULL,
  `rejected_at` timestamp NULL DEFAULT NULL,
  `shortlisted_at` timestamp NULL DEFAULT NULL,
  `shortlisted_referral` enum('list_view','bulk_operator','details_view','admin_tool','chatbot','chat','invitation') DEFAULT NULL,
  `hired_referral` enum('bulk_operator','details_view','admin_tool','chatbot','chat','closure','list_view') DEFAULT NULL,
  `rejected_referral` enum('list_view','bulk_operator','details_view','admin_tool','chatbot','chat') DEFAULT NULL,
  `employer_dashboard_status` enum('opened','unopened') NOT NULL DEFAULT 'unopened',
  `opened_at` timestamp NULL DEFAULT NULL,
  `is_filtered_out` tinyint(1) NOT NULL DEFAULT '0',
  `is_sent_in_employer_digest` tinyint(1) NOT NULL DEFAULT '0',
  `is_internship_story_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `referral` enum('in_product_notification','web_push_notification','app_push_notification','mobile_web_push_notification','in_panel_notification','news_letter','matching_preferences','student_digest','app','student_digest_active','student_digest_inactive','search_matching_preferences','search_vanilla','search_filters','search_keyword','unknown','bookmark_web','bookmark_app','app_search_matching_preferences','app_search_keyword','app_search_vanilla','app_search_filters','app_unknown','similar_internships','app_similar_internships') DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `previous_session_last_seen_at` timestamp NULL DEFAULT NULL,
  `applied_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications_additional_infos` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `additional_info_id` int(11) NOT NULL,
  `value` varchar(5000) DEFAULT NULL,
  `language_tool_response` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications_evaluation` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `length` int(11) NOT NULL DEFAULT '0',
  `typos` int(11) NOT NULL DEFAULT '0',
  `casings` int(11) NOT NULL DEFAULT '0',
  `grammar` int(11) NOT NULL DEFAULT '0',
  `typographicals` int(11) DEFAULT '0',
  `punctuations` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications_new` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `internship_id` int(11) NOT NULL,
  `status` enum('open','rejected','shortlisted','hired','inactive','invited') NOT NULL,
  `invitation_status` enum('open','accepted','rejected','expired') DEFAULT NULL,
  `is_invitation_seen` tinyint(1) NOT NULL DEFAULT '0',
  `cover_letter` varchar(5000) DEFAULT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `is_hiring_exception` tinyint(4) DEFAULT '0',
  `is_message_sent` int(10) NOT NULL DEFAULT '0',
  `last_employer_message_sent_at` timestamp NULL DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `hired_at` timestamp NULL DEFAULT NULL,
  `rejected_at` timestamp NULL DEFAULT NULL,
  `shortlisted_at` timestamp NULL DEFAULT NULL,
  `shortlisted_referral` enum('list_view','bulk_operator','details_view','admin_tool','chatbot','chat','invitation') DEFAULT NULL,
  `hired_referral` enum('bulk_operator','details_view','admin_tool','chatbot','chat','closure','list_view') DEFAULT NULL,
  `rejected_referral` enum('list_view','bulk_operator','details_view','admin_tool','chatbot','chat') DEFAULT NULL,
  `employer_dashboard_status` enum('opened','unopened') NOT NULL DEFAULT 'unopened',
  `opened_at` timestamp NULL DEFAULT NULL,
  `is_filtered_out` tinyint(1) NOT NULL DEFAULT '0',
  `is_sent_in_employer_digest` tinyint(1) NOT NULL DEFAULT '0',
  `is_internship_story_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `referral` enum('in_product_notification','web_push_notification','app_push_notification','mobile_web_push_notification','in_panel_notification','news_letter','matching_preferences','student_digest','app','student_digest_active','student_digest_inactive','search_matching_preferences','search_vanilla','search_filters','search_keyword','unknown','bookmark_web','bookmark_app','app_search_matching_preferences','app_search_keyword','app_search_vanilla','app_search_filters','app_unknown','similar_internships','app_similar_internships') DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `previous_session_last_seen_at` timestamp NULL DEFAULT NULL,
  `applied_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications_similar_jobs_data` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `similar_job_id` int(11) DEFAULT NULL,
  `is_clicked` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications_soft_seen_data` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `in_time` timestamp NULL DEFAULT NULL,
  `out_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `applications_status_queue` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `old_status` enum('open','shortlisted') DEFAULT NULL,
  `new_status` enum('rejected','shortlisted','hired') DEFAULT NULL,
  `processing_status` enum('to_be_processed','processing','processed','reverted') NOT NULL DEFAULT 'to_be_processed',
  `internshala_session_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `application_form_views` (
  `id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `visit_count_web` int(11) DEFAULT '0',
  `visit_count_app` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `application_reporting_reasons` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `professionalism_report_reason_id` int(11) NOT NULL,
  `status` enum('reported','contested','reverted','not reverted') DEFAULT NULL,
  `description` varchar(1024) NOT NULL,
  `options` enum('yes','no') DEFAULT NULL,
  `source` enum('list_view','individual_application','chat','chatbot') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `approved_colleges` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `used_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `app_notifications` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `message` varchar(256) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `click_type` enum('open_browser','open_fragment') DEFAULT NULL,
  `sending_type` enum('all','custom') DEFAULT NULL,
  `custom_csv_file` varchar(256) DEFAULT NULL,
  `custom_json_data` varchar(20480) DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `cron_processing_status` enum('to_process','processing','processed','error') NOT NULL DEFAULT 'to_process',
  `error` varchar(10240) DEFAULT NULL,
  `sending_status` enum('yet_to_send','sending','sent','paused') NOT NULL DEFAULT 'yet_to_send',
  `send_on` date DEFAULT NULL,
  `send_at` timestamp NULL DEFAULT NULL,
  `threads` int(11) NOT NULL DEFAULT '1',
  `sending_limit` int(11) NOT NULL DEFAULT '900',
  `total_clicks` int(11) NOT NULL DEFAULT '0',
  `share_clicks` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `app_notification_gcm_keys` (
  `id` int(11) NOT NULL,
  `app_notification_id` int(11) NOT NULL,
  `gcm_key_id` int(11) NOT NULL,
  `is_sent` tinyint(4) NOT NULL DEFAULT '0',
  `sent_at` timestamp NULL DEFAULT NULL,
  `delivered_at` timestamp NULL DEFAULT NULL,
  `clicked_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `app_rating_dialogs` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `modal_name` enum('1','2','3','1_new','2_new') DEFAULT NULL,
  `response` enum('yes','no','not_now','already_rated','rate_us','share_feedback') DEFAULT NULL,
  `modal_shown_date` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `banned_employer_details` (
  `id` int(11) NOT NULL,
  `type` enum('phone','email_domain','company_name') DEFAULT NULL,
  `value` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `banner_key_values` (
  `id` int(11) NOT NULL,
  `slider_banner_id` int(11) NOT NULL,
  `banner_template_key_id` int(11) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `banner_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `html_template` varchar(10240) NOT NULL,
  `css_template` varchar(10240) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `banner_template_keys` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `banner_template_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `blocked_domains` (
  `id` int(11) NOT NULL,
  `domain_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `boards` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `business_case_contests` (
  `id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  `preference` int(11) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `file_name_2` varchar(256) DEFAULT NULL,
  `file_name_1_width` int(11) DEFAULT NULL,
  `file_name_1_height` int(11) DEFAULT NULL,
  `file_name_2_width` int(11) DEFAULT NULL,
  `file_name_2_height` int(11) DEFAULT NULL,
  `solution` varchar(50000) DEFAULT NULL,
  `topic` varchar(512) DEFAULT NULL,
  `title` varchar(512) DEFAULT NULL,
  `is_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_image_dimension_processed` tinyint(1) NOT NULL DEFAULT '0',
  `score` varchar(256) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `business_case_contest_certificates` (
  `id` int(11) NOT NULL,
  `certificate_type` enum('participation','merit','top performer') NOT NULL,
  `business_case_contest_preference_id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `institute_name` varchar(200) DEFAULT NULL,
  `is_email_sent` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-19 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `business_case_contest_mail_templates` (
  `id` int(11) NOT NULL,
  `business_case_contest_preference_id` int(11) NOT NULL,
  `certificate_type` enum('merit','participation','top performer') NOT NULL,
  `from_email` varchar(256) NOT NULL,
  `from_name` varchar(256) NOT NULL,
  `subject` varchar(512) NOT NULL,
  `mail_template` varchar(10000) NOT NULL,
  `certificate_template` varchar(10000) DEFAULT NULL,
  `file_name` varchar(512) DEFAULT NULL,
  `bcc` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `business_case_contest_preferences` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `start_date_time` timestamp NULL DEFAULT NULL,
  `end_date_time` timestamp NULL DEFAULT NULL,
  `is_registration_started` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2017-01-19 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `b_emails` (
  `id` int(11) NOT NULL,
  `domain_name` varchar(126) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `campaigns` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `display_name` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `type` enum('offline_fair','tier_2_campaign','first_year_campaign','degree_campaign','campus_drive','engg_campaign','segment','branding','gie_campaign','internships_campaign','gsif_campaign','wfh_campaign') DEFAULT NULL,
  `status` enum('draft','active','deactivated') NOT NULL DEFAULT 'draft',
  `source` enum('tool','code') NOT NULL DEFAULT 'code',
  `contest_winners` varchar(512) DEFAULT NULL,
  `data` varchar(20000) DEFAULT NULL,
  `user_type` enum('student','employer','tnp') NOT NULL,
  `is_auto_register_on_landing` tinyint(1) NOT NULL,
  `start_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `early_access_start_date_time` timestamp NOT NULL,
  `launch_date_time` timestamp NULL DEFAULT NULL,
  `end_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `code` varchar(128) DEFAULT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_description` varchar(512) NOT NULL,
  `og_title` varchar(256) NOT NULL,
  `og_description` varchar(512) NOT NULL,
  `og_image` varchar(256) DEFAULT NULL,
  `og_image_dimensions` varchar(10) DEFAULT NULL,
  `og_image_html_head` varchar(1024) DEFAULT NULL,
  `og_image_html_body` varchar(1024) DEFAULT NULL,
  `dynamic_og_image_dimensions` varchar(10) DEFAULT NULL,
  `dynamic_og_image_html_head` varchar(1024) DEFAULT NULL,
  `dynamic_og_image_html_body` varchar(1024) DEFAULT NULL,
  `dynamic_og_image_version` int(11) NOT NULL DEFAULT '0',
  `keys_for_cache` varchar(2048) DEFAULT NULL,
  `to_clear_dynamic_cache` tinyint(1) NOT NULL DEFAULT '0',
  `whatsapp_share_msg` varchar(512) CHARACTER SET utf8mb4 DEFAULT NULL,
  `acknowledgement_mail_template_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaigns_users` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_eligible` tinyint(1) DEFAULT NULL,
  `param` varchar(256) DEFAULT NULL,
  `referral_count` int(11) NOT NULL DEFAULT '0',
  `referral_campaign_user_id` int(11) DEFAULT NULL,
  `has_early_access` tinyint(1) NOT NULL DEFAULT '0',
  `has_later_access` tinyint(1) NOT NULL DEFAULT '0',
  `unique_key` varchar(128) DEFAULT NULL,
  `is_present` tinyint(1) DEFAULT NULL,
  `utm_source` varchar(256) NOT NULL,
  `utm_medium` varchar(256) NOT NULL,
  `utm_campaign` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `campaign_experiment` (
  `id` int(14) NOT NULL,
  `campaign_user_id` int(14) NOT NULL,
  `behaviour_type` enum('no_modal','modal_with_skip','modal_without_skip','excluded') DEFAULT NULL,
  `preference_status` enum('submitted','skipped','already_filled') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_fair_application_details` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `campaign_user_id` int(11) DEFAULT NULL,
  `application_id` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `is_shortlisted` tinyint(1) NOT NULL DEFAULT '0',
  `interview_slot` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_fair_details` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `interview_slot_duration` int(4) NOT NULL,
  `interview_starts_at` timestamp NULL DEFAULT NULL,
  `interview_ends_at` timestamp NULL DEFAULT NULL,
  `attendance_sheet_path` varchar(512) NOT NULL,
  `max_interview_per_student` int(4) NOT NULL DEFAULT '1',
  `is_attendance_sheet_processed` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_fair_interviewees` (
  `id` int(11) NOT NULL,
  `campaign_fair_detail_id` int(11) NOT NULL,
  `student_code` varchar(15) NOT NULL,
  `student_id` int(11) NOT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_fair_interviewees_slots` (
  `id` int(11) NOT NULL,
  `campaign_fair_interviewee_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `interview_slot` timestamp NULL DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_faqs` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `campaign_mail_templates` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `mail_template_id` int(11) NOT NULL,
  `type` enum('acknowledgment_mail_before_launch','acknowledgment_mail_after_launch','referral_mail_before_early_access','referral_mail_during_early_access') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_sections` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `type` enum('banner','about','whats_in_it','timelines','referral','carousel','testimonial','media_partners','internships','faqs') NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `data` varchar(20000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_tnps` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `college_location_id` int(11) DEFAULT NULL,
  `tnp_name` varchar(256) DEFAULT NULL,
  `college_name` varchar(256) NOT NULL,
  `city_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_users_additional_info` (
  `id` int(11) NOT NULL,
  `campaign_user_id` int(11) NOT NULL,
  `campaign_key` varchar(256) NOT NULL,
  `value` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `campaign_user_fair_details` (
  `id` int(11) NOT NULL,
  `campaign_user_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `status` enum('invited','accepted','denied','expired','walkin') DEFAULT NULL,
  `shortlist_count` int(4) NOT NULL DEFAULT '0',
  `is_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `campaign_user_referral_details` (
  `id` int(11) NOT NULL,
  `campaign_user_id` int(11) NOT NULL,
  `referral_phase` varchar(32) DEFAULT NULL,
  `unlocked_on` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `career_page` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `experience` varchar(256) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `color` varchar(256) DEFAULT NULL,
  `link` varchar(512) DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `status` enum('active','inactive','draft') NOT NULL DEFAULT 'draft',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `label` varchar(256) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `to_display_for_search` enum('0','1') NOT NULL DEFAULT '0',
  `to_display_for_preferences` enum('0','1') NOT NULL DEFAULT '0',
  `linkedin_job_function_id` int(11) DEFAULT NULL,
  `linkedin_industry_code_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `categories_streams` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `category_scores` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `precise` varchar(64) NOT NULL DEFAULT '0',
  `recall` varchar(64) DEFAULT '0',
  `f1_score` varchar(64) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `certificates` (
  `id` int(11) NOT NULL,
  `certificate_campaign_id` int(11) NOT NULL,
  `unique_key` varchar(36) NOT NULL,
  `email` varchar(512) NOT NULL,
  `first_name` varchar(512) DEFAULT NULL,
  `last_name` varchar(512) DEFAULT NULL,
  `college_name` varchar(512) DEFAULT NULL,
  `additional_info_1` varchar(512) DEFAULT NULL,
  `additional_info_2` varchar(512) DEFAULT NULL,
  `additional_info_3` varchar(512) DEFAULT NULL,
  `is_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `certificate_campaigns` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `certificate_template` text NOT NULL,
  `orientation` enum('portrait','landscape') NOT NULL,
  `certificates_csv` varchar(512) NOT NULL,
  `status` enum('draft','file uploading','yet to schedule','scheduled','sending','sent','paused') NOT NULL,
  `from_email` varchar(512) DEFAULT NULL,
  `from_name` varchar(512) DEFAULT NULL,
  `reply_to_email` varchar(512) DEFAULT NULL,
  `reply_to_name` varchar(512) DEFAULT NULL,
  `bcc` varchar(512) DEFAULT NULL,
  `subject` varchar(1024) CHARACTER SET utf8mb4 DEFAULT NULL,
  `mail_template` varchar(10000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `scheduled_at` timestamp NULL DEFAULT NULL,
  `csv_errors` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `certificate_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `template` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `chatbot_logs` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `url` varchar(512) NOT NULL,
  `method` enum('post','get') NOT NULL,
  `payload` varchar(512) NOT NULL,
  `status` enum('success','failed') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `chat_conversations` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `is_seen_by_student` tinyint(1) NOT NULL DEFAULT '1',
  `is_seen_by_employer` tinyint(1) NOT NULL DEFAULT '1',
  `blocked_by` enum('student','employer') DEFAULT NULL,
  `chat_initation_referral` enum('bulk','list_view','application_detail','excel','invitation') CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-08-19 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `chat_conversation_extended` (
  `id` int(11) NOT NULL,
  `chat_conversation_id` int(11) NOT NULL,
  `chat_api_response_status` enum('response_required','response_not_required','not_predicted','api_failed') DEFAULT NULL,
  `chat_api_response_probability` float DEFAULT NULL,
  `student_response_reminder_status` enum('chat_blocked','application_rejected','employer_blocked','replied','access_expired1','access_expired2','sent','replied_after_reminder') DEFAULT NULL,
  `student_response_reminder_status_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `chat_emails_sent` (
  `id` int(11) NOT NULL,
  `chat_message_id` int(11) NOT NULL,
  `chat_conversation_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-toProcess, 1-mailSent, 2-emailCancel, 3-messageSeen. 4-userSuppressed',
  `created_at` timestamp NOT NULL DEFAULT '2018-08-19 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL,
  `chat_conversation_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `message` varchar(12000) DEFAULT NULL,
  `attachment` varchar(256) DEFAULT NULL,
  `filename` varchar(1024) DEFAULT NULL,
  `old_attachment` varchar(256) DEFAULT NULL,
  `mini_thumbnail` text,
  `sender` enum('student','employer') NOT NULL,
  `is_first_message_for_the_day_for_conversation` tinyint(1) NOT NULL DEFAULT '0',
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `seen_at` timestamp NULL DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2018-08-19 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `chat_message_thumbnails` (
  `id` int(11) NOT NULL,
  `chat_message_id` int(11) NOT NULL,
  `thumbnail` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-07-04 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `chat_users_presence` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_checked_at` timestamp NOT NULL DEFAULT '2000-01-01 06:30:00',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 06:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `city` (
  `COL 1` varchar(21) DEFAULT NULL,
  `city_name` varchar(18) DEFAULT NULL,
  `COL 3` varchar(18) DEFAULT NULL,
  `discount_id` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `city_clusters` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2016-12-13 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `city_clusters_cities` (
  `id` int(11) NOT NULL,
  `city_cluster_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `locality_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2016-12-13 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `city_specific_discounts` (
  `id` int(11) NOT NULL,
  `city_name` varchar(256) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `ci_sessions` (
  `rid` int(11) NOT NULL,
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_from_trainings` tinyint(1) DEFAULT '0',
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `classified_emails` (
  `id` int(11) NOT NULL,
  `status` enum('to_be_processed','processing','tag_computed','action_taken','na') NOT NULL DEFAULT 'to_be_processed',
  `type` enum('employer') NOT NULL DEFAULT 'employer',
  `message_id` varchar(512) DEFAULT NULL,
  `email_from` varchar(256) DEFAULT NULL,
  `employer_email` varchar(256) DEFAULT NULL,
  `email_name` varchar(256) DEFAULT NULL,
  `original_subject` varchar(512) DEFAULT NULL,
  `subject` varchar(512) DEFAULT NULL,
  `body` varchar(51200) DEFAULT NULL,
  `identifier` varchar(128) DEFAULT NULL,
  `received_at` timestamp NULL DEFAULT NULL,
  `received_on` date DEFAULT NULL,
  `reply_to_id` varchar(2048) DEFAULT NULL,
  `reference_id` varchar(2048) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `w2v_predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general') DEFAULT NULL,
  `tfidf_predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general') DEFAULT NULL,
  `w2v_probability` decimal(10,5) DEFAULT NULL,
  `tfidf_probability` decimal(10,5) DEFAULT NULL,
  `predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general','delete_account') DEFAULT NULL,
  `probability` decimal(10,5) DEFAULT NULL,
  `description` varchar(5012) DEFAULT NULL,
  `is_mail_sent` tinyint(1) DEFAULT NULL,
  `additional_info` varchar(1024) DEFAULT NULL,
  `mail` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `classified_emails_help_center` (
  `id` int(11) NOT NULL,
  `status` enum('to_be_processed','processing','tag_computed','action_taken','na') NOT NULL DEFAULT 'to_be_processed',
  `type` enum('employer') NOT NULL DEFAULT 'employer',
  `message_id` varchar(512) DEFAULT NULL,
  `email_from` varchar(256) DEFAULT NULL,
  `employer_email` varchar(256) DEFAULT NULL,
  `email_name` varchar(256) DEFAULT NULL,
  `original_subject` varchar(512) DEFAULT NULL,
  `subject` varchar(512) DEFAULT NULL,
  `body` varchar(51200) DEFAULT NULL,
  `identifier` varchar(256) DEFAULT NULL,
  `received_at` timestamp NULL DEFAULT NULL,
  `received_on` date DEFAULT NULL,
  `reply_to_id` varchar(2048) DEFAULT NULL,
  `reference_id` varchar(2048) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `w2v_predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general','failed') DEFAULT NULL,
  `tfidf_predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general','failed') DEFAULT NULL,
  `w2v_probability` decimal(10,5) DEFAULT NULL,
  `tfidf_probability` decimal(10,5) DEFAULT NULL,
  `description` varchar(5012) DEFAULT NULL,
  `is_mail_sent` tinyint(1) DEFAULT NULL,
  `additional_info` varchar(1024) DEFAULT NULL,
  `mail` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `classified_emails_trainings` (
  `id` int(11) NOT NULL,
  `status` enum('to_be_processed','processing','tag_computed','action_taken','na') NOT NULL DEFAULT 'to_be_processed',
  `type` enum('employer') NOT NULL DEFAULT 'employer',
  `message_id` varchar(512) DEFAULT NULL,
  `email_from` varchar(256) DEFAULT NULL,
  `employer_email` varchar(256) DEFAULT NULL,
  `email_name` varchar(256) DEFAULT NULL,
  `original_subject` varchar(512) DEFAULT NULL,
  `subject` text,
  `body` varchar(51200) DEFAULT NULL,
  `received_at` timestamp NULL DEFAULT NULL,
  `received_on` date DEFAULT NULL,
  `reply_to_id` varchar(2048) DEFAULT NULL,
  `reference_id` varchar(2048) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general') DEFAULT NULL,
  `probability` decimal(10,5) DEFAULT NULL,
  `description` varchar(5012) DEFAULT NULL,
  `is_mail_sent` tinyint(1) DEFAULT NULL,
  `additional_info` varchar(1024) DEFAULT NULL,
  `mail` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `classified_emails_trainings_bk_new` (
  `id` int(11) NOT NULL,
  `status` enum('to_be_processed','processing','tag_computed','action_taken','na') NOT NULL DEFAULT 'to_be_processed',
  `type` enum('employer') NOT NULL DEFAULT 'employer',
  `message_id` varchar(512) DEFAULT NULL,
  `email_from` varchar(256) DEFAULT NULL,
  `employer_email` varchar(256) DEFAULT NULL,
  `email_name` varchar(256) DEFAULT NULL,
  `original_subject` varchar(512) DEFAULT NULL,
  `subject` text,
  `body` varchar(51200) DEFAULT NULL,
  `received_at` timestamp NULL DEFAULT NULL,
  `received_on` date DEFAULT NULL,
  `reply_to_id` varchar(2048) DEFAULT NULL,
  `reference_id` varchar(2048) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `predicted_tag` enum('approve_internship','change_application_status','edit_stipend','extend_access','request_template','general') DEFAULT NULL,
  `probability` decimal(10,5) DEFAULT NULL,
  `description` varchar(5012) DEFAULT NULL,
  `is_mail_sent` tinyint(1) DEFAULT NULL,
  `additional_info` varchar(1024) DEFAULT NULL,
  `mail` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_app_contents` (
  `id` int(11) NOT NULL,
  `cms_content_id` int(11) DEFAULT NULL,
  `content` varchar(20000) NOT NULL,
  `display_count` int(11) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_cities` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_contents` (
  `id` int(30) NOT NULL,
  `training_campaign_id` int(30) DEFAULT NULL,
  `slot_id` int(50) NOT NULL,
  `cms_template_id` int(11) DEFAULT NULL,
  `content` varchar(15000) DEFAULT NULL,
  `content_mob` varchar(5000) DEFAULT NULL,
  `type` enum('image','text','facebook','google','native','vtc-native','internshala-native','ftu-native','college-specific','college-specific-generic','college-specific-generic-ftu-native','college-specific-ftu-native','campaign-native') NOT NULL,
  `ad_link` varchar(200) DEFAULT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `clicks` int(50) NOT NULL DEFAULT '0',
  `display_count` int(50) NOT NULL DEFAULT '0',
  `show_on_internship_search_page_home` tinyint(1) NOT NULL DEFAULT '1',
  `show_on_keyword_search_pages` tinyint(1) NOT NULL DEFAULT '1',
  `show_on_company_specific_pages` tinyint(1) NOT NULL DEFAULT '1',
  `show_on_work_from_home_internships_pages` tinyint(1) NOT NULL DEFAULT '1',
  `show_on_category_specific_pages` enum('all','none','no_category_assigned_pages','select_a_few') NOT NULL DEFAULT 'all',
  `show_on_location_specific_pages` enum('all','none','no_location_assigned_pages','select_few_cities','select_few_states') NOT NULL DEFAULT 'all',
  `visible_to` enum('all','student','not_student') NOT NULL DEFAULT 'all',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_contents_categories` (
  `id` int(11) NOT NULL,
  `cms_content_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_contents_internshala_devices` (
  `id` int(11) NOT NULL,
  `cms_content_id` int(11) DEFAULT NULL,
  `cms_app_content_id` int(11) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `display_counter` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_contents_locations` (
  `id` int(11) NOT NULL,
  `cms_content_id` int(11) NOT NULL,
  `cms_city_id` int(11) DEFAULT NULL,
  `cms_state_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `Last_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_content_key_values` (
  `id` int(11) NOT NULL,
  `cms_content_id` int(11) NOT NULL,
  `cms_template_key_id` int(11) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_states` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `template` varchar(10240) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `cms_template_keys` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `cms_template_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `google_college_id` int(11) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `website` varchar(1024) DEFAULT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `colleges_aicte` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `google_name` varchar(512) DEFAULT NULL,
  `unique_key` varchar(128) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `colleges_extended` (
  `id` int(11) NOT NULL,
  `college_id` int(11) DEFAULT NULL,
  `google_name` varchar(512) DEFAULT NULL,
  `g_status` varchar(128) DEFAULT NULL,
  `unique_key` varchar(512) DEFAULT NULL,
  `google_college_first_id` int(11) DEFAULT NULL,
  `is_google_university` tinyint(1) NOT NULL DEFAULT '0',
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `premium_college_id` int(11) DEFAULT '-1',
  `location_id` int(11) DEFAULT NULL,
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `college_discount_coupon_codes` (
  `id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `abbr` varchar(256) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `college_names` (
`name` varchar(256)
); 
 


CREATE TABLE `college_registrations` (
  `id` int(11) NOT NULL,
  `unique_key` varchar(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `phone_primary` varchar(10) NOT NULL,
  `college_name` varchar(500) NOT NULL,
  `designation` enum('Training and placement officer','Head of department','Professor','Principal','Vice Chancellor','Dean','Placement team member','Student','Other','') DEFAULT NULL,
  `file_path` varchar(500) DEFAULT NULL,
  `is_whatsapp_subscription_checked` tinyint(2) NOT NULL DEFAULT '1',
  `is_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_early_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_final_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `state_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `registered_through` enum('task','task_engineering','task_degree','task_diploma','india_employed','ictak','contact','tier2_canonical','contact_gsif','contact_ugc','tnp_dashboard','ur_associate','recruitment_fair') DEFAULT NULL,
  `is_approved` tinyint(2) NOT NULL DEFAULT '0',
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `college_registration_bulk_students` (
  `id` int(11) NOT NULL,
  `college_registration_students_csv_id` int(11) DEFAULT NULL,
  `student_email` varchar(256) NOT NULL,
  `student_first_name` varchar(256) NOT NULL,
  `student_last_name` varchar(256) DEFAULT NULL,
  `student_phone_primary` varchar(20) DEFAULT '9999999999',
  `tnp_first_name` varchar(256) NOT NULL,
  `tnp_last_name` varchar(256) DEFAULT NULL,
  `tnp_email` varchar(256) NOT NULL,
  `tnp_country_code` varchar(8) DEFAULT '+91',
  `tnp_phone_primary` varchar(16) NOT NULL,
  `college_name` varchar(256) NOT NULL,
  `registered_through` varchar(32) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `unprocessed_reason` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `college_registration_files` (
  `id` int(11) NOT NULL,
  `college_registration_id` int(11) NOT NULL,
  `file_path` varchar(500) NOT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `college_registration_students` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `college_registration_bulk_student_id` int(11) DEFAULT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `phone_primary` varchar(20) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `tnp_id` int(11) DEFAULT NULL,
  `registered_through` enum('india_employed','task','task_engineering','task_degree','task_diploma','manually','tier2_canonical','contact_gsif','tnp_dashboard','ur_associate','recruitment_fair') DEFAULT NULL,
  `degree_name` varchar(256) DEFAULT NULL,
  `end_year_of_study` int(4) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `is_already_registered` int(4) NOT NULL DEFAULT '0',
  `verification_mail_sent_at` timestamp NULL DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `college_registration_students_csv` (
  `id` int(11) NOT NULL,
  `tnp_id` int(11) DEFAULT NULL,
  `csv_path` varchar(512) NOT NULL,
  `registered_through` enum('india_employed','task','task_engineering','task_degree','task_diploma','tier2_canonical','contact_gsif','ur_associate','recruitment_fair') DEFAULT NULL,
  `status` enum('to_be_processed','processed','invalid','processing') NOT NULL DEFAULT 'to_be_processed',
  `is_mail_sent` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `type` enum('Academic','Corporate','NGO','Government','Startup') DEFAULT NULL,
  `is_big_brand` tinyint(1) NOT NULL DEFAULT '0',
  `is_cleaned` tinyint(1) NOT NULL DEFAULT '0',
  `facebook_page` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `contest_faqs` (
  `id` int(11) NOT NULL,
  `business_case_contest_preference_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `contest_icons` (
  `id` int(11) NOT NULL,
  `business_case_contest_preference_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `crons` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `is_run_on_monday` tinyint(1) NOT NULL DEFAULT '0',
  `is_run_on_tuesday` tinyint(1) NOT NULL DEFAULT '0',
  `is_run_on_wednesday` tinyint(1) NOT NULL DEFAULT '0',
  `is_run_on_thursday` tinyint(1) NOT NULL DEFAULT '0',
  `is_run_on_friday` tinyint(1) NOT NULL DEFAULT '0',
  `is_run_on_saturday` tinyint(1) NOT NULL DEFAULT '0',
  `is_run_on_sunday` tinyint(1) NOT NULL DEFAULT '0',
  `min_threshold` int(11) DEFAULT NULL,
  `max_threshold` int(11) DEFAULT NULL,
  `max_timetaken` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `crons_stats` (
  `id` int(11) NOT NULL,
  `cron_id` int(11) NOT NULL,
  `cron_start_at` timestamp NULL DEFAULT NULL,
  `cron_end_at` timestamp NULL DEFAULT NULL,
  `computation_count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `curl_requests` (
  `id` int(11) NOT NULL,
  `type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_method` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_body` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curl_options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_code` int(11) NOT NULL,
  `response` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `error_no` int(11) NOT NULL DEFAULT '0',
  `error` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `c_emails` (
  `domain_name` varchar(126) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `decline_reasons` (
  `id` int(11) NOT NULL,
  `employment_type` enum('internship','job') NOT NULL DEFAULT 'internship',
  `parent_id` int(11) DEFAULT NULL,
  `reason` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `degrees` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `degree_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `degree_groups_degrees` (
  `id` int(11) NOT NULL,
  `degree_group_id` int(11) DEFAULT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `value` float NOT NULL,
  `value_rs` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `coupon_limit` varchar(255) NOT NULL,
  `number_of_times_coupon_used` varchar(255) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `contact_email` varchar(200) NOT NULL,
  `isp_group` int(11) DEFAULT NULL,
  `to_show_resume_score_calculator` tinyint(2) DEFAULT '0',
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `emails_to_block` (
  `id` int(11) NOT NULL,
  `email` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `email_change_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_key` varchar(256) NOT NULL,
  `old_email` varchar(256) NOT NULL,
  `new_email` varchar(256) NOT NULL,
  `status` enum('unconfirmed','confirmed','expired') NOT NULL,
  `is_em_sync_done` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `email_daily_stats` (
  `id` int(11) NOT NULL,
  `source` enum('internshala','trainings') DEFAULT NULL,
  `type` enum('transactional','promotional','product_marketing') DEFAULT NULL,
  `date` date DEFAULT NULL,
  `mails_sent` bigint(20) NOT NULL DEFAULT '0',
  `unique_mails_sent` bigint(20) NOT NULL DEFAULT '0',
  `mails_tracked` bigint(20) NOT NULL DEFAULT '0',
  `opened` bigint(20) NOT NULL DEFAULT '0',
  `bounced` int(11) NOT NULL DEFAULT '0',
  `permanent_bounces` int(11) DEFAULT '0',
  `transient_bounces` int(11) DEFAULT '0',
  `complaints` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `email_daily_stats_entries` (
  `id` int(11) NOT NULL,
  `source` enum('internshala','trainings') DEFAULT NULL,
  `type` enum('transactional','promotional','product_marketing') DEFAULT NULL,
  `date` date DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `extra_mails` int(11) DEFAULT '0',
  `is_tracked` tinyint(4) DEFAULT '0',
  `is_from_queue` tinyint(4) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `email_list` (
  `email` varchar(126) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `email_stats` (
  `id` int(11) NOT NULL,
  `mail_to` varchar(5000) DEFAULT NULL,
  `reply_to` varchar(5000) DEFAULT NULL,
  `subject` varchar(1024) CHARACTER SET utf8mb4 DEFAULT NULL,
  `message` mediumtext CHARACTER SET utf8mb4,
  `attachment_path` varchar(1024) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `source` enum('internshala','trainings') DEFAULT NULL,
  `is_email_sent` tinyint(1) DEFAULT NULL,
  `email_message_id` varchar(512) DEFAULT NULL,
  `email_response` varchar(10000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `sns_status` varchar(512) DEFAULT NULL,
  `sns_response` text,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `email_tracking_stats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mail_tracker_type` varchar(256) NOT NULL,
  `email_client` varchar(2048) DEFAULT NULL,
  `unique_key` varchar(256) NOT NULL,
  `sent_on` date DEFAULT NULL,
  `opened_at` timestamp NULL DEFAULT NULL,
  `clicked_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `salutation` enum('mr','mrs','ms','dr') DEFAULT NULL,
  `profile` enum('incomplete','complete') DEFAULT 'incomplete',
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT '+91',
  `phone_primary` varchar(10) DEFAULT NULL,
  `is_phone_verified` tinyint(2) NOT NULL DEFAULT '0',
  `linkedin_page` varchar(255) DEFAULT NULL,
  `digest_sent_at` timestamp NULL DEFAULT NULL,
  `job_digest_sent_at` timestamp NULL DEFAULT NULL,
  `comments` varchar(5000) DEFAULT NULL,
  `authenticated_by_admin_id` int(11) DEFAULT NULL,
  `is_day_one_activation_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_day_one_post_internship_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `day_one_post_internship_reminder_sent_at` timestamp NULL DEFAULT NULL,
  `is_day_three_post_internship_reminder_sent` tinyint(4) NOT NULL DEFAULT '0',
  `is_first_hired_email_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_first_shortlisted_email_sent` tinyint(1) NOT NULL DEFAULT '0',
  `unsubscribe_from_daily_digest` tinyint(1) NOT NULL DEFAULT '0',
  `is_flexible_work_hours_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `is_informal_dress_code_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `is_five_days_a_week_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `is_snacks_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `referral` varchar(128) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `registration_source` enum('internship','job') NOT NULL DEFAULT 'internship',
  `is_fast_processing_ignored` tinyint(4) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employers_highlights` (
  `id` int(11) NOT NULL,
  `employer_company_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  `highlight_id` int(11) NOT NULL,
  `type` enum('personal','company') DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `status` enum('active','closed','inactive') NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '2018-01-01 11:11:11',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employers_notifications` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `is_action_taken` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_authentication` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `employer_authentication_method_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_authentication_methods` (
  `id` int(11) NOT NULL,
  `method` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_bulk_requests` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `unique_key` varchar(256) NOT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `processing_status` enum('draft','to_be_processed','processing','processed','make_excel') NOT NULL DEFAULT 'to_be_processed',
  `link` varchar(256) DEFAULT NULL,
  `status` enum('open','shortlisted','hired') DEFAULT NULL,
  `type` enum('excel','chat') DEFAULT 'excel',
  `data` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_bulk_request_applications` (
  `id` int(11) NOT NULL,
  `employer_bulk_request_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `processing_status` enum('to_be_processed','processed','garbage') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_bulk_request_excel_applications` (
  `id` int(11) NOT NULL,
  `employer_bulk_request_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `student_full_name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `current_city` varchar(256) DEFAULT NULL,
  `skills` varchar(5000) DEFAULT NULL,
  `other_skills` varchar(5000) DEFAULT NULL,
  `assessment_questions` text,
  `current_education` varchar(5000) DEFAULT NULL,
  `performance_education` varchar(5000) DEFAULT NULL,
  `others` varchar(5000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_cin_verification_requests` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `cin` varchar(64) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `unique_key` varchar(36) DEFAULT NULL,
  `token` varchar(64) DEFAULT NULL,
  `cin_company_name` varchar(256) DEFAULT NULL,
  `cin_email` varchar(128) DEFAULT NULL,
  `email_sent_count` int(11) DEFAULT '0',
  `last_email_sent_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_companies` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `logo` varchar(1024) DEFAULT NULL,
  `is_google_logo_updated` tinyint(1) NOT NULL DEFAULT '0',
  `logo_exists` int(11) NOT NULL DEFAULT '2',
  `detail_status` enum('pending','approved') DEFAULT 'pending',
  `is_self` tinyint(4) NOT NULL DEFAULT '0',
  `authentication_type` enum('website','social_media','others') DEFAULT NULL,
  `authentication_status` enum('approved','pending','incomplete','follow_up') DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(512) DEFAULT NULL,
  `instagram` varchar(512) DEFAULT NULL,
  `linkedin` varchar(512) DEFAULT NULL,
  `youtube` varchar(512) DEFAULT NULL,
  `doc_url` varchar(512) DEFAULT NULL,
  `cin` varchar(128) DEFAULT NULL,
  `is_cin_verified` tinyint(2) NOT NULL DEFAULT '0',
  `linkedin_na` tinyint(4) DEFAULT NULL,
  `cron_auth_status` enum('to_be_processed','processing','processed') DEFAULT 'to_be_processed',
  `org_details_approved_by_admin_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_companies_auto_saved` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `logo` varchar(1024) DEFAULT NULL,
  `is_website_popover_clicked` int(11) NOT NULL DEFAULT '0',
  `company_name` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_feedbacks` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `rating` enum('poor','average','good','awesome') NOT NULL,
  `feedback` varchar(5000) NOT NULL,
  `testimonial` varchar(5000) DEFAULT NULL,
  `is_recommend_selected` tinyint(1) NOT NULL,
  `is_recommend_shown` tinyint(1) NOT NULL,
  `is_replied_to_feedback` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_mail_processing_tracking` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `utm_source` varchar(128) DEFAULT NULL,
  `action` enum('opened','shortlisted','rejected','hired','mail_sent') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `employer_social_connects` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `profile_type` enum('fb_connect','li_connect','insta_connect','youtube_connect','li_personal') NOT NULL,
  `profile_url` varchar(512) NOT NULL,
  `organization_name` varchar(512) DEFAULT NULL,
  `personal_name` varchar(512) DEFAULT NULL,
  `followers` int(11) DEFAULT NULL,
  `activity` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `error_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` enum('user','student','employer','training_partner','tnp','admin') NOT NULL DEFAULT 'user',
  `level` varchar(256) DEFAULT NULL,
  `ip` varchar(256) DEFAULT NULL,
  `request_method` varchar(256) DEFAULT NULL,
  `request_url` varchar(1024) DEFAULT NULL,
  `referral_url` varchar(1024) DEFAULT NULL,
  `error_message` mediumtext,
  `user_agent` varchar(8192) DEFAULT NULL,
  `device_type` varchar(256) DEFAULT NULL,
  `os` varchar(256) DEFAULT NULL,
  `mobile_os` varchar(256) DEFAULT NULL,
  `browser` varchar(256) DEFAULT NULL,
  `mobile_browser` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `error_logs_email` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` enum('user','student','employer','training_partner','tnp','admin') NOT NULL DEFAULT 'user',
  `level` varchar(256) DEFAULT NULL,
  `ip` varchar(256) DEFAULT NULL,
  `request_method` varchar(256) DEFAULT NULL,
  `request_url` varchar(1024) DEFAULT NULL,
  `referral_url` varchar(1024) DEFAULT NULL,
  `error_message` mediumtext,
  `user_agent` varchar(256) DEFAULT NULL,
  `device_type` varchar(256) DEFAULT NULL,
  `os` varchar(256) DEFAULT NULL,
  `mobile_os` varchar(256) DEFAULT NULL,
  `browser` varchar(256) DEFAULT NULL,
  `mobile_browser` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `error_logs_general` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` enum('user','student','employer','training_partner','tnp','admin') NOT NULL DEFAULT 'user',
  `level` varchar(256) DEFAULT NULL,
  `ip` varchar(256) DEFAULT NULL,
  `request_method` varchar(256) DEFAULT NULL,
  `request_url` varchar(1024) DEFAULT NULL,
  `referral_url` varchar(1024) DEFAULT NULL,
  `error_message` mediumtext,
  `user_agent` varchar(8192) DEFAULT NULL,
  `device_type` varchar(256) DEFAULT NULL,
  `os` varchar(256) DEFAULT NULL,
  `mobile_os` varchar(256) DEFAULT NULL,
  `browser` varchar(256) DEFAULT NULL,
  `mobile_browser` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `error_logs_sql` (
  `id` int(11) NOT NULL,
  `sql_query` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `exam_specific_mailer` (
  `id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `college_abbr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yog` int(11) DEFAULT NULL,
  `is_mail_sent` tinyint(4) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 
 


CREATE TABLE `experience_organizations` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `external_internship_companies` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `logo` varchar(128) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `flask_access_logs` (
  `id` int(11) NOT NULL,
  `internship_id` int(11) DEFAULT NULL,
  `type` varchar(256) NOT NULL,
  `request_url` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `follow_up_types` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `gcm_keys` (
  `id` int(11) NOT NULL,
  `gcm_key` varchar(512) NOT NULL,
  `is_canonical` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('registered','unregistered','unavailable','invalid registration','canonical') NOT NULL DEFAULT 'registered',
  `installed_version` varchar(10) NOT NULL DEFAULT '1',
  `is_notification_sent` tinyint(4) DEFAULT '0',
  `is_trainings_notification_sent` tinyint(1) DEFAULT '0',
  `is_notification_to_college_student` tinyint(1) DEFAULT '2',
  `extras` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `gcm_users` (
  `id` int(11) NOT NULL,
  `gcm_key_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_at` timestamp NULL DEFAULT NULL,
  `is_logged_in` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `gmail_referral_logs` (
  `id` int(11) NOT NULL,
  `referral_user_id` int(11) NOT NULL,
  `referred_to_email` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_applications` (
  `id` int(11) NOT NULL,
  `google_internship_id` int(11) NOT NULL,
  `google_student_detail_id` int(11) NOT NULL,
  `cover_letter` mediumtext NOT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `unique_key` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_clean_companies` (
  `id` int(11) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `g_name` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_colleges` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `is_premium_college` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_colleges_locations` (
  `id` int(11) NOT NULL,
  `google_college_id` int(11) DEFAULT NULL,
  `unique_key` varchar(256) NOT NULL,
  `place_id` varchar(512) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `is_college` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_college_types` (
  `id` int(11) NOT NULL,
  `google_college_id` int(11) DEFAULT NULL,
  `type` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_companies` (
  `id` int(11) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `start` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_companies_experience_temp` (
  `exp` varchar(1024) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `type` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_form_res` (
  `id` int(11) NOT NULL,
  `c1` varchar(256) DEFAULT NULL,
  `c2` varchar(256) DEFAULT NULL,
  `c3` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_internships` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `category` enum('tech','management') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_jobs` (
  `id` int(11) NOT NULL,
  `google_company_id` int(11) DEFAULT NULL,
  `profile` varchar(512) DEFAULT NULL,
  `type` enum('white collar','blue collar','na') DEFAULT NULL,
  `company` varchar(256) DEFAULT NULL,
  `internshala_company` varchar(1024) DEFAULT NULL,
  `google_company` varchar(1024) DEFAULT NULL,
  `google_internshala_company` varchar(1024) DEFAULT NULL,
  `percentage_match` decimal(10,0) DEFAULT NULL,
  `is_processed` tinyint(4) DEFAULT '0',
  `is_company_matched` tinyint(4) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `salary` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `salary_clean` decimal(10,2) DEFAULT NULL,
  `posted_on` varchar(256) DEFAULT NULL,
  `posted_on_date` date DEFAULT NULL,
  `location` varchar(512) DEFAULT NULL,
  `link` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_jobs_apply_on` (
  `id` int(11) NOT NULL,
  `google_job_id` int(11) DEFAULT NULL,
  `apply_on` varchar(256) DEFAULT NULL,
  `apply_on_link` varchar(4096) DEFAULT NULL,
  `is_processed` tinyint(4) NOT NULL DEFAULT '0',
  `job_details` text,
  `posted_on` date DEFAULT NULL,
  `valid_through` date DEFAULT NULL,
  `experience` varchar(10000) DEFAULT NULL,
  `is_fresher` varchar(8) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `google_student_details` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `gender` enum('mr','ms','other','do_not_wish_to_specify','mrs') NOT NULL,
  `computer_languages` varchar(2000) NOT NULL,
  `course_pursuing` enum('under-graduate engineering degree','post-graduate engineering degree','under-graduate nontech degree','post-graduate nontech degree','mba') DEFAULT NULL,
  `home_town` int(11) NOT NULL,
  `college_location` int(11) NOT NULL,
  `graduation_degree` varchar(256) NOT NULL,
  `is_other_graduation_degree` tinyint(1) NOT NULL DEFAULT '0',
  `graduation_stream` varchar(256) NOT NULL,
  `is_other_graduation_stream` tinyint(1) DEFAULT '0',
  `graduation_institute` varchar(512) NOT NULL,
  `graduation_marks` decimal(5,2) NOT NULL,
  `graduation_marks_scale` enum('4','10','100') NOT NULL,
  `post_graduation_degree` varchar(256) DEFAULT NULL,
  `is_other_post_graduation_degree` tinyint(1) DEFAULT '0',
  `post_graduation_stream` varchar(256) DEFAULT NULL,
  `is_other_post_graduation_stream` tinyint(1) NOT NULL DEFAULT '0',
  `post_graduation_institute` varchar(512) DEFAULT NULL,
  `post_graduation_marks` decimal(5,2) DEFAULT NULL,
  `post_graduation_marks_scale` enum('4','10','100') DEFAULT NULL,
  `total_work_experience` int(11) NOT NULL,
  `experience_1_duration` int(11) DEFAULT NULL,
  `experience_1_company` varchar(256) DEFAULT NULL,
  `experience_1_designation` varchar(256) DEFAULT NULL,
  `experience_1_description` varchar(2048) DEFAULT NULL,
  `experience_2_duration` int(11) DEFAULT NULL,
  `experience_2_company` varchar(256) DEFAULT NULL,
  `experience_2_designation` varchar(256) DEFAULT NULL,
  `experience_2_description` varchar(2048) DEFAULT NULL,
  `experience_3_duration` int(11) DEFAULT NULL,
  `experience_3_company` varchar(256) DEFAULT NULL,
  `experience_3_designation` varchar(256) DEFAULT NULL,
  `experience_3_description` varchar(2048) DEFAULT NULL,
  `availability` enum('april_may_2018','june_july_2018') DEFAULT NULL,
  `available_from` date NOT NULL,
  `available_to` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `hall_of_fame_details` (
  `id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `help_center_categories` (
  `id` int(11) NOT NULL,
  `user_type` enum('student','employer','employer_job') NOT NULL,
  `category_name` varchar(512) NOT NULL,
  `subheading` varchar(512) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `help_center_faqs` (
  `id` int(11) NOT NULL,
  `help_center_category_id` int(11) NOT NULL,
  `question` varchar(1024) NOT NULL,
  `solution` varchar(5000) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `help_center_query_box` (
  `id` int(11) NOT NULL,
  `user_type` enum('student','employer','employer_job') NOT NULL,
  `heading` varchar(512) NOT NULL,
  `placeholder` varchar(512) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `help_center_query_history` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `name` varchar(512) NOT NULL,
  `help_center_query_box_id` int(11) NOT NULL,
  `query` varchar(2048) NOT NULL,
  `attachment` varchar(2048) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `highlights` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internshala_devices` (
  `id` int(11) NOT NULL,
  `device_key` varchar(128) DEFAULT NULL,
  `device` varchar(64) DEFAULT NULL,
  `source` enum('web','app','chatbot') DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internshala_device_behaviour` (
  `id` int(11) NOT NULL,
  `internshala_device_id` int(111) DEFAULT NULL,
  `behaviour` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internshala_sessions` (
  `id` int(11) NOT NULL,
  `persistent_session` varchar(128) DEFAULT NULL,
  `token` varchar(126) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `session_started_at` timestamp NULL DEFAULT NULL,
  `session_started_on` date DEFAULT NULL,
  `source` enum('web','app','chatbot') DEFAULT NULL,
  `utm_source` varchar(64) DEFAULT NULL,
  `utm_medium` varchar(64) DEFAULT NULL,
  `utm_campaign` varchar(64) DEFAULT NULL,
  `is_logged_in` tinyint(1) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `is_vpn_or_proxy` tinyint(1) DEFAULT NULL,
  `browser` varchar(64) DEFAULT NULL,
  `user_agent` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internshala_trainings` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internshala_view_caches` (
  `id` int(11) NOT NULL,
  `type` enum('filters','autocomplete','search_page') DEFAULT NULL,
  `cache_id` varchar(64) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cache` mediumtext,
  `expires_at` timestamp NULL DEFAULT NULL,
  `expires_on` date DEFAULT NULL,
  `cache_id_string` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_day_files` (
  `id` int(11) NOT NULL,
  `internship_day_signup_id` int(11) NOT NULL,
  `file_path` varchar(500) NOT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_day_participants` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `college_name` varchar(256) NOT NULL,
  `name` varchar(128) NOT NULL,
  `unique_key` varchar(64) DEFAULT NULL,
  `is_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_day_signups` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `email` varchar(256) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `college_name` varchar(512) NOT NULL,
  `designation` enum('Training and placement officer','Head of department','Professor','Principal','Vice Chancellor','Dean','Placement team member','Student','Other') DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `file_path` varchar(512) DEFAULT NULL,
  `unique_key` varchar(36) NOT NULL,
  `is_whatsapp_subscription_checked` tinyint(2) NOT NULL DEFAULT '1',
  `is_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_sheet_processed` tinyint(1) NOT NULL DEFAULT '0',
  `utm_source` varchar(64) DEFAULT NULL,
  `utm_medium` varchar(64) DEFAULT NULL,
  `utm_campaign` varchar(64) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_day_signups_2018` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `college_name` varchar(512) NOT NULL,
  `designation` enum('Training and placement officer','Head of department','Professor','Principal','Vice Chancellor','Dean','Placement team member','Student','Other') DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `file_path` varchar(512) DEFAULT NULL,
  `unique_key` varchar(36) NOT NULL,
  `is_whatsapp_subscription_checked` tinyint(2) NOT NULL DEFAULT '1',
  `is_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_sheet_processed` tinyint(1) NOT NULL DEFAULT '0',
  `utm_source` varchar(64) DEFAULT NULL,
  `utm_medium` varchar(64) DEFAULT NULL,
  `utm_campaign` varchar(64) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_day_tnps` (
  `id` int(11) NOT NULL,
  `unique_key` varchar(36) NOT NULL,
  `college_name` varchar(500) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `phone_primary` varchar(10) NOT NULL,
  `is_reminder_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_day_tnp_guests` (
  `id` int(11) NOT NULL,
  `internship_day_tnp_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `internship_stipend` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `stipend_type` enum('fixed','variable','lumpsum','performance','unpaid') DEFAULT NULL,
  `salary` int(8) DEFAULT NULL,
  `salary2` int(11) DEFAULT NULL,
  `salary_scale` enum('permonth','perweek','lumpsum') DEFAULT NULL,
  `salary_performance` int(11) DEFAULT NULL,
  `salary_performance_currency` enum('rs','dollar','pound_sterling','euro','swiss_franc','singapore_dollar','yen','nis','jpy','aed','lkr') DEFAULT NULL,
  `salary_currency` enum('rs','dollar','pound_sterling','euro','swiss_franc','singapore_dollar','yen','nis','jpy','aed','lkr') DEFAULT NULL,
  `salary_performance_scale_id` int(11) DEFAULT NULL,
  `salary_performance_additional_details` varchar(5000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `ip_blocked_stats` (
  `id` int(11) NOT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `type` enum('registration','subscription','permanent') DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL,
  `blocked_till` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `ip_countries` (
  `id` int(11) NOT NULL,
  `ip_from` bigint(20) DEFAULT NULL,
  `ip_to` bigint(20) DEFAULT NULL,
  `country_code` varchar(1024) DEFAULT NULL,
  `country_name` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isps_to_exclude` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_applications` (
  `id` int(11) NOT NULL,
  `isp_version_id` int(11) NOT NULL,
  `email` varchar(512) NOT NULL,
  `first_name` varchar(512) NOT NULL,
  `last_name` varchar(512) DEFAULT NULL,
  `country_code` varchar(8) DEFAULT '+91',
  `mobile` varchar(16) NOT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `isp_group` varchar(256) DEFAULT NULL,
  `preference` int(4) NOT NULL DEFAULT '0',
  `original_preference` int(4) NOT NULL DEFAULT '0',
  `shortlisted_at` datetime DEFAULT NULL,
  `college_id` int(11) NOT NULL,
  `college_location_id` int(11) NOT NULL,
  `degree_id` int(11) NOT NULL,
  `stream_id` int(11) DEFAULT NULL,
  `graduation_start_year` int(11) DEFAULT NULL,
  `graduation_end_year` int(11) DEFAULT NULL,
  `year_of_college` enum('1st','2nd','3rd','4th','5th','not in college') DEFAULT NULL,
  `to_register` tinyint(2) NOT NULL DEFAULT '0',
  `status` enum('open','shortlisted','hired','rejected','inactive','round1_shortlisted','expired','declined') NOT NULL,
  `action_taken_at` timestamp NULL DEFAULT NULL,
  `is_rejected_mail_sent` tinyint(2) NOT NULL DEFAULT '0',
  `is_shortlisted_mail_sent` tinyint(2) NOT NULL,
  `is_shortlisted_sms_sent` tinyint(2) NOT NULL,
  `referral_isp_application_id` int(11) DEFAULT NULL,
  `whatsapp_group_url` varchar(512) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `is_inserted_in_trainings_isp` int(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_applications1` (
  `id` int(11) NOT NULL,
  `isp_version_id` int(11) NOT NULL,
  `email` varchar(512) NOT NULL,
  `first_name` varchar(512) NOT NULL,
  `last_name` varchar(512) DEFAULT NULL,
  `country_code` varchar(8) DEFAULT '+91',
  `mobile` varchar(16) NOT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `isp_group` varchar(256) DEFAULT NULL,
  `preference` int(4) NOT NULL DEFAULT '0',
  `original_preference` int(4) NOT NULL DEFAULT '0',
  `shortlisted_at` datetime DEFAULT NULL,
  `college_id` int(11) NOT NULL,
  `college_location_id` int(11) NOT NULL,
  `degree_id` int(11) NOT NULL,
  `stream_id` int(11) DEFAULT NULL,
  `graduation_start_year` int(11) DEFAULT NULL,
  `graduation_end_year` int(11) DEFAULT NULL,
  `year_of_college` enum('1st','2nd','3rd','4th','5th','not in college') DEFAULT NULL,
  `to_register` tinyint(2) NOT NULL DEFAULT '0',
  `status` enum('open','shortlisted','hired','rejected','inactive','round1_shortlisted','expired','declined') NOT NULL,
  `action_taken_at` timestamp NULL DEFAULT NULL,
  `is_rejected_mail_sent` tinyint(2) NOT NULL DEFAULT '0',
  `is_shortlisted_mail_sent` tinyint(2) NOT NULL,
  `is_shortlisted_sms_sent` tinyint(2) NOT NULL,
  `referral_isp_application_id` int(11) DEFAULT NULL,
  `whatsapp_group_url` varchar(512) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `is_inserted_in_trainings_isp` int(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_application_question_answers` (
  `id` int(11) NOT NULL,
  `isp_application_id` int(11) NOT NULL,
  `isp_question_id` int(11) NOT NULL,
  `answer` varchar(2048) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_completion_certificates` (
  `id` int(11) NOT NULL,
  `isp_version_id` int(11) DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `type` varchar(512) DEFAULT NULL,
  `email` varchar(512) DEFAULT NULL,
  `first_name` varchar(512) DEFAULT NULL,
  `full_name` varchar(512) DEFAULT NULL,
  `college_name` varchar(512) DEFAULT NULL,
  `is_mail_sent` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 07:14:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `isp_emails` (
  `email` varchar(39) DEFAULT NULL,
  `new_email` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `isp_faqs` (
  `id` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `isp_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(12) DEFAULT NULL,
  `link` varchar(49) DEFAULT NULL,
  `count` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `isp_hiring_general` (
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `isp_hiring_tier2` (
  `email` varchar(47) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `isp_joining_letters` (
  `id` int(11) NOT NULL,
  `isp_version_id` int(11) NOT NULL,
  `isp_application_id` int(11) DEFAULT NULL,
  `unique_key` varchar(36) NOT NULL,
  `email` varchar(512) DEFAULT NULL,
  `first_name` varchar(512) DEFAULT NULL,
  `last_name` varchar(512) DEFAULT NULL,
  `full_name` varchar(512) DEFAULT NULL,
  `college_name` varchar(512) DEFAULT NULL,
  `tier` int(3) DEFAULT NULL,
  `is_mail_sent` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `isp_lists` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `status` enum('active','inactive','draft') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_lists_members` (
  `id` int(11) NOT NULL,
  `isp_list_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_questions` (
  `id` int(11) NOT NULL,
  `isp_version_id` int(11) NOT NULL,
  `round` int(4) NOT NULL DEFAULT '1',
  `question` varchar(1024) NOT NULL,
  `answer_min_length` int(11) NOT NULL DEFAULT '100',
  `answer_max_length` int(11) NOT NULL DEFAULT '2048',
  `placeholder` varchar(256) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_registration_stats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isp_id` int(11) NOT NULL,
  `registered_on` date NOT NULL,
  `verified_on` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_registration_stats_temp` (
  `user_id` int(11) NOT NULL,
  `verified_at` timestamp NOT NULL,
  `verified_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_result_files` (
  `id` int(11) NOT NULL,
  `isp_result_page_id` int(11) NOT NULL,
  `result_file` varchar(512) DEFAULT NULL,
  `result_file_status` enum('yet to process','processing','processed') NOT NULL DEFAULT 'yet to process',
  `entries_processed` int(11) DEFAULT NULL,
  `errors` varchar(10000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_result_pages` (
  `id` int(11) NOT NULL,
  `isp_version_id` int(11) NOT NULL,
  `offer_letter_page` varchar(20000) NOT NULL,
  `offer_acceptance_page` varchar(20000) NOT NULL,
  `voice_message_id` varchar(32) DEFAULT NULL,
  `offer_sms_content` varchar(256) NOT NULL,
  `reminder_sms_content` varchar(256) NOT NULL,
  `accepted_sms_content` varchar(256) NOT NULL,
  `offer_letter_mail_template_id` int(11) DEFAULT NULL,
  `reminder_mail_template_id` int(11) DEFAULT NULL,
  `offer_acceptance_mail_template_id` int(11) DEFAULT NULL,
  `certificate` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_share_page_template` (
  `id` int(11) NOT NULL,
  `type` enum('main_heading','sub_heading','whatsapp_message','facebook_message','mail_subject','mail_message') NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `isp_versions` (
  `id` int(11) NOT NULL,
  `isp_version` varchar(512) NOT NULL,
  `start_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `end_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `round2_end_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `result_start_date_time` timestamp NULL DEFAULT NULL,
  `result_end_date_time` timestamp NULL DEFAULT NULL,
  `result_status` enum('yet to schedule','active','sent') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2017-08-01 12:44:35',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `employment_type` enum('job','internship') NOT NULL DEFAULT 'internship',
  `company_id` int(11) NOT NULL,
  `posted_by` int(11) NOT NULL,
  `clone_job_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `part_time_sort_order` int(11) DEFAULT '0',
  `type` enum('regular','virtual','campus_ambassador') DEFAULT NULL,
  `title` varchar(512) NOT NULL,
  `url` varchar(256) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `introduction` varchar(5000) DEFAULT NULL,
  `conclusion` varchar(5000) DEFAULT NULL,
  `status` enum('draft','pending review','active','rejected','closed','inactive','follow up','deleted','to_process_active') NOT NULL,
  `application_count` int(11) NOT NULL DEFAULT '0',
  `job_authentication_status` enum('pending','complete') DEFAULT NULL,
  `assigned_admin_id` int(11) DEFAULT NULL,
  `assigned_at` timestamp NULL DEFAULT '2000-01-01 00:00:00',
  `processed_by_admin_id` int(11) DEFAULT NULL,
  `posted_at` timestamp NULL DEFAULT NULL,
  `start_date1` date DEFAULT NULL,
  `start_date2` date DEFAULT NULL,
  `is_applications_from_other_cities_restricted` tinyint(4) NOT NULL DEFAULT '0',
  `follow_up_date` timestamp NULL DEFAULT NULL,
  `rejected_on` timestamp NULL DEFAULT NULL,
  `is_ask_a_recruiter_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_followup_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_followup_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `approved_at` timestamp NULL DEFAULT NULL,
  `closed_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `access_applications_till` timestamp NULL DEFAULT NULL,
  `is_access_expiring_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `closure_status` enum('inactive','active','processed','') NOT NULL,
  `processed_outcome` enum('hired','not hired','zero applicant','all rejected') DEFAULT NULL,
  `closure_activated_on` timestamp NULL DEFAULT NULL,
  `is_not_finished_processing_shown` tinyint(1) NOT NULL DEFAULT '0',
  `is_closure_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `is_work_from_home` tinyint(1) NOT NULL DEFAULT '0',
  `open_positions` int(3) DEFAULT NULL,
  `is_part_time` tinyint(1) NOT NULL DEFAULT '0',
  `duration_type` enum('fixed','flexible') NOT NULL DEFAULT 'fixed',
  `duration1` int(11) DEFAULT NULL,
  `duration2` int(11) DEFAULT NULL,
  `duration_scale` enum('month','week') DEFAULT NULL,
  `is_expired_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_extended` tinyint(1) NOT NULL DEFAULT '0',
  `is_filter_ever_used` tinyint(1) DEFAULT NULL,
  `is_eligible_for_unprocessed_applications_increase_exception` tinyint(1) NOT NULL DEFAULT '0',
  `is_unprocessed_applications_increased` tinyint(1) NOT NULL DEFAULT '0',
  `is_salary_negotiable` tinyint(1) DEFAULT NULL,
  `job_prospect` tinyint(1) DEFAULT '1',
  `ppo_salary` int(11) DEFAULT NULL,
  `ppo_salary2` int(11) DEFAULT NULL,
  `is_ppo_salary_disclosed` tinyint(4) NOT NULL DEFAULT '0',
  `additional_details` varchar(5000) DEFAULT NULL,
  `who_can_apply` varchar(5000) DEFAULT NULL,
  `is_external` tinyint(1) NOT NULL DEFAULT '0',
  `external_link` varchar(256) DEFAULT NULL,
  `is_international` tinyint(1) NOT NULL DEFAULT '0',
  `campaign_id` int(11) DEFAULT NULL,
  `to_show_campaign_jobs_in_search` tinyint(1) NOT NULL DEFAULT '0',
  `is_feedback_received` tinyint(1) NOT NULL DEFAULT '0',
  `is_three_day_zero_application_mail_sent` tinyint(4) NOT NULL DEFAULT '0',
  `candidate_preference` varchar(256) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_categories` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_degree_groups` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `degree_group_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_highlights` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `highlight_id` int(11) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 11:11:11',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_locations` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_metas` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `type` enum('category','location','keyword','location_cluster') NOT NULL,
  `value` varchar(10000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_not_hired_reasons` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_perks` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `perk` enum('certification','lor','flexible_day_hours','informal_dress_code','five_days','free_snacks','transportation','health_insurance','life_insurance') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_profiles` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `is_primary_profile` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_segments` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `segment` enum('summer','summer fair','iwi2','osif_mumbai','internship_for_women','iwi3','first_year_campaign','ibsi','online_campus_drive','first_year_campaign2','ifw_career_relaunch','ibsi_students_side','freedom_hair_internships','easy_todo_internships') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_share_stats` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `sharer` varchar(256) NOT NULL,
  `utm_source` varchar(2048) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_skills` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_status_histories` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `old_status` enum('draft','pending review','active','rejected','closed','inactive','follow up','to_process_active') DEFAULT NULL,
  `new_status` enum('draft','pending review','active','rejected','closed','inactive','follow up','to_process_active') DEFAULT NULL,
  `processed_by_admin_id` int(11) NOT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_streams` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_titles` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `title_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_to_evaluate` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_transactions` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `status` enum('initiated','expired','failed','success') DEFAULT NULL,
  `amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `message` varchar(1024) DEFAULT NULL,
  `payment_gateway` enum('ccavenue') NOT NULL DEFAULT 'ccavenue',
  `transaction_id` varchar(128) NOT NULL,
  `payment_gateway_id` varchar(128) DEFAULT NULL,
  `payment_error_code_id` int(11) DEFAULT NULL,
  `bank_reference_number` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `jobs_view_application_click_stats` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_access_application_till_referers` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `referer` enum('chat','ic_module','dashboard') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-07-04 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_auto_save` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `unique_key` varchar(256) NOT NULL,
  `data` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_campus_ambassador_info` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `type` enum('company','college','student') DEFAULT NULL,
  `learning_and_mentoring` varchar(2048) NOT NULL,
  `performance_tracking` varchar(2048) NOT NULL,
  `selection_procedure` varchar(2048) NOT NULL,
  `stipend_details` varchar(2048) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_detail_additional_infos` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `type` enum('banner') DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `info` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_extended` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `last_job_transaction_id` int(11) DEFAULT NULL,
  `payment_reminder_day_one` int(11) NOT NULL DEFAULT '0',
  `payment_reminder_day_three` int(11) NOT NULL DEFAULT '0',
  `is_bulk_hiring_mail_sent` tinyint(4) NOT NULL DEFAULT '0',
  `recommendation_computed_on` date DEFAULT NULL,
  `recommendation_computed_status` enum('success','cron_failed','api_failed','profile_not_found','processing') DEFAULT NULL,
  `is_3_day_invite_reminder_sent` enum('sent','not_sent','control_group') DEFAULT NULL,
  `allow_work_from_home_for_covid` int(1) DEFAULT '0',
  `draft_reminder` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 01:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_follow_up_mails` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `mail_content` varchar(15000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_follow_up_types` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `follow_up_type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_histories` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `clone_job_id` int(11) DEFAULT NULL,
  `description` varchar(5000) NOT NULL,
  `salary` int(8) DEFAULT NULL,
  `salary2` int(11) DEFAULT NULL,
  `salary_scale` varchar(64) DEFAULT NULL,
  `salary_performance` int(11) DEFAULT NULL,
  `salary_performance_scale_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_original_admin` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_rejection_mails` (
  `id` bigint(20) NOT NULL,
  `job_id` int(11) NOT NULL,
  `mail_content` varchar(15000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_rejection_types` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `rejection_type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_salary` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `salary` decimal(11,2) DEFAULT NULL,
  `salary2` decimal(11,2) DEFAULT NULL,
  `breakup_type` enum('lpa','percentage') DEFAULT NULL,
  `fixed_pay` decimal(11,2) DEFAULT NULL,
  `variable_pay` decimal(11,2) DEFAULT NULL,
  `other` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `job_year_of_completions` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `year_of_completion` int(4) NOT NULL,
  `operator` enum('>=','<=','>','<','=') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_boards` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_board_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_categories` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_colleges` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_college_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_degrees` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_degree_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_email_domains` (
  `id` int(11) NOT NULL,
  `junk_domain_name` varchar(256) NOT NULL,
  `approved_domain_name` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2015-12-31 12:19:25',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_profiles` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_profile_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_schools` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_school_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_skills` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_skill_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_streams` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_stream_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `junk_trainings` (
  `id` int(11) NOT NULL,
  `junk_name` varchar(256) NOT NULL,
  `approved_training_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `karix_sms_responses` (
  `id` int(11) NOT NULL,
  `response` varchar(20000) NOT NULL,
  `karix_request_id` varchar(256) NOT NULL,
  `dest` varchar(16) NOT NULL,
  `stime` timestamp NOT NULL DEFAULT '2018-09-27 00:00:00',
  `dtime` timestamp NOT NULL DEFAULT '2018-09-27 00:00:00',
  `status` varchar(128) NOT NULL,
  `reason` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-09-27 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `karix_sms_stats` (
  `id` int(11) NOT NULL,
  `mobile_number` varchar(16) NOT NULL,
  `message` varchar(512) NOT NULL,
  `source` enum('internshala','trainings') NOT NULL DEFAULT 'internshala',
  `type` enum('promotional','transactional') NOT NULL,
  `karix_request_id` varchar(128) DEFAULT NULL,
  `time_of_acceptance` timestamp NULL DEFAULT NULL,
  `sms_response_code_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `linkedin_industry_codes` (
  `id` int(11) NOT NULL,
  `code` int(11) DEFAULT NULL,
  `group` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2015-05-11 13:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `linkedin_job_functions` (
  `id` int(11) NOT NULL,
  `code` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2015-05-11 13:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `localities` (
  `id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `place_id` varchar(512) DEFAULT NULL,
  `name` varchar(512) DEFAULT NULL,
  `to_show_in_preferences` tinyint(4) NOT NULL DEFAULT '0',
  `sublocality_level_2_id` int(11) DEFAULT NULL,
  `sublocality_level_1_id` int(11) DEFAULT NULL,
  `locality_id` int(11) DEFAULT NULL,
  `administrative_area_level_2_id` int(11) DEFAULT NULL,
  `administrative_area_level_1_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `use_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `location_maps` (
  `id` int(11) NOT NULL,
  `google_name` varchar(256) DEFAULT NULL,
  `approved_name` varchar(256) DEFAULT NULL,
  `location_type` enum('locality','administrative_area_level_2','both') NOT NULL DEFAULT 'locality',
  `is_cleaned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_emails` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `is_allowed_in_from` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_history` (
  `id` int(11) NOT NULL,
  `mail_to` varchar(5000) DEFAULT NULL,
  `cc` varchar(5000) DEFAULT NULL,
  `bcc` varchar(5000) DEFAULT NULL,
  `subject` varchar(1024) NOT NULL,
  `message` text CHARACTER SET utf8mb4 NOT NULL,
  `type` varchar(256) NOT NULL,
  `alt_message` varchar(1024) DEFAULT NULL,
  `sent` tinyint(4) NOT NULL,
  `no_of_tries` int(11) NOT NULL,
  `failed_info` text,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_queue` (
  `id` int(11) NOT NULL,
  `mail_to` varchar(5000) DEFAULT NULL,
  `cc` varchar(5000) DEFAULT NULL,
  `bcc` varchar(5000) DEFAULT NULL,
  `subject` varchar(1024) NOT NULL,
  `message` mediumtext NOT NULL,
  `type` varchar(256) NOT NULL,
  `alt_message` varchar(1024) DEFAULT NULL,
  `sent` tinyint(4) NOT NULL,
  `no_of_tries` int(11) NOT NULL,
  `failed_info` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_queue_general` (
  `id` int(11) NOT NULL,
  `mail_to` varchar(5000) DEFAULT NULL,
  `cc` varchar(5000) DEFAULT NULL,
  `bcc` varchar(5000) DEFAULT NULL,
  `from_email` varchar(5000) DEFAULT NULL,
  `from_name` varchar(5000) DEFAULT NULL,
  `reply_to` varchar(5000) DEFAULT NULL,
  `reply_to_name` varchar(5000) DEFAULT NULL,
  `subject` varchar(1024) CHARACTER SET utf8mb4 NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 NOT NULL,
  `attachment_path` varchar(1024) DEFAULT NULL,
  `type` varchar(256) NOT NULL,
  `alt_message` varchar(1024) DEFAULT NULL,
  `sent` tinyint(4) NOT NULL,
  `no_of_tries` int(11) NOT NULL,
  `failed_info` mediumtext,
  `sms_to` varchar(20) DEFAULT NULL,
  `sms_message` varchar(1024) DEFAULT NULL,
  `sms_sent` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_senders_receivers` (
  `id` int(11) NOT NULL,
  `type` enum('from','reply_to','cc','bcc') NOT NULL,
  `mail_type_id` int(11) NOT NULL,
  `mail_email_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_signatures` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `signature` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `template_type` enum('html','php') NOT NULL,
  `mail_type_id` int(11) NOT NULL,
  `subject` varchar(1024) CHARACTER SET utf8mb4 NOT NULL,
  `template` varchar(10240) CHARACTER SET utf8mb4 NOT NULL,
  `mail_signature_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mail_types` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `mca_crawl_data` (
  `id` int(11) NOT NULL,
  `cin` varchar(256) DEFAULT NULL,
  `name_from_sheet` varchar(256) DEFAULT NULL,
  `date_of_registration` varchar(36) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `processed_at` timestamp NULL DEFAULT NULL,
  `status` enum('subscribed','unsubscribed','bounced','registered','duplicate_email','no_email') NOT NULL DEFAULT 'subscribed',
  `is_first_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `first_mail_sent_at` timestamp NULL DEFAULT NULL,
  `is_follow_up_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `follow_up_mail_sent_at` timestamp NULL DEFAULT NULL,
  `is_periodic_mail_sent` tinyint(1) NOT NULL DEFAULT '1',
  `periodic_mail_sent_at` timestamp NULL DEFAULT NULL,
  `is_periodic_follow_up_mail_sent` tinyint(1) NOT NULL DEFAULT '1',
  `periodic_follow_up_mail_sent_at` timestamp NULL DEFAULT NULL,
  `delay` varchar(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `url` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `noc` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `tnp_id` int(11) NOT NULL,
  `tnp_first_name` varchar(256) NOT NULL,
  `tnp_last_name` varchar(256) NOT NULL,
  `country_code` varchar(5) NOT NULL DEFAULT '+91',
  `tnp_phone_number` varchar(20) NOT NULL,
  `status` enum('under_process','confirmed','rejected','dont_know','withdrawn','exception','expired','dnd') NOT NULL DEFAULT 'under_process',
  `unique_key` varchar(36) NOT NULL,
  `can_add_new_tnp` enum('0','1') NOT NULL DEFAULT '0',
  `is_mail_sent` enum('0','1') NOT NULL DEFAULT '0',
  `tnp_verification_sent_email_date` date DEFAULT NULL,
  `is_sent_tnp_verification_reminder_to_student` enum('0','1') NOT NULL DEFAULT '0',
  `is_sent_tnp_verification_reminder_to_tnp` enum('0','1') NOT NULL DEFAULT '0',
  `is_sent_final_verification_reminder_to_student` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `type` enum('student','employer','') NOT NULL,
  `employment_type` enum('job','internship') DEFAULT NULL,
  `short_message_1` varchar(625) NOT NULL,
  `message_1` varchar(2000) NOT NULL,
  `short_message_2` varchar(625) NOT NULL,
  `message_2` varchar(2000) NOT NULL,
  `display_location` enum('employer_dashboard_internships','employer_dashboard_applications','student_dashboard','employer_dashboard_shortlisted_tab','internship_form','student_dashboard_modal') DEFAULT NULL,
  `to_show_in_dashboard` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `not_hired_reasons` (
  `id` int(11) NOT NULL,
  `reason` varchar(1024) NOT NULL,
  `to_show_in_internship_closure_form` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `out_of_office_dates` (
  `id` int(11) NOT NULL,
  `out_of_office_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `pagename` varchar(50) NOT NULL,
  `is_active` int(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `payment_error_codes` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `short_message` varchar(512) NOT NULL,
  `long_message` varchar(512) NOT NULL,
  `description` varchar(2048) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `personal_email_domains` (
  `id` int(11) NOT NULL,
  `domain_name` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `premium_colleges` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `google_college_location_id` int(11) DEFAULT NULL,
  `google_college_id` int(11) DEFAULT NULL,
  `is_mapped` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `professionalism_contests` (
  `id` int(11) NOT NULL,
  `application_reporting_reason_id` int(11) NOT NULL,
  `description` varchar(2048) NOT NULL,
  `attachments` varchar(2048) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `professionalism_report_reasons` (
  `id` int(11) NOT NULL,
  `reason` varchar(512) NOT NULL,
  `tooltip` varchar(128) NOT NULL,
  `employment_type` enum('job','internship') NOT NULL DEFAULT 'internship',
  `sort_preference` int(11) NOT NULL,
  `deductions` decimal(5,4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `is_niche` int(2) NOT NULL DEFAULT '0',
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `profiles_categories` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 05:05:05',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `profile_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `profile_groups_profiles` (
  `id` int(11) NOT NULL,
  `profile_group_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_expected_behaviour_faqs` (
  `id` int(11) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `content` varchar(2048) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_levels` (
  `id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_questions` (
  `id` int(11) NOT NULL,
  `rating_level_id` int(11) NOT NULL,
  `rating_question_set_id` int(11) NOT NULL,
  `question` varchar(1024) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `type` enum('single_select','multi_select') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_questions_helpers` (
  `id` int(11) NOT NULL,
  `rating_question_id` int(11) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `description` varchar(2048) NOT NULL,
  `type` enum('image','comprehension','explanation') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_question_options` (
  `id` int(11) NOT NULL,
  `rating_question_id` int(11) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_question_sets` (
  `id` int(11) NOT NULL,
  `question_set` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_quiz_student_scores` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `rating_level_id` int(11) NOT NULL,
  `rating_question_set_id` int(11) NOT NULL,
  `score` decimal(5,4) NOT NULL,
  `is_level_cleared` tinyint(1) NOT NULL DEFAULT '0',
  `internshala_session_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_quiz_student_sources` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `rating_level_id` int(11) NOT NULL,
  `source` enum('rating_homepage','retry_level','next_level') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rating_student_answers` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `rating_question_option_id` int(11) NOT NULL,
  `rating_quiz_student_score_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `real_score` int(11) NOT NULL DEFAULT '0',
  `old_real_score` decimal(11,10) DEFAULT '0.0000000000',
  `status` enum('skipped','invited','already_applied','expired') DEFAULT NULL,
  `computed_on` date DEFAULT '2000-01-01',
  `last_status_changed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_filtered_out` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations_advanced_search` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `search_by` varchar(20000) NOT NULL,
  `operator` enum('>=','<=','>','<','=','<>','in','like') NOT NULL,
  `value` varchar(5000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations_decline_reasons` (
  `id` int(11) NOT NULL,
  `recommendation_id` int(11) NOT NULL,
  `decline_reason_id` int(11) NOT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations_intermediate` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `recency_score` int(11) NOT NULL DEFAULT '0',
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations_intermediate_job` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `score` varchar(20) NOT NULL,
  `recency_score` int(11) NOT NULL DEFAULT '0',
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations_job` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `real_score` int(11) NOT NULL DEFAULT '0',
  `old_real_score` decimal(11,10) DEFAULT '0.0000000000',
  `status` enum('skipped','invited','already_applied','expired') DEFAULT NULL,
  `last_status_changed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `shortlisted_count` int(11) DEFAULT '0',
  `hired_count` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendations_view_source` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `source` enum('dashboard','email','auto_approve_modal','bucket_change','3_day_reminder_email','pending_decision_card') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `recommendation_dumps` (
  `id` bigint(20) NOT NULL,
  `job_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `recency_score` int(11) NOT NULL DEFAULT '0',
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rejection_types` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `remember_me_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `persistant_key` varchar(36) NOT NULL,
  `token` varchar(36) NOT NULL,
  `remote_address` varchar(39) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `resume_detail_sources` (
  `id` int(11) NOT NULL,
  `detail` enum('students_colleges','students_schools','students_skills','student_experiences','student_position_of_responsibilities','student_trainings','student_projects','student_additional_details','student_work_samples') NOT NULL,
  `detail_id` int(11) NOT NULL,
  `source` enum('profile','resume_after_profile','resume_direct','resume_feedback','resume_intermediate','resume_eligibility','resume_unknown','resume_application_view','resume_search') NOT NULL DEFAULT 'resume_unknown',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `resume_quiz_questions_answers` (
  `id` int(11) NOT NULL,
  `question` varchar(5000) NOT NULL,
  `answer` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `resume_quiz_user_details` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `isp_code` varchar(50) NOT NULL,
  `to_register` tinyint(4) NOT NULL DEFAULT '0',
  `score` int(11) NOT NULL DEFAULT '-1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `ribbon_messages` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `message` varchar(1024) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `status` enum('active','closed') DEFAULT NULL,
  `location` enum('all','home_page','search','details','student_dashboard','employer_dashboard','tnp_dashboard') DEFAULT NULL,
  `visible_to` enum('all','student','employer','tnp','admin','guest') NOT NULL DEFAULT 'all',
  `priority` int(4) NOT NULL DEFAULT '100',
  `type` enum('generic','college_specific','college_specific_generic') NOT NULL DEFAULT 'generic',
  `background_color` varchar(20) DEFAULT NULL,
  `ribbon_placement_type` enum('fixed','scrollable') DEFAULT NULL,
  `timer_type` enum('current_date','end_date') DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `rsa_keys` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `public_key` varchar(2048) DEFAULT NULL,
  `private_key` varchar(2048) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-02-19 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `salary_performance_scales` (
  `id` int(11) NOT NULL,
  `scale` varchar(256) NOT NULL,
  `to_show_in_dropdown` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `schools` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `server_time` (
  `id` int(11) NOT NULL,
  `datetime` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `shortened_urls` (
  `id` int(11) NOT NULL,
  `shortened_url` varchar(32) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `shortened_url_csv` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `link` varchar(256) NOT NULL,
  `is_custom_url` tinyint(1) NOT NULL,
  `status` enum('yet to start','processing','processed') NOT NULL DEFAULT 'yet to start',
  `error` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `shortened_url_csv_shortened_urls` (
  `id` int(11) NOT NULL,
  `shortened_url_csv_id` int(11) NOT NULL,
  `shortened_url_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `skills_trainings_mapping` (
  `id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `training_text` varchar(200) NOT NULL,
  `training_url` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `slider_banners` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `banner_template_id` int(11) NOT NULL,
  `css` varchar(10240) DEFAULT NULL,
  `location` enum('home','ifw') NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '1000',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_time_based` tinyint(1) NOT NULL,
  `to_show_timer` tinyint(1) DEFAULT '0',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `slots` (
  `id` int(50) NOT NULL,
  `page_id` int(50) NOT NULL,
  `slot_num` int(50) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `is_active` int(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_campaigns` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `message_type` enum('promotional','transactional') NOT NULL,
  `sms_list_id` int(11) NOT NULL,
  `status` enum('yet_to_schedule','yet_to_start','sending','sent') NOT NULL,
  `schedule_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_events` (
  `id` int(11) NOT NULL,
  `sms_lists_contact_id` int(11) NOT NULL,
  `sms_campaign_id` int(11) NOT NULL,
  `is_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_lists` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_lists_contacts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `number` varchar(12) NOT NULL,
  `sms_list_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_lists_contacts_csv` (
  `id` int(11) NOT NULL,
  `sms_list_id` int(11) NOT NULL,
  `csv_path` varchar(512) NOT NULL,
  `status` enum('to_be_processed','processing','processed') NOT NULL DEFAULT 'to_be_processed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_queue_general` (
  `id` int(11) NOT NULL,
  `mail_queue_general_id` int(11) DEFAULT NULL,
  `source` enum('internshala','trainings') NOT NULL DEFAULT 'internshala',
  `sms_to` varchar(512) NOT NULL,
  `message` varchar(512) NOT NULL,
  `is_sent` tinyint(4) NOT NULL DEFAULT '0',
  `type` varchar(512) DEFAULT NULL,
  `sms_type` enum('transactional','promotional') NOT NULL DEFAULT 'transactional',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_referral_logs` (
  `id` int(11) NOT NULL,
  `referral_user_id` int(11) NOT NULL,
  `referred_to_number` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_response_codes` (
  `id` int(11) NOT NULL,
  `code` varchar(16) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-07-20 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sms_stats` (
  `id` int(11) NOT NULL,
  `mobile_number` varchar(16) DEFAULT NULL,
  `message` varchar(512) DEFAULT NULL,
  `type` enum('promotional','transactional') DEFAULT NULL,
  `response_code` varchar(12) DEFAULT NULL,
  `response_value` varchar(512) DEFAULT NULL,
  `response` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `stats_count` (
  `id` int(11) NOT NULL,
  `entity` varchar(20) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `no_need_of_subscription` int(11) NOT NULL DEFAULT '0',
  `already_subscribed` int(11) NOT NULL DEFAULT '0',
  `subscribe` int(11) NOT NULL DEFAULT '0',
  `first_time_user_cookie` int(11) NOT NULL DEFAULT '0',
  `user_cookie` int(11) NOT NULL DEFAULT '0',
  `date` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `stipend_prediction_internships` (
  `id` int(11) NOT NULL,
  `internship_id` int(11) NOT NULL,
  `profile_name` varchar(256) NOT NULL,
  `location` varchar(256) NOT NULL,
  `stipend` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `stipend_prediction_recommendations` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `predicted_applications` decimal(10,2) NOT NULL,
  `stipend_recommendation_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `duration_recommendation_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `predicted_change_in_applications_by_stipend` decimal(5,2) NOT NULL,
  `predicted_change_in_applications_by_duration` decimal(5,2) NOT NULL,
  `predicted_change_in_applications_by_stipend_for_updated_duration` decimal(5,2) NOT NULL,
  `predicted_change_in_applications_by_duration_for_updated_stipend` decimal(5,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `streams` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `stream_degree_discounts` (
  `id` int(11) NOT NULL,
  `degree_id` int(11) NOT NULL,
  `degree_short_name` varchar(255) NOT NULL,
  `stream_id` int(11) DEFAULT NULL,
  `stream_short_name` varchar(255) DEFAULT NULL,
  `discount_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 01:01:01',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `salutation` enum('mr','mrs','ms','dr') DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `current_city` int(11) DEFAULT NULL,
  `second_city_of_residence` int(11) DEFAULT NULL,
  `phone_primary` varchar(12) DEFAULT NULL,
  `consecutive_expired_count` int(11) DEFAULT '0',
  `otp` mediumint(9) DEFAULT NULL,
  `is_phone_verified` tinyint(4) NOT NULL DEFAULT '0',
  `updated_phone_on` date DEFAULT '1970-01-01',
  `otp_count` smallint(6) NOT NULL DEFAULT '0',
  `country_code` varchar(5) DEFAULT '+91',
  `email_college` varchar(256) DEFAULT NULL,
  `matching_preference` tinyint(1) NOT NULL DEFAULT '1',
  `is_first_rejected_email_sent` tinyint(4) NOT NULL DEFAULT '0',
  `is_resume_displayed` tinyint(1) NOT NULL DEFAULT '1',
  `application_success_message_shown_on` date DEFAULT NULL,
  `webpush_notification_denied_on` date DEFAULT NULL,
  `reported_abuse_count` int(10) NOT NULL DEFAULT '0',
  `student_digest_sent_on` date DEFAULT '1970-01-01',
  `to_show_resume_score` enum('0','1','2') DEFAULT '0',
  `is_cr_benefits_mail_sent` tinyint(1) DEFAULT '0',
  `referral` varchar(256) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `students_activation_tracker` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `action` enum('preferences','personal_details','education_details','work_experiences','other_experiences','skills','work_samples','additional_details','resume','application_form','not_eligible','first_application_same_internship','first_application_different_internship','first_application_direct_registration','internships') DEFAULT NULL,
  `source` enum('app','web') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `students_certificates` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `unique_key` varchar(36) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `college_name` varchar(256) NOT NULL,
  `is_certificate_download` enum('0','1') NOT NULL DEFAULT '0',
  `is_image_generated` tinyint(1) NOT NULL DEFAULT '0',
  `image_path` varchar(1024) DEFAULT NULL,
  `is_image_scraped` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `students_colleges` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `degree_type` enum('ug','pg','diploma','phd') NOT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `old_degree_id` int(11) NOT NULL DEFAULT '0',
  `stream_id` int(11) NOT NULL,
  `old_stream_id` int(11) NOT NULL DEFAULT '0',
  `start_year` int(4) NOT NULL,
  `end_year` int(4) NOT NULL,
  `is_pursuing` tinyint(4) NOT NULL DEFAULT '0',
  `performance_scale` enum('4','10','100','5','7','8','9') NOT NULL,
  `performance` decimal(5,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `students_notifications` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `students_schools` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `standard_type` enum('secondary','senior_secondary') NOT NULL,
  `board_id` int(11) NOT NULL,
  `stream` enum('science','medical','arts','commerce') DEFAULT NULL,
  `performance_scale` enum('4','10','100','5','7') NOT NULL,
  `performance` decimal(5,2) DEFAULT NULL,
  `year_of_completion` int(4) NOT NULL,
  `is_pursuing` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `students_skills` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `level` enum('None','Beginner','Intermediate','Advanced') DEFAULT 'None',
  `to_show_in_resume` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_additional_details` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `student_badges` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `internshala_training_id` int(11) NOT NULL,
  `student_score` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_campaigns` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `application_flow` enum('default') NOT NULL DEFAULT 'default',
  `verification_flow` enum('default') NOT NULL DEFAULT 'default',
  `registration_flow` enum('default') NOT NULL DEFAULT 'default',
  `login_flow` enum('default') NOT NULL DEFAULT 'default',
  `internships_layout` enum('default') NOT NULL DEFAULT 'default',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 
 


CREATE TABLE `student_digest_notifications` (
  `id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `gcm_user_id` int(11) DEFAULT NULL,
  `preference_type` enum('internship','job','tentative_job') DEFAULT NULL,
  `is_eligible` tinyint(1) DEFAULT NULL,
  `matching_internships_count` int(11) DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `delivered_at` timestamp NULL DEFAULT NULL,
  `clicked_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_experiences` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `experience_type` enum('internship','job') NOT NULL DEFAULT 'internship',
  `experience_organization_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `is_work_from_home` tinyint(3) NOT NULL DEFAULT '0',
  `description` varchar(5000) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_on_going` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_extended` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `last_searched_at` timestamp NULL DEFAULT NULL,
  `to_exclude_for_recommendation` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 01:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_invitation_counts` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `open_invites` int(11) NOT NULL DEFAULT '0',
  `open_invites_job` int(11) NOT NULL DEFAULT '0',
  `consecutive_expired_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_metas` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `type` varchar(1024) NOT NULL,
  `value` varchar(20000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_position_of_responsibilities` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_projects` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `title` varchar(512) NOT NULL,
  `start_month` date NOT NULL,
  `end_month` date DEFAULT NULL,
  `is_on_going` tinyint(3) DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `project_link` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_registration_custom_messages` (
  `id` int(11) NOT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `message` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_trainings` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `experience_organization_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `is_work_from_home` tinyint(3) NOT NULL DEFAULT '0',
  `description` varchar(5000) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_on_going` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `student_work_samples` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `type` enum('blog','github','portfolio','developer_account','behance') NOT NULL,
  `link` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 


CREATE TABLE `sublocality_levels_1` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `sublocality_levels_2` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `status` enum('unconfirmed','active','unsubscribed','inactive','workshop','workshop_subscription','hard_bounce') NOT NULL,
  `source` enum('unknown','subscription','registration','vtc','google_login','registration_app','google_login_app','workshop','gsif_prelaunch') DEFAULT 'unknown',
  `url` varchar(1024) DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(512) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `subscription_location` enum('subscription_popup','subscription_inbox','no_internship','search_page_bottom','','subscription_popup_old','subscription_popup_new','subscription_popup_blog','gsif_prelaunch') DEFAULT NULL,
  `digest_sent_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_first_time_user` tinyint(1) NOT NULL DEFAULT '0',
  `is_three_day_subscription_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_seven_day_subscription_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_fifteen_day_subscription_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_subscription_blog_mail_sent` tinyint(4) NOT NULL DEFAULT '0',
  `is_eligible_for_ifw` tinyint(1) NOT NULL DEFAULT '0',
  `activation_link_sent_count` int(11) NOT NULL DEFAULT '0',
  `confirmed_at` date DEFAULT '1970-01-01',
  `verified_at` timestamp NULL DEFAULT NULL,
  `last_updated_at_for_sync` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 


CREATE TABLE `subscribers_digest_count` (
  `id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `preferences_count` int(11) NOT NULL DEFAULT '0',
  `application_history_count` int(11) NOT NULL DEFAULT '0',
  `profile_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL,
  `last_updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `subscribers_status_trigger` (
  `id` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `email` varchar(512) DEFAULT NULL,
  `old_status` enum('unconfirmed','active','unsubscribed','inactive','workshop','workshop_subscription') NOT NULL,
  `new_status` enum('unconfirmed','active','unsubscribed','inactive','workshop','workshop_subscription') DEFAULT NULL,
  `is_eligible_for_ifw` tinyint(2) NOT NULL DEFAULT '0',
  `subscribers_created_at` datetime DEFAULT NULL,
  `users_last_seen` datetime DEFAULT NULL,
  `subscribers_last_updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `subscribers_status_trigger_dump` (
  `id` int(11) NOT NULL,
  `email` varchar(512) DEFAULT NULL,
  `is_eligible_for_ifw` tinyint(4) NOT NULL DEFAULT '0',
  `old_status` enum('unconfirmed','active','unsubscribed','inactive','workshop','workshop_subscription') NOT NULL,
  `new_status` enum('unconfirmed','active','unsubscribed','inactive','workshop','workshop_subscription') DEFAULT NULL,
  `users_last_seen_at` datetime DEFAULT NULL,
  `subscribers_created_at` datetime DEFAULT NULL,
  `subscribers_last_updated_at` datetime NOT NULL,
  `em_status` varchar(512) DEFAULT NULL,
  `em_status_update_time` datetime DEFAULT NULL,
  `recently_updated_database` enum('internshala','em','default') DEFAULT NULL,
  `recent_status` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `suppressed_list` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `unique_key` varchar(256) DEFAULT NULL,
  `status` enum('suppressed','draft','deleted') NOT NULL DEFAULT 'draft',
  `message` varchar(1024) DEFAULT NULL,
  `is_mail_sent` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `survey_experiences` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `college_id` int(11) DEFAULT NULL,
  `additional_detail` varchar(1500) NOT NULL,
  `company_name` varchar(256) NOT NULL,
  `year` int(4) DEFAULT NULL,
  `stipend` int(11) DEFAULT NULL,
  `overall_experience` enum('1','2','3','4','5') NOT NULL,
  `work_culture` enum('1','2','3','4','5') NOT NULL,
  `mentorship` enum('1','2','3','4','5') NOT NULL,
  `learning` enum('1','2','3','4','5') NOT NULL,
  `selection_process` int(11) DEFAULT NULL,
  `is_recommended` tinyint(1) NOT NULL,
  `utm_source` varchar(512) DEFAULT NULL,
  `utm_medium` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `team_members_page` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_autocomplete_count` (
  `id` int(11) NOT NULL,
  `offline` int(11) NOT NULL,
  `google` int(11) NOT NULL,
  `offline_details` int(11) NOT NULL DEFAULT '0',
  `google_details` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_birthday_registrations` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `country_code` varchar(5) NOT NULL,
  `phone_primary` varchar(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_birthday_values` (
  `id` int(11) NOT NULL,
  `start_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `daily_contest_link` varchar(1024) NOT NULL,
  `wish_us_link` varchar(1024) NOT NULL,
  `scholarship_link` varchar(1024) NOT NULL,
  `trainings_link` varchar(1024) NOT NULL,
  `join_our_team_link` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_blocked_chat_messages` (
  `id` int(11) NOT NULL,
  `blocked_employer_detail_id_old` int(11) DEFAULT NULL,
  `blocked_employer_detail_id` int(11) DEFAULT NULL,
  `chat_message_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_blocked_employer_details` (
  `id` int(11) NOT NULL,
  `blocked_employer_email_id` int(11) DEFAULT NULL,
  `type` enum('phone','email_domain','website','URL') NOT NULL,
  `value` varchar(10000) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `start_at` int(11) DEFAULT '1',
  `end_at` int(11) DEFAULT '1000',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_blocked_employer_details_new` (
  `id` int(11) NOT NULL,
  `old_id` int(11) DEFAULT NULL,
  `blocked_employer_email_id` int(11) DEFAULT NULL,
  `type` enum('phone','email_domain','website','URL') NOT NULL,
  `value` varchar(10000) DEFAULT NULL,
  `domain` varchar(1024) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `start_at` int(11) DEFAULT '1',
  `end_at` int(11) DEFAULT '1000',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_blocked_employer_email` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_chat_message_response_analysis` (
  `id` int(11) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `grp_message` varchar(5000) NOT NULL,
  `emp_t` timestamp NULL DEFAULT NULL,
  `stu_t` timestamp NULL DEFAULT NULL,
  `time_diff` varchar(50) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_chat_message_response_analysis_2` (
  `id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `cc_aid` int(11) NOT NULL,
  `grp_message` varchar(5000) NOT NULL,
  `emp_t` timestamp NULL DEFAULT NULL,
  `stu_t` timestamp NULL DEFAULT NULL,
  `time_diff` varchar(50) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(50) DEFAULT NULL,
  `probablity_1` varchar(10) DEFAULT NULL,
  `probablity_2` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_college_discount_coupon_codes` (
  `id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `abbr` varchar(256) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_crs` (
  `id` int(11) NOT NULL,
  `student_email` varchar(256) NOT NULL,
  `student_first_name` varchar(256) NOT NULL,
  `student_last_name` varchar(256) DEFAULT NULL,
  `student_phone_primary` varchar(20) DEFAULT '9999999999',
  `tnp_id` int(11) DEFAULT NULL,
  `tnp_first_name` varchar(256) NOT NULL,
  `tnp_last_name` varchar(256) DEFAULT NULL,
  `tnp_email` varchar(256) NOT NULL,
  `tnp_country_code` varchar(8) DEFAULT '+91',
  `tnp_phone_primary` varchar(16) NOT NULL,
  `college_name` varchar(256) NOT NULL,
  `is_tnp_processed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_crs_2` (
  `id` int(11) NOT NULL,
  `student_email` varchar(256) NOT NULL,
  `student_first_name` varchar(256) NOT NULL,
  `student_last_name` varchar(256) DEFAULT NULL,
  `student_phone_primary` varchar(20) DEFAULT '9999999999',
  `tnp_first_name` varchar(256) NOT NULL,
  `tnp_last_name` varchar(256) DEFAULT NULL,
  `tnp_email` varchar(256) NOT NULL,
  `tnp_phone_primary` varchar(16) NOT NULL,
  `college_name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_domain` (
  `email` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_emails` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_emails_1` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `email` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_isp_coupons` (
  `id` int(11) NOT NULL,
  `coupon` varchar(36) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `temp_spam_test` (
  `id` int(11) NOT NULL,
  `email` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_processed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` enum('active','unconfirmed') DEFAULT 'unconfirmed',
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `titles_profiles` (
  `id` int(11) NOT NULL,
  `title_id` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `tnp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `country_code` varchar(5) NOT NULL DEFAULT '+91',
  `phone_number` varchar(20) NOT NULL,
  `college_id` int(11) DEFAULT NULL,
  `college_rank` int(4) DEFAULT NULL,
  `is_dnd` enum('0','1') NOT NULL DEFAULT '0',
  `unique_key` varchar(36) NOT NULL,
  `is_education_banner_shown` tinyint(1) NOT NULL DEFAULT '0',
  `last_digest_computed_on` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `tnps_hired_students_applications_history` (
  `id` int(11) NOT NULL,
  `tnp_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `tnp_college_rank_csvs` (
  `id` int(11) NOT NULL,
  `file_path` varchar(512) NOT NULL,
  `status` enum('yet to start','processing','processed') NOT NULL,
  `errors` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `trainings` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `training_campaigns` (
  `id` int(30) NOT NULL,
  `name` varchar(256) NOT NULL,
  `campaign_type` enum('college','branch','year','city','exam') NOT NULL,
  `data` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `training_contests` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `training_contest_detail_id` int(11) NOT NULL,
  `start_date_time` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `end_date_time` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `training_contest_details` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `mega_contest_sort_order` int(11) DEFAULT NULL,
  `training_url` varchar(256) DEFAULT NULL,
  `contest_url` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `tu` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `urls_to_index` (
  `id` int(11) NOT NULL,
  `url` varchar(512) NOT NULL,
  `is_indexed` tinyint(1) NOT NULL DEFAULT '0',
  `response` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(1024) DEFAULT NULL,
  `password_hash_algo` enum('mysql','mysql_argon2','argon2') DEFAULT NULL,
  `type` enum('student','employer','admin','user','tnp','training_partner') NOT NULL,
  `status` enum('unregistered','unconfirmed','active','inactive','blocked','deleted') DEFAULT 'unregistered',
  `source` enum('unknown','subscription','internshala','facebook','internconnect','vtc','subscription_vtc','careerservices','subscription_careerservices','partners','subscription_partners','tnp','mentor','training_applicant','google_login','internshala_app','google_login_app','workshop','subscription_workshop','college_registration','isp_fest','subscription_blog','college_registration_tier2','college_registration_gsif','gsif_prelaunch') DEFAULT 'unknown',
  `do_subscribe` enum('0','1') NOT NULL DEFAULT '1',
  `unique_key` varchar(36) DEFAULT NULL,
  `google_token` varchar(2048) DEFAULT NULL,
  `is_three_day_activation_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_seven_day_activation_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_fifteen_day_activation_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_registered_in_first_session` tinyint(1) DEFAULT NULL,
  `is_verified_in_same_session` tinyint(1) DEFAULT NULL,
  `activation_link_sent_count` int(11) NOT NULL DEFAULT '0',
  `verified_at` timestamp NULL DEFAULT NULL,
  `last_seen_at` timestamp NULL DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `users_app_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_type` enum('trainings_slab') NOT NULL,
  `notification_sent_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 05:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_autocomplete_request_stats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` enum('location','location_detail') DEFAULT NULL,
  `ip2long` bigint(20) DEFAULT NULL,
  `ip` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_blocked_stats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` enum('login') DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL,
  `blocked_till` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_block_reasons` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `block_type` enum('soft_block','hard_block') DEFAULT NULL,
  `reason` varchar(2048) DEFAULT NULL,
  `blocked_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `unblocked_at` timestamp NULL DEFAULT '2001-01-01 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_otps` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `otp` varchar(8) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `is_active` tinyint(2) NOT NULL,
  `used_count` int(11) NOT NULL,
  `generated_at` timestamp NULL DEFAULT NULL,
  `last_sent_at_via_sms` timestamp NULL DEFAULT NULL,
  `last_sent_at_via_call` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_preferences` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `preference_type` enum('job','internship','tentative_job') DEFAULT 'internship',
  `sub_preference_type` enum('full-time','part-time','all-internship','work-from-home') DEFAULT NULL,
  `relocate` enum('yes','no') DEFAULT NULL,
  `international_internship` int(11) NOT NULL DEFAULT '0',
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_preferences_availabilities` (
  `id` int(11) NOT NULL,
  `user_preference_id` int(11) NOT NULL,
  `available_from` date DEFAULT NULL,
  `available_till` date DEFAULT NULL,
  `type` enum('part-time','full-time') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_preferences_categories` (
  `id` int(11) NOT NULL,
  `user_preference_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `preference_number` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_preferences_locations` (
  `id` int(11) NOT NULL,
  `user_preference_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `preference_number` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_preference_seen_stats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `source` varchar(128) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-02-19 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_registration_devices` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `browser_name` varchar(256) DEFAULT NULL,
  `browser_version` varchar(256) DEFAULT NULL,
  `device_type` varchar(256) DEFAULT NULL,
  `device_os` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2016-12-27 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_request_stats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` enum('login','registration','otp','subscription') DEFAULT NULL,
  `status` enum('success','failed') DEFAULT NULL,
  `ip` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `source` enum('password_reset','activation_link','request_activation','login') DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `token_hash` varchar(1024) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `valid_till` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
 


CREATE TABLE `vtc_notifications` (
  `id` int(11) NOT NULL,
  `type` varchar(256) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `total_discount_on_webdev` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;