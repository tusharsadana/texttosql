CREATE TABLE IF NOT EXISTS `applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `status` enum('open','rejected','shortlisted','hired','inactive','invited') NOT NULL,
  `invitation_status` enum('open','accepted','rejected','expired') DEFAULT NULL,
  `is_invitation_seen` tinyint(1) NOT NULL DEFAULT '0',
  `cover_letter` varchar(5000) DEFAULT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `is_hiring_exception` tinyint(4) DEFAULT '0',
  `is_message_sent` int(10) NOT NULL DEFAULT '0',
  `last_employer_message_sent_at` timestamp NULL DEFAULT NULL,
  `unique_key` varchar(36) DEFAULT NULL,
  `internshala_session_id` int(11) DEFAULT NULL,
  `hired_at` timestamp NULL DEFAULT NULL,
  `rejected_at` timestamp NULL DEFAULT NULL,
  `shortlisted_at` timestamp NULL DEFAULT NULL,
  `shortlisted_referral` enum('list_view','bulk_operator','details_view','admin_tool','chatbot','chat','invitation') DEFAULT NULL,
  `hired_referral` enum('bulk_operator','details_view','admin_tool','chatbot','chat','closure','list_view') DEFAULT NULL,
  `rejected_referral` enum('list_view','bulk_operator','details_view','admin_tool','chatbot','chat') DEFAULT NULL,
  `employer_dashboard_status` enum('opened','unopened') NOT NULL DEFAULT 'unopened',
  `opened_at` timestamp NULL DEFAULT NULL,
  `is_filtered_out` tinyint(1) NOT NULL DEFAULT '0',
  `is_sent_in_employer_digest` tinyint(1) NOT NULL DEFAULT '0',
  `is_internship_story_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `referral` enum('in_product_notification','web_push_notification','app_push_notification','mobile_web_push_notification','in_panel_notification','news_letter','matching_preferences','student_digest','app','student_digest_active','student_digest_inactive','search_matching_preferences','search_vanilla','search_filters','search_keyword','unknown','bookmark_web','bookmark_app','app_search_matching_preferences','app_search_keyword','app_search_vanilla','app_search_filters','app_unknown','similar_internships','app_similar_internships') DEFAULT NULL,
  `utm_source` varchar(256) DEFAULT NULL,
  `utm_medium` varchar(256) DEFAULT NULL,
  `utm_campaign` varchar(256) DEFAULT NULL,
  `previous_session_last_seen_at` timestamp NULL DEFAULT NULL,
  `applied_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `applications_combo_uq` (`student_id`,`job_id`),
  KEY `applications_internship_id_idx` (`job_id`),
  KEY `applications_student_id_idx` (`student_id`),
  KEY `internshala_session_id` (`internshala_session_id`),
  KEY `sort_order` (`sort_order`),
  KEY `status` (`status`),
  KEY `is_filtered_out` (`is_filtered_out`),
  KEY `applications_invitation_status` (`invitation_status`),
  KEY `is_internship_story_mail_sent` (`is_internship_story_mail_sent`)
) ENGINE=InnoDB AUTO_INCREMENT=32499988 DEFAULT CHARSET=latin1;
