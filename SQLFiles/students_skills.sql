CREATE TABLE IF NOT EXISTS `students_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `level` enum('None','Beginner','Intermediate','Advanced') DEFAULT 'None',
  `to_show_in_resume` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `students_skills_combo_uq` (`student_id`,`skill_id`),
  KEY `students_skills_skill_id_idx` (`skill_id`),
  KEY `students_skills_student_id_idx` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;