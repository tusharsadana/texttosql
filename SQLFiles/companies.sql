CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `type` enum('Academic','Corporate','NGO','Government','Startup') DEFAULT NULL,
  `is_big_brand` tinyint(1) NOT NULL DEFAULT '0',
  `is_cleaned` tinyint(1) NOT NULL DEFAULT '0',
  `facebook_page` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_user` (`created_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;