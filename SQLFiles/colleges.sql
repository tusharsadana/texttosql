CREATE TABLE IF NOT EXISTS `colleges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `premium_college_id` int(11) DEFAULT '-1',
  `description` varchar(1024) DEFAULT NULL,
  `website` varchar(1024) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `status` enum('active','unconfirmed') NOT NULL DEFAULT 'unconfirmed',
  `used_count` int(11) NOT NULL DEFAULT '0',
  `is_ignored` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '1999-12-31 13:00:00',
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Institutes_location_id_idx` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=400217 DEFAULT CHARSET=latin1;